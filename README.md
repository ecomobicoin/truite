# <h1 align="center"> 🐟 Truite 🐟 </h1>

An EcoMobiCoin consensus client, written in Rust and based on [Akula](https://akula.app/): Next-generation implementation of Ethereum protocol ("client") written in Rust, based on [Erigon architecture](https://github.com/ledgerwatch/interfaces).

## Installation

Installation instructions available on our [docs website](https://docs.ecomobicoin.limos.fr/docs/contribute/dev).

---

"Truite" is the french word for Trout, species of freshwater fish and emblematic of the Puy-de-Dôme department.

It is the symbol of a good quality of aquatic environments.

Several modifications of their habitat put in danger the maintenance of the good state of the wild populations:

-   Silting of the waterways which no longer offer a substrate adapted to reproduction (plantation of conifers, drainage, ...)
-   Presence of impassable obstacles that prevent access to the best spawning grounds (weirs, dams, etc.)
-   Water quality, particularly the temperature factor (increase linked to the presence of reserved flows, water bodies, etc.)

## License

The entire code within this repository is licensed under the [GNU Affero General Public License v3](./LICENSE)
