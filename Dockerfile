FROM registry.gitlab.limos.fr/ecomobicoin/infra/rust-builder-image:1.71 as chef
WORKDIR /app
RUN cargo install cargo-chef --version 0.1.53

FROM chef as planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef as builder
COPY --from=planner /app/recipe.json recipe.json
# Build dependencies - this is the caching Docker layer!
RUN cargo chef cook --profile=production --recipe-path recipe.json
# Build application
COPY . .
RUN RUST_BACKTRACE=1 cargo build --workspace --profile=production

FROM debian:bullseye-slim AS runtime
# GMP is already installed
RUN apt-get install -y e2fsprogs

COPY --from=builder /app/target/production/truite /usr/local/bin/truite
COPY --from=builder /app/target/production/truite-rpc /usr/local/bin/truite-rpc
COPY --from=builder /app/target/production/truite-sentry /usr/local/bin/truite-sentry
COPY --from=builder /app/target/production/truite-toolbox /usr/local/bin/truite-toolbox
COPY --from=builder /app/target/production/consensus-tests /usr/local/bin/consensus-tests

ARG UID=1000
ARG GID=1000
RUN groupadd -g $GID truite
RUN adduser --uid $UID --gid $GID truite
USER truite
RUN mkdir -p ~/.local/share/truite

EXPOSE 8545 \
    8551 \
    30303 \
    30303/udp \
    7545
