use clap::Parser;
use educe::Educe;
use secp256k1::{PublicKey, SecretKey, SECP256K1};

use truite::sentry::devp2p;

#[derive(Educe, Parser)]
#[clap(
    name = "truite-secrets",
    about = "Generate secrets for an Ecomobicoin node."
)]
#[educe(Debug)]
pub struct Opts {
    // TODO What options are needed ? Dunno yet @20230502
    /// Path to database directory.
    // #[clap(long = "datadir", help = "Database directory path", default_value_t)]
    // pub data_dir: TruiteDataDir,
    #[clap(long, default_value = "1")]
    pub n: usize,
}

// https://geth.ethereum.org/docs/fundamentals/private-network#running-member-nodes
// bootnode -genkey boot.key
// bootnode -nodekey boot.key -addr :30305
// ---
// secrets=$("$POLYGON_EDGE_BIN" secrets init --insecure --num 4 --data-dir /data/data- --json)
fn main() -> anyhow::Result<()> {
    let opts: Opts = Opts::parse();

    println!("Generating {} secrets...", opts.n);

    for i in 0..opts.n {
        println!("---");
        let secret_key = SecretKey::new(&mut secp256k1::rand::thread_rng());
        println!(
            "Generated new node key ({}): {}",
            i,
            hex::encode(secret_key.secret_bytes())
        );
        println!(
            "Node ID ({}): {}",
            i,
            hex::encode(
                devp2p::util::pk2id(&PublicKey::from_secret_key(SECP256K1, &secret_key)).as_bytes()
            )
        );
    }

    // std::fs::write(&secret_key_path, &hex::encode(secret_key.secret_bytes()))?;
    // std::fs::write(
    //     &node_id_path,
    //     &hex::encode(
    //         devp2p::util::pk2id(&PublicKey::from_secret_key(SECP256K1, &secret_key)).as_bytes(),
    //     ),
    // )?;

    // let jwt_secret_path = opt
    //                 .jwt_secret_path
    //                 .map(|v| v.0)
    //                 .unwrap_or_else(|| opt.datadir.0.join("jwt.hex"));
    //             if let Ok(mut file) = OpenOptions::new()
    //                 .write(true)
    //                 .create_new(true)
    //                 .open(jwt_secret_path)
    //             {
    //                 file.write_all(
    //                     hex::encode(
    //                         std::iter::repeat_with(rand::random)
    //                             .take(32)
    //                             .collect::<Vec<_>>(),
    //                     )
    //                     .as_bytes(),
    //                 )?;
    //                 file.flush()?;
    //             }
    Ok(())
}
