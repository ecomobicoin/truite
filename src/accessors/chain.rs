use tracing::*;

use crate::{
    kv::{mdbx::*, tables},
    models::*,
};

pub mod canonical_hash {
    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Option<H256>> {
        let number = number.into();
        trace!("Reading canonical hash for block number {}", number);

        tx.get(tables::CanonicalHeader, number)
    }
}

pub mod header_number {
    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        hash: H256,
    ) -> anyhow::Result<Option<BlockNumber>> {
        trace!("Reading header number for hash {:?}", hash);

        tx.get(tables::HeaderNumber, hash)
    }
}

pub mod chain_config {
    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
    ) -> anyhow::Result<Option<ChainSpec>> {
        trace!("Reading chain specification");

        tx.get(tables::Config, ())
    }
}

pub mod header {
    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Option<BlockHeader>> {
        let number = number.into();
        trace!("Reading header for block number {}", number);

        tx.get(tables::Header, number)
    }
}

pub mod tx {
    use super::*;

    pub fn read_for_sender_before<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        addr: Address,
        block_num: u64,
        page_size: usize,
    ) -> anyhow::Result<Vec<(MessageWithSignature, BlockNumber)>> {
        let block_num_for_address = tx.get(tables::TransactionsBySender, addr)?;
        if block_num_for_address.is_none() {
            return Ok(vec![]);
        }
        let block_num_for_address = block_num_for_address.unwrap();

        let mut res: Vec<(MessageWithSignature, BlockNumber)> = vec![];
        let mut found_txs_count = 0_usize;
        // We iter until we found enough txs to fill the page or there is not tx to find anymore
        for block_number in block_num_for_address.iter().rev() {
            if found_txs_count >= page_size {
                break;
            }
            if block_number.0 >= block_num {
                let block = block_body::read_without_senders(tx, block_number.0)?;
                if block.is_some() {
                    let block = block.unwrap();
                    for tx in block.transactions {
                        if tx.recover_sender()?.eq(&addr) {
                            found_txs_count += 1;
                            res.push((tx, block_number.clone()));
                        }
                    }
                }
            }
        }

        Ok(res)
    }

    pub fn read_for_sender_after<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        addr: Address,
        block_num: u64,
        page_size: usize,
    ) -> anyhow::Result<Vec<(MessageWithSignature, BlockNumber)>> {
        let block_num_for_address = tx.get(tables::TransactionsBySender, addr)?;
        if block_num_for_address.is_none() {
            return Ok(vec![]);
        }
        let block_num_for_address = block_num_for_address.unwrap();

        let mut res: Vec<(MessageWithSignature, BlockNumber)> = vec![];
        let mut found_txs_count = 0_usize;
        // We iter until we found enough txs to fill the page or there is not tx to find anymore
        for block_number in block_num_for_address.iter() {
            if found_txs_count >= page_size {
                break;
            }
            if block_number.0 <= block_num {
                let block = block_body::read_without_senders(tx, block_number.0)?;
                if block.is_some() {
                    let block = block.unwrap();
                    for tx in block.transactions {
                        if tx.recover_sender()?.eq(&addr) {
                            found_txs_count += 1;
                            res.push((tx, block_number.clone()));
                        }
                    }
                }
            }
        }

        Ok(res)
    }

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        base_tx_id: impl Into<TxIndex>,
        amount: usize,
    ) -> anyhow::Result<Vec<MessageWithSignature>> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Reading {} transactions starting from {}",
            amount,
            base_tx_id
        );

        if amount > 0 {
            tx.cursor(tables::BlockTransaction)?
                .walk(Some(base_tx_id))
                .take(amount)
                .map(|res| res.map(|(_, v)| v))
                .collect()
        } else {
            Ok(vec![])
        }
    }

    pub fn read_many<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        amount: usize,
    ) -> anyhow::Result<Vec<MessageWithSignature>> {
        if amount > 0 {
            tx.cursor(tables::BlockTransaction)?
                .walk(None)
                .take(amount)
                .map(|res| res.map(|(_, v)| v))
                .collect()
        } else {
            Ok(vec![])
        }
    }

    pub fn write<'db, E: EnvironmentKind>(
        tx: &MdbxTransaction<'db, RW, E>,
        base_tx_id: impl Into<TxIndex>,
        txs: &[MessageWithSignature],
    ) -> anyhow::Result<()> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Writing {} transactions starting from {}",
            txs.len(),
            base_tx_id
        );

        let mut cursor = tx.cursor(tables::BlockTransaction)?;

        for (i, eth_tx) in txs.iter().enumerate() {
            cursor.put(base_tx_id + i as u64, eth_tx.clone())?;
        }

        Ok(())
    }

    pub fn write_from_vec<'db, E: EnvironmentKind>(
        tx: &MdbxTransaction<'db, RW, E>,
        base_tx_id: impl Into<TxIndex>,
        txs: &Vec<MessageWithSignature>,
    ) -> anyhow::Result<()> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Writing {} transactions starting from {}",
            txs.len(),
            base_tx_id
        );

        let mut cursor = tx.cursor(tables::BlockTransaction)?;

        for (i, eth_tx) in txs.iter().enumerate() {
            cursor.put(base_tx_id + i as u64, eth_tx.clone())?;
        }

        Ok(())
    }
}

pub mod tx_pending {
    use itertools::Itertools;
    use rand::Rng;

    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        base_tx_id: impl Into<TxIndex>,
        amount: usize,
    ) -> anyhow::Result<Vec<MessageWithSignature>> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Reading {} transactions starting from {}",
            amount,
            base_tx_id
        );

        if amount > 0 {
            tx.cursor(tables::PendingTransaction)?
                .walk(Some(base_tx_id))
                .take(amount)
                .map(|res| res.map(|(_, v)| v))
                .collect()
        } else {
            Ok(vec![])
        }
    }

    pub fn read_many_with_index<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        amount: usize,
    ) -> anyhow::Result<Vec<(TxIndex, MessageWithSignature)>> {
        if amount > 0 {
            tx.cursor(tables::PendingTransaction)?
                .walk(None)
                .take(amount)
                .collect()
        } else {
            Ok(vec![])
        }
    }

    pub fn read_many<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        amount: usize,
    ) -> anyhow::Result<Vec<MessageWithSignature>> {
        if amount > 0 {
            tx.cursor(tables::PendingTransaction)?
                .walk(None)
                .take(amount)
                .map(|res| res.map(|(_, v)| v))
                .collect()
        } else {
            Ok(vec![])
        }
    }

    /// Used to gather txs / bxs from pool with random strategy of mining
    pub fn read_many_randomly<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        amount: usize,
    ) -> anyhow::Result<Vec<(TxIndex, MessageWithSignature)>> {
        if amount > 0 {
            let pending_transactions_in_db = tx
                .cursor(tables::PendingTransaction)?
                .walk(None)
                .collect_vec();
            // Return less entries than wanted if db is not full enough
            if amount > pending_transactions_in_db.len() {
                return tx
                    .cursor(tables::PendingTransaction)?
                    .walk(None)
                    .take(amount)
                    .collect();
            } else {
                let mut remaining_to_read = amount;
                let mut result: Vec<(TxIndex, MessageWithSignature)> = vec![];
                let mut iterator = tx.cursor(tables::PendingTransaction)?.walk(None);
                while remaining_to_read > 0 {
                    let mut random_walk: u8 = rand::thread_rng().gen();
                    let val = {
                        // Do a random number of next in iterator to get random entry
                        while random_walk > 0 {
                            random_walk -= 1;
                            let value = iterator.next();
                            // If you arrive at the end of the db, return at first entry
                            if value.is_none() {
                                iterator = tx.cursor(tables::PendingTransaction)?.walk(None);
                            }
                        }
                        let mut value = iterator.next();
                        if value.is_none() {
                            value = tx.cursor(tables::PendingTransaction)?.first().transpose();
                        }
                        let mut value = value.unwrap().unwrap();
                        // Check that the value is not already present in the result vector
                        // Otherwise iterates until a non-duplicate value can be added
                        while result.contains(&value) {
                            let mut tmp = iterator.next();
                            if tmp.is_none() {
                                tmp = tx.cursor(tables::PendingTransaction)?.first().transpose();
                            }
                            value = tmp.unwrap().unwrap();
                        }
                        value
                    };
                    result.push(val);
                    remaining_to_read -= 1;
                }
                Ok(result)
            }
        } else {
            Ok(vec![])
        }
    }

    pub fn write<'db, E: EnvironmentKind>(
        tx: &MdbxTransaction<'db, RW, E>,
        base_tx_id: impl Into<TxIndex>,
        txs: &[MessageWithSignature],
    ) -> anyhow::Result<()> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Writing {} transactions starting from {}",
            txs.len(),
            base_tx_id
        );

        let mut cursor = tx.cursor(tables::PendingTransaction)?;

        for (i, eth_tx) in txs.iter().enumerate() {
            cursor.put(base_tx_id + i as u64, eth_tx.clone())?;
        }

        Ok(())
    }

    pub fn write_from_vec<'db, E: EnvironmentKind>(
        tx: &MdbxTransaction<'db, RW, E>,
        base_tx_id: impl Into<TxIndex>,
        txs: &Vec<MessageWithSignature>,
    ) -> anyhow::Result<()> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Writing {} transactions starting from {}",
            txs.len(),
            base_tx_id
        );

        let mut cursor = tx.cursor(tables::PendingTransaction)?;

        for (i, eth_tx) in txs.iter().enumerate() {
            cursor.put(base_tx_id + i as u64, eth_tx.clone())?;
        }

        Ok(())
    }

    // Handle full pending lifecycle
    pub fn delete() {}
}

pub mod bx {
    use crate::models::behavior::BehaviorWithSignature;

    use super::*;

    pub fn read_for_sender_before<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        addr: Address,
        block_num: u64,
        page_size: usize,
    ) -> anyhow::Result<Vec<(BehaviorWithSignature, BlockNumber)>> {
        let block_num_for_address = tx.get(tables::BehaviorsBySender, addr)?;
        if block_num_for_address.is_none() {
            return Ok(vec![]);
        }
        let block_num_for_address = block_num_for_address.unwrap();

        let mut res: Vec<(BehaviorWithSignature, BlockNumber)> = vec![];
        let mut found_bxs_count = 0_usize;
        // We iter until we found enough txs to fill the page or there is not tx to find anymore
        for block_number in block_num_for_address.iter().rev() {
            if found_bxs_count >= page_size {
                break;
            }
            if block_number.0 >= block_num {
                let block = block_body::read_without_senders(tx, block_number.0)?;
                if block.is_some() {
                    let block = block.unwrap();
                    for bx in block.behaviors {
                        if bx.recover_sender()?.eq(&addr) {
                            found_bxs_count += 1;
                            res.push((bx, block_number.clone()));
                        }
                    }
                }
            }
        }
        Ok(res)
    }

    pub fn read_for_sender_after<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        addr: Address,
        block_num: u64,
        page_size: usize,
    ) -> anyhow::Result<Vec<(BehaviorWithSignature, BlockNumber)>> {
        let block_num_for_address = tx.get(tables::BehaviorsBySender, addr)?;
        if block_num_for_address.is_none() {
            return Ok(vec![]);
        }
        let block_num_for_address = block_num_for_address.unwrap();

        let mut res: Vec<(BehaviorWithSignature, BlockNumber)> = vec![];
        let mut found_bxs_count = 0_usize;
        // We iter until we found enough txs to fill the page or there is not tx to find anymore
        for block_number in block_num_for_address.iter() {
            if found_bxs_count >= page_size {
                break;
            }
            if block_number.0 <= block_num {
                let block = block_body::read_without_senders(tx, block_number.0)?;
                if block.is_some() {
                    let block = block.unwrap();
                    for bx in block.behaviors {
                        if bx.recover_sender()?.eq(&addr) {
                            found_bxs_count += 1;
                            res.push((bx, block_number.clone()));
                        }
                    }
                }
            }
        }

        Ok(res)
    }

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        base_bx_id: impl Into<TxIndex>,
        amount: usize,
    ) -> anyhow::Result<Vec<BehaviorWithSignature>> {
        let base_bx_id = base_bx_id.into();
        trace!("Reading {} behaviors starting from {}", amount, base_bx_id);

        if amount > 0 {
            tx.cursor(tables::BlockBehavior)?
                .walk(Some(base_bx_id))
                .take(amount)
                .map(|res| res.map(|(_, v)| v))
                .collect()
        } else {
            Ok(vec![])
        }
    }

    pub fn read_many<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        amount: usize,
    ) -> anyhow::Result<Vec<BehaviorWithSignature>> {
        if amount > 0 {
            tx.cursor(tables::BlockBehavior)?
                .walk(None)
                .take(amount)
                .map(|res| res.map(|(_, v)| v))
                .collect()
        } else {
            Ok(vec![])
        }
    }

    pub fn write<'db, E: EnvironmentKind>(
        tx: &MdbxTransaction<'db, RW, E>,
        base_tx_id: impl Into<TxIndex>,
        txs: &[BehaviorWithSignature],
    ) -> anyhow::Result<()> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Writing {} behaviors starting from {}",
            txs.len(),
            base_tx_id
        );

        let mut cursor = tx.cursor(tables::BlockBehavior)?;

        for (i, eth_tx) in txs.iter().enumerate() {
            cursor.put(base_tx_id + i as u64, eth_tx.clone())?;
        }

        Ok(())
    }

    pub fn write_from_vec<'db, E: EnvironmentKind>(
        tx: &MdbxTransaction<'db, RW, E>,
        base_tx_id: impl Into<TxIndex>,
        txs: &Vec<BehaviorWithSignature>,
    ) -> anyhow::Result<()> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Writing {} behaviors starting from {}",
            txs.len(),
            base_tx_id
        );

        let mut cursor = tx.cursor(tables::BlockBehavior)?;

        for (i, eth_tx) in txs.iter().enumerate() {
            cursor.put(base_tx_id + i as u64, eth_tx.clone())?;
        }

        Ok(())
    }
}

pub mod bx_pending {
    use itertools::Itertools;
    use rand::Rng;

    use crate::models::behavior::BehaviorWithSignature;

    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        base_bx_id: impl Into<TxIndex>,
        amount: usize,
    ) -> anyhow::Result<Vec<BehaviorWithSignature>> {
        let base_bx_id = base_bx_id.into();
        trace!("Reading {} behaviors starting from {}", amount, base_bx_id);

        if amount > 0 {
            tx.cursor(tables::PendingBehavior)?
                .walk(Some(base_bx_id))
                .take(amount)
                .map(|res| res.map(|(_, v)| v))
                .collect()
        } else {
            Ok(vec![])
        }
    }

    pub fn read_many<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        amount: usize,
    ) -> anyhow::Result<Vec<BehaviorWithSignature>> {
        if amount > 0 {
            tx.cursor(tables::PendingBehavior)?
                .walk(None)
                .take(amount)
                .map(|res| res.map(|(_, v)| v))
                .collect()
        } else {
            Ok(vec![])
        }
    }

    pub fn read_many_with_index<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        amount: usize,
    ) -> anyhow::Result<Vec<(TxIndex, BehaviorWithSignature)>> {
        if amount > 0 {
            tx.cursor(tables::PendingBehavior)?
                .walk(None)
                .take(amount)
                .collect()
        } else {
            Ok(vec![])
        }
    }

    /// Used to gather txs / bxs from pool with random strategy of mining
    pub fn read_many_randomly<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        amount: usize,
    ) -> anyhow::Result<Vec<(TxIndex, BehaviorWithSignature)>> {
        if amount > 0 {
            let pending_behaviors_in_db =
                tx.cursor(tables::PendingBehavior)?.walk(None).collect_vec();
            // Return less entries than wanted if db is not full enough
            if amount > pending_behaviors_in_db.len() {
                return tx
                    .cursor(tables::PendingBehavior)?
                    .walk(None)
                    .take(amount)
                    .collect();
            } else {
                let mut remaining_to_read = amount;
                let mut result: Vec<(TxIndex, BehaviorWithSignature)> = vec![];
                let mut iterator = tx.cursor(tables::PendingBehavior)?.walk(None);
                while remaining_to_read > 0 {
                    let mut random_walk: u8 = rand::thread_rng().gen();
                    let val = {
                        // Do a random number of next in iterator to get random entry
                        while random_walk > 0 {
                            random_walk -= 1;
                            let value = iterator.next();
                            // If you arrive at the end of the db, return at first entry
                            if value.is_none() {
                                iterator = tx.cursor(tables::PendingBehavior)?.walk(None);
                            }
                        }
                        let mut value = iterator.next();
                        if value.is_none() {
                            value = tx.cursor(tables::PendingBehavior)?.first().transpose();
                        }
                        let mut value = value.unwrap().unwrap();
                        // Check that the value is not already present in the result vector
                        // Otherwise iterates until a non-duplicate value can be added
                        while result.contains(&value) {
                            let mut tmp = iterator.next();
                            if tmp.is_none() {
                                tmp = tx.cursor(tables::PendingBehavior)?.first().transpose();
                            }
                            value = tmp.unwrap().unwrap();
                        }
                        value
                    };
                    result.push(val);
                    remaining_to_read -= 1;
                }
                Ok(result)
            }
        } else {
            Ok(vec![])
        }
    }

    pub fn write<'db, E: EnvironmentKind>(
        tx: &MdbxTransaction<'db, RW, E>,
        base_tx_id: impl Into<TxIndex>,
        bxs: &[BehaviorWithSignature],
    ) -> anyhow::Result<()> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Writing {} behaviors starting from {}",
            bxs.len(),
            base_tx_id
        );

        let mut cursor = tx.cursor(tables::PendingBehavior)?;

        for (i, eth_bx) in bxs.iter().enumerate() {
            cursor.put(base_tx_id + i as u64, eth_bx.clone())?;
        }

        Ok(())
    }

    pub fn write_from_vec<'db, E: EnvironmentKind>(
        tx: &MdbxTransaction<'db, RW, E>,
        base_tx_id: impl Into<TxIndex>,
        bxs: &Vec<BehaviorWithSignature>,
    ) -> anyhow::Result<()> {
        let base_tx_id = base_tx_id.into();
        trace!(
            "Writing {} behaviors starting from {}",
            bxs.len(),
            base_tx_id
        );

        let mut cursor = tx.cursor(tables::PendingBehavior)?;

        for (i, eth_tx) in bxs.iter().enumerate() {
            cursor.put(base_tx_id + i as u64, eth_tx.clone())?;
        }

        Ok(())
    }
}

pub mod bx_sender {
    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Vec<Address>> {
        let number = number.into();

        trace!("Reading behaviors senders for block {}", number,);

        Ok(tx.get(tables::BxSender, number)?.unwrap_or_default())
    }

    pub fn write<E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, RW, E>,
        number: impl Into<BlockNumber>,
        senders: Vec<Address>,
    ) -> anyhow::Result<()> {
        let number = number.into();
        trace!(
            "Writing {} transaction senders for block {}",
            senders.len(),
            number,
        );

        tx.set(tables::BxSender, number, senders)?;

        Ok(())
    }
}

pub mod tx_sender {
    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Vec<Address>> {
        let number = number.into();

        trace!("Reading transaction senders for block {}", number,);

        Ok(tx.get(tables::TxSender, number)?.unwrap_or_default())
    }

    pub fn write<E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, RW, E>,
        number: impl Into<BlockNumber>,
        senders: Vec<Address>,
    ) -> anyhow::Result<()> {
        let number = number.into();
        trace!(
            "Writing {} transaction senders for block {}",
            senders.len(),
            number,
        );

        tx.set(tables::TxSender, number, senders)?;

        Ok(())
    }
}

pub mod storage_body {
    use super::*;

    pub fn read<K, E>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Option<BodyForStorage>>
    where
        K: TransactionKind,
        E: EnvironmentKind,
    {
        let number = number.into();
        trace!("Reading storage body for block {number}");

        tx.get(tables::BlockBody, number)
    }

    pub fn write<E>(
        tx: &MdbxTransaction<'_, RW, E>,
        number: impl Into<BlockNumber>,
        body: &BodyForStorage,
    ) -> anyhow::Result<()>
    where
        E: EnvironmentKind,
    {
        let number = number.into();
        trace!("Writing storage body for block {number}");

        tx.set(tables::BlockBody, number, body.clone())?;

        Ok(())
    }
}

pub mod behavior_storage_body {
    use super::*;

    pub fn read<K, E>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Option<BehaviorBodyForStorage>>
    where
        K: TransactionKind,
        E: EnvironmentKind,
    {
        let number = number.into();
        trace!("Reading storage body for block {number}");

        tx.get(tables::BehaviorBlockBody, number)
    }

    pub fn write<E>(
        tx: &MdbxTransaction<'_, RW, E>,
        number: impl Into<BlockNumber>,
        body: &BehaviorBodyForStorage,
    ) -> anyhow::Result<()>
    where
        E: EnvironmentKind,
    {
        let number = number.into();
        trace!("Writing storage body for block {number}");

        tx.set(tables::BehaviorBlockBody, number, body.clone())?;

        Ok(())
    }
}

pub mod block_body {
    use crate::models::behavior::BehaviorWithSender;

    use super::*;

    fn read_base<K, E>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Option<(BlockBody, TxIndex)>>
    where
        K: TransactionKind,
        E: EnvironmentKind,
    {
        if let Some(body) = super::storage_body::read(tx, number)? {
            debug!("Body from DB: {:?}", body);
            let transactions = super::tx::read(tx, body.base_tx_id, body.tx_amount.try_into()?)?;
            let behaviors = super::bx::read(tx, body.base_bx_id, body.bx_amount.try_into()?)?;

            return Ok(Some((
                BlockBody {
                    transactions,
                    behaviors,
                    ommers: body.ommers,
                },
                body.base_tx_id,
            )));
        }

        Ok(None)
    }

    pub fn read_without_senders<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Option<BlockBody>> {
        Ok(read_base(tx, number)?.map(|(v, _)| v))
    }

    pub fn read_with_senders<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Option<BlockBodyWithSenders>> {
        let number = number.into();
        if let Some((body, _)) = read_base(tx, number)? {
            return Ok(Some(BlockBodyWithSenders {
                transactions: body
                    .transactions
                    .into_iter()
                    .map(|tx| MessageWithSender {
                        message: tx.message.clone(),
                        sender: tx.recover_sender().unwrap_or(Address::zero()),
                    })
                    .collect(),
                behaviors: body
                    .behaviors
                    .into_iter()
                    .map(|bx| BehaviorWithSender {
                        behavior: bx.behavior.clone(),
                        sender: bx.recover_sender().unwrap_or(Address::zero()),
                    })
                    .collect(),
                ommers: body.ommers,
            }));
        }

        Ok(None)
    }
}

pub mod td {
    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        number: impl Into<BlockNumber>,
    ) -> anyhow::Result<Option<U256>> {
        let number = number.into();
        trace!("Reading total difficulty at block {number}");

        tx.get(tables::HeadersTotalDifficulty, number)
    }
}

pub mod tl {
    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        tx_hash: H256,
    ) -> anyhow::Result<Option<BlockNumber>> {
        trace!("Reading Block number for a tx_hash {:?}", tx_hash);

        Ok(tx
            .get(tables::BlockTransactionLookup, tx_hash)?
            .map(|b| b.0))
    }

    pub fn write<'db: 'tx, 'tx, E: EnvironmentKind>(
        tx: &'tx MdbxTransaction<'db, RW, E>,
        hashed_tx_data: H256,
        block_number: BlockNumber,
    ) -> anyhow::Result<()> {
        trace!("Writing tx_lookup for hash {}", hashed_tx_data);

        tx.set(
            tables::BlockTransactionLookup,
            hashed_tx_data,
            block_number.into(),
        )?;

        Ok(())
    }
}

pub mod bl {
    use super::*;

    pub fn read<K: TransactionKind, E: EnvironmentKind>(
        tx: &MdbxTransaction<'_, K, E>,
        bx_hash: H256,
    ) -> anyhow::Result<Option<BlockNumber>> {
        trace!("Reading Block number for a bx_hash {:?}", bx_hash);

        Ok(tx.get(tables::BlockBehaviorLookup, bx_hash)?.map(|b| b.0))
    }

    pub fn write<'db: 'tx, 'tx, E: EnvironmentKind>(
        tx: &'tx MdbxTransaction<'db, RW, E>,
        hashed_bx_data: H256,
        block_number: BlockNumber,
    ) -> anyhow::Result<()> {
        trace!("Writing bx_lookup for hash {}", hashed_bx_data);

        tx.set(
            tables::BlockBehaviorLookup,
            hashed_bx_data,
            block_number.into(),
        )?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use bytes::Bytes;

    use crate::kv::new_mem_chaindata;

    use super::*;

    #[test]
    fn accessors() {
        let tx1 = MessageWithSignature {
            message: Message::Legacy {
                chain_id: None,
                nonce: 1,
                gas_price: 20_000.as_u256(),
                gas_limit: 3_000_000,
                action: TransactionAction::Create,
                value: 0.as_u256(),
                input: Bytes::new(),
            },
            signature: MessageSignature::new(false, H256::repeat_byte(2), H256::repeat_byte(3))
                .unwrap(),
        };
        let tx2 = MessageWithSignature {
            message: Message::Legacy {
                chain_id: None,
                nonce: 2,
                gas_price: 30_000.as_u256(),
                gas_limit: 1_000_000,
                action: TransactionAction::Create,
                value: 10.as_u256(),
                input: Bytes::new(),
            },
            signature: MessageSignature::new(true, H256::repeat_byte(6), H256::repeat_byte(9))
                .unwrap(),
        };
        let txs = [tx1, tx2];

        let sender1 = Address::random();
        let sender2 = Address::random();
        let senders = [sender1, sender2];

        let block1_hash = H256::random();
        let body = BodyForStorage {
            base_tx_id: 1.into(),
            base_bx_id: 0.into(),
            tx_amount: 2,
            bx_amount: 0,
            ommers: Default::default(),
        };

        let db = new_mem_chaindata().unwrap();
        let rwtx = db.begin_mutable().unwrap();
        let rwtx = &rwtx;

        storage_body::write(rwtx, 1, &body).unwrap();
        rwtx.set(tables::CanonicalHeader, 1.into(), block1_hash)
            .unwrap();
        tx::write(rwtx, 1, &txs).unwrap();
        tx_sender::write(rwtx, 1, senders.to_vec()).unwrap();

        let recovered_body = storage_body::read(rwtx, 1)
            .unwrap()
            .expect("Could not recover storage body.");
        let recovered_hash = rwtx
            .get(tables::CanonicalHeader, 1.into())
            .unwrap()
            .expect("Could not recover block hash");
        let recovered_txs = tx::read(rwtx, 1, 2).unwrap();
        let recovered_senders = tx_sender::read(rwtx, 1).unwrap();

        assert_eq!(body, recovered_body);
        assert_eq!(block1_hash, recovered_hash);
        assert_eq!(txs, *recovered_txs);
        assert_eq!(senders, *recovered_senders);
    }
}
