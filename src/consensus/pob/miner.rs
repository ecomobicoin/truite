use std::{
    ops::{Add, Div, Mul},
    sync::Arc,
    time::SystemTime,
};

use anyhow::{anyhow, format_err};
use arrayvec::ArrayVec;
use bitvec::macros::internal::funty::Fundamental;
use bytes::Bytes;
use derive_more::Display;
use ecomobicoin_jsonrpc::types::BlockNumber;
use ethereum_types::{Address, BigEndianHash, H256};
use ethnum::U256;
use itertools::Itertools;
use mdbx::{EnvironmentKind, TransactionKind, RO, RW};
use parking_lot::RwLock;
use tracing::*;

use crate::consensus::{
    demurrage::demurrage::reverse_demurrage_for_block_on_all_account,
    difficulty::should_update_network_difficulty,
};
use crate::consensus::{validate_behavior, validate_transaction};
use crate::{
    accessors::{
        chain,
        chain::bl,
        {self},
    },
    consensus::{
        demurrage::demurrage, miner::MinerError::DidntProduceOnTime, Consensus, DuoError, PoBError,
        Quantifiable, ValidationError::CantComputeStateRoot,
    },
    crypto::keccak256,
    execution::{analysis_cache::AnalysisCache, processor::ExecutionProcessor, tracer::Tracer},
    kv::{mdbx::MdbxTransaction, tables, MdbxWithDirHandle},
    models::{
        behavior::{Behavior, BehaviorWithSender, BehaviorWithSignature},
        Account, Block, BlockBodyWithSenders, BlockExecutionSpec, BlockHeader, BodyForStorage,
        ExecutedBlock, Message, MessageWithSender, MessageWithSignature, OpenBlock, TxIndex,
    },
    p2p,
    p2p::{
        node::Node,
        types::{NewBlock, PeerFilter, Status},
    },
    rpc::helpers,
    stages, Buffer, InMemoryState, StateWriter,
};

use super::{
    difficulty::{compute_average_block_time_or_desired, update_network_difficulty},
    vdf::{compute_vdf_difficulty, vdf},
};

/// Configures the behaviour of the miner.
#[derive(Debug, PartialEq, Eq)]
pub struct MinerOptions {
    pub(crate) strategy: MiningStrategy,
}

/// Configurable parameters of block authoring.
#[derive(Debug, Default, Clone)]
pub struct AuthoringParams {
    /// Block author
    pub author: Address,
    /// Block extra data
    pub extra_data: Bytes,
}

/// Block sealing mechanism
pub enum Author {
    /// Sealing(=mining) block is external and we only need a reward beneficiary (i.e. PoW)
    External(Address),
    // Sealing is done internally, we need a way to create signatures to seal block (i.e. PoA)
    // Sealer(Box<dyn EngineSigner>),
}

/// Configurable mining strategy of block authoring.
#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub enum MiningStrategy {
    EqualProportions,
    // of txs / bxs
    #[default]
    RandomSelection,
    // MaxTransactions,
    // MaxBehaviors,
    // ...
    InternalTests, // few number of txs / bxs, fixed, also fixes vdf
}

/// At the moment, two errors can be encountered when trying to mine, first is if node is not fully synced,
/// node will answer with error "sync progress not found".
/// Other one is if, during VDF computation, another miner computes his VDF faster and produces a valid block before
/// this node. In this case, error DidntProduceOnTime is returned
#[derive(Debug, Clone, PartialEq, Eq, Display)]
pub enum MinerError {
    DidntProduceOnTime,
}

pub const BX_NUMBER_CHOSEN_BY_MINER: usize = 500;

/// All parameters, structure, db access, ... needed to mine
pub struct Miner<'tracer, 'analysis, 'c, SE>
where
    SE: EnvironmentKind,
{
    params: RwLock<AuthoringParams>,
    options: MinerOptions,
    tracer: &'tracer mut dyn Tracer,
    analysis_cache: &'analysis mut AnalysisCache,
    engine: Box<dyn Consensus>,
    block_spec: &'c BlockExecutionSpec,
    pub db: Arc<MdbxWithDirHandle<SE>>,
    pub node: Arc<Node>,
}

impl<'tracer, 'analysis, 'c, SE> Miner<'tracer, 'analysis, 'c, SE>
where
    SE: EnvironmentKind,
{
    pub fn new(
        params: AuthoringParams,
        options: MinerOptions,
        tracer: &'tracer mut dyn Tracer,              // optional
        analysis_cache: &'analysis mut AnalysisCache, // ?
        engine: Box<dyn Consensus>,                   // Consensus engine: needed
        block_spec: &'c BlockExecutionSpec,           // or chainspec or chainconfig: needed
        db: Arc<MdbxWithDirHandle<SE>>,
        node: Arc<Node>,
    ) -> Self {
        Miner {
            params: RwLock::new(params),
            options,
            tracer,
            analysis_cache,
            engine,
            block_spec,
            db,
            node,
        }
    }

    /// Start mining from a Miner's Proof of Behavior
    pub async fn start_from_pob_m(
        &mut self,
        miner_behavior: BehaviorWithSignature,
    ) -> anyhow::Result<ExecutedBlock> {
        let db = self.db.clone();
        let txn = db.begin()?;

        let parent_block_header = get_latest_parent_header(&txn)?;

        info!(
            "Start from PoB miner, parent block {:?}, parent header hash: {:?}",
            parent_block_header.number,
            parent_block_header.hash()
        );

        let next_block_number = parent_block_header.number.0 + 1;
        let network_difficulty = if should_update_network_difficulty(
            self.block_spec,
            crate::models::BlockNumber(next_block_number),
        ) {
            let mut previous_timestamps = vec![];
            for n in (next_block_number)
                .checked_sub(self.block_spec.params.desired_average_mining_time_in_ms)
                .unwrap_or(0)..(next_block_number)
            {
                let header = accessors::chain::header::read(&txn, n)?;
                match header {
                    Some(header) => previous_timestamps.push(header.timestamp),
                    None => warn!("No header found for block number {}", n),
                }
            }
            update_network_difficulty(
                self.block_spec,
                parent_block_header.difficulty,
                compute_average_block_time_or_desired(
                    self.block_spec.params.desired_average_mining_time_in_ms,
                    previous_timestamps,
                ),
            )
        } else {
            parent_block_header.difficulty
        };
        let network_difficulty = if network_difficulty.eq(&U256::ZERO) {
            U256::ONE
        } else {
            network_difficulty
        };
        let mut vdf_difficulty = prepare_work(
            parent_block_header.parent_hash,
            miner_behavior.behavior.clone(),
            network_difficulty.as_u64(),
        );

        info!(
            "VDF difficulty: {:#?}, network difficulty: {:?}",
            vdf_difficulty, network_difficulty
        );

        // Dry run the VDF is true, replacing its output by zeroes.
        if self.options.strategy == MiningStrategy::InternalTests {
            vdf_difficulty = 7357;
            warn!("DRY RUN ENABLED, FOR TEST PURPOSES ONLY");
            println!("DRY RUN ENABLED, FOR TEST PURPOSES ONLY");
        }

        let proof = self
            .compute_vdf_parallel(&parent_block_header, vdf_difficulty)
            .await;
        if proof.is_err() {
            return Err(anyhow!(DidntProduceOnTime));
        }
        let vdf_proof = proof.unwrap();

        let pending_txs: Vec<(TxIndex, MessageWithSignature)>;
        let pending_bxs: Vec<(TxIndex, BehaviorWithSignature)>;
        let txn = db.begin_mutable().unwrap();
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();

        clean_bx_pool(&txn, self.block_spec, now)?;
        txn.commit()?;
        let txn = db.begin().unwrap();

        match self.options.strategy {
            MiningStrategy::EqualProportions => {
                pending_txs =
                    chain::tx_pending::read_many_with_index(&txn, BX_NUMBER_CHOSEN_BY_MINER)?;
                pending_bxs =
                    chain::bx_pending::read_many_with_index(&txn, BX_NUMBER_CHOSEN_BY_MINER)?;
            }
            MiningStrategy::RandomSelection => {
                pending_txs =
                    chain::tx_pending::read_many_randomly(&txn, BX_NUMBER_CHOSEN_BY_MINER)?;
                pending_bxs =
                    chain::bx_pending::read_many_randomly(&txn, BX_NUMBER_CHOSEN_BY_MINER)?;
            }
            MiningStrategy::InternalTests => {
                pending_txs =
                    chain::tx_pending::read_many_with_index(&txn, BX_NUMBER_CHOSEN_BY_MINER)?;
                pending_bxs =
                    chain::bx_pending::read_many_with_index(&txn, BX_NUMBER_CHOSEN_BY_MINER)?;
            }
        }

        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();

        check_behavior_expiration(
            self.block_spec.clone(),
            now,
            miner_behavior.clone().behavior,
        )?;

        check_pending_tx_bx(
            db.clone(),
            self.block_spec,
            parent_block_header.clone(),
            now,
            pending_txs.clone(),
            pending_bxs.clone(),
        )?;
        let txn = db.begin_mutable()?;

        let _ = demurrage::apply_demurrage_for_block_on_all_account(
            &txn,
            self.node
                .config
                .chain_spec
                .params
                .desired_demurrage_percent_per_day,
            self.node
                .config
                .chain_spec
                .params
                .desired_average_mining_time_in_ms,
        );
        txn.commit()?;
        let pending_txs = pending_txs.iter().map(|(_, v)| v.clone()).collect();
        let pending_bxs = pending_bxs.iter().map(|(_, v)| v.clone()).collect();
        self.mine(
            parent_block_header,
            pending_txs,
            pending_bxs,
            miner_behavior,
            network_difficulty,
            vdf_proof,
            vdf_difficulty,
        )
        .await
    }

    /// Mine a block when all requirements are gathered.
    /// (and a convenient function for tests to mine in a time independant way)
    /// 1. Prepare an open block
    /// 2. Execute block to get transactions and behaviors receipts
    /// 3. Prepare work and its VDF from block params time delay
    /// 4. Run VDF
    /// 5. Close block
    pub async fn mine(
        &mut self,
        parent_block_header: BlockHeader,
        pending_txs: Vec<MessageWithSignature>,
        mut pending_bxs: Vec<BehaviorWithSignature>,
        miner_behavior: BehaviorWithSignature,
        network_difficulty: U256,
        vdf_proof: Vec<u8>,
        vdf_difficulty: u64,
    ) -> anyhow::Result<ExecutedBlock> {
        pending_bxs.push(miner_behavior.clone());

        let open_block = self.prepare_block(
            pending_txs,
            pending_bxs,
            parent_block_header.clone(),
            network_difficulty,
        )?;

        self.mine_block(
            miner_behavior,
            open_block,
            parent_block_header,
            vdf_proof,
            vdf_difficulty,
        )
        .await
    }

    /// Mine a block from a miner's behavior
    ///
    /// Prepare work and its VDF from block params time delay
    /// Run VDF and close block with results
    #[allow(unused_assignments)]
    pub async fn mine_block(
        &self,
        miner_behavior: BehaviorWithSignature,
        open_block: OpenBlock,
        parent_block_header: BlockHeader,
        proof: Vec<u8>,
        vdf_difficulty: u64,
    ) -> anyhow::Result<ExecutedBlock> {
        let db = self.db.clone();
        let mut block_for_storage = open_block.clone().block;
        block_for_storage.ommers.truncate(2);
        // let ommers = ArrayVec::<&BlockHeader, 2>::from_iter(block_for_storage.ommers.iter());
        let mut ommers: ArrayVec<BlockHeader, 2> = ArrayVec::new();
        for ommer in block_for_storage.ommers {
            ommers.push(ommer);
        }

        let txn = db.begin_mutable().unwrap();

        let mut addresses: Vec<Address> = Vec::new();
        let mut accounts: Vec<Account> = Vec::new();

        let accounts_cursor = txn.cursor(tables::Account)?;
        let try_table_values: Vec<anyhow::Result<(Address, Account)>> =
            accounts_cursor.walk(None).collect_vec();
        let mut values: Vec<(Address, Account)> = vec![];
        for value in try_table_values {
            values.push(value.unwrap());
        }
        let values = values.into_iter().unique();
        for v in values {
            addresses.push(v.0);
            accounts.push(v.1);
        }

        let mut in_memory_state = InMemoryState::default();
        // let mut state = IntraBlockState::new(&mut in_memory_state);

        if addresses.len() != accounts.len() {
            return Err(anyhow!(DuoError::Validation(CantComputeStateRoot)));
        }
        for i in 0..addresses.len() {
            in_memory_state.update_account(addresses[i], None, Some(accounts[i]));
            // state.write_to_state(block_number)
            // TODO: no smart contract supported yet, so no need to update storage
            // state_root.update_storage(addresses[i], location, initial, current);
        }
        trace!(
            "State root number of accounts {:#?} and addresses: {:#?}",
            in_memory_state.number_of_accounts(),
            addresses
        );
        let state_root = in_memory_state.state_root_hash();

        let mined_block = open_block.close(
            state_root,
            parent_block_header.hash().as_bytes().try_into().unwrap(),
            vdf_difficulty,
            proof.try_into().unwrap(),
        )?;

        let block_number = mined_block.header.number;
        let header_hash = mined_block.header.hash();

        debug!(
            "Saving mined block #{:?} to DB ({:?}) ... ",
            block_number, header_hash
        );

        // call blockchain.rs/insert_block()
        // let db = self.db.clone();
        // let txn = db.begin_mutable().unwrap();
        let mut canonical_header_cursor = txn.cursor(tables::CanonicalHeader)?;
        let mut header_cursor = txn.cursor(tables::Header)?;
        let mut executed_transaction_cursor = txn.cursor(tables::BlockTransaction)?;
        let mut executed_behavior_cursor = txn.cursor(tables::BlockBehavior)?;
        let mut staged_sync_cursor = txn.cursor(tables::SyncStage)?;
        let mut block_body_cursor = txn.cursor(tables::BlockBody)?;
        let mut tx_sender_cursor = txn.cursor(tables::TxSender)?;
        let mut bx_sender_cursor = txn.cursor(tables::BxSender)?;

        let mut pending_transaction_cursor = txn.cursor(tables::PendingTransaction)?;
        let mut pending_behavior_cursor = txn.cursor(tables::PendingBehavior)?;
        let mut total_gas_cursor = txn.cursor(tables::TotalGas)?;
        let mut total_tx_cursor = txn.cursor(tables::TotalTx)?;

        canonical_header_cursor.append(block_number, header_hash)?;
        header_cursor.append(block_number, mined_block.header.clone())?;
        txn.set(tables::HeaderNumber, header_hash, block_number)?;
        let mut tx_index = TxIndex(0);
        let latest_transaction = executed_transaction_cursor.last()?;
        if latest_transaction.is_some() {
            tx_index = latest_transaction.unwrap().0;
            tx_index.0 += 1;
        }
        let len = block_for_storage.transactions.len();
        let mut all_tx_addresses_in_block: Vec<Address> = vec![];
        let mut senders = vec![];
        for transaction in &block_for_storage.transactions {
            senders.push(transaction.recover_sender()?);
        }
        let senders = senders.into_iter().unique().collect();

        // Added transaction for addresses
        for transaction in block_for_storage.transactions {
            let receiver = transaction
                .message
                .action()
                .into_address()
                .unwrap_or(Address::default());
            executed_transaction_cursor.append(tx_index, transaction.clone())?;
            all_tx_addresses_in_block.push(transaction.recover_sender()?);
            all_tx_addresses_in_block.push(receiver);

            let pending_transaction = pending_transaction_cursor.seek(tx_index)?;
            if pending_transaction.is_some() {
                pending_transaction_cursor.delete_current()?;
            }
            tx_index += 1_u8;
        }

        let addresses_for_tx: Vec<Address> =
            all_tx_addresses_in_block.into_iter().unique().collect();
        for address in addresses_for_tx {
            let existing_block_number_for_address =
                txn.get(tables::TransactionsBySender, address.clone())?;
            if existing_block_number_for_address.is_some() {
                let mut new_entry = existing_block_number_for_address.unwrap();
                new_entry.push(block_number);
                txn.set(tables::TransactionsBySender, address, new_entry)
                    .expect("Failed to set new value in db");
            } else {
                let mut new_entry = vec![];
                new_entry.push(block_number);
                txn.set(tables::TransactionsBySender, address, new_entry)
                    .expect("Failed to set new value in db");
            }
        }

        let _ = tx_sender_cursor.append(block_number, senders);

        let behavior_len = block_for_storage.behaviors.len();
        let mut bx_index = TxIndex(0);
        let latest_behavior = executed_behavior_cursor.last()?;
        if latest_behavior.is_some() {
            bx_index = latest_behavior.unwrap().0;
            bx_index.0 += 1;
        }

        let mut senders = vec![];
        let mut all_bx_addresses_in_block: Vec<Address> = vec![];
        for behavior in &block_for_storage.behaviors {
            senders.push(behavior.recover_sender()?);
        }
        let senders = senders.into_iter().unique().collect();

        for behavior in block_for_storage.behaviors {
            executed_behavior_cursor.append(bx_index, behavior.clone())?;
            all_bx_addresses_in_block.push(behavior.recover_sender()?);

            if behavior.hash() != miner_behavior.clone().hash() {
                let pending_behavior = pending_behavior_cursor.seek(bx_index)?;
                if pending_behavior.is_some() {
                    pending_behavior_cursor.delete_current()?
                }
            }
            bx_index += 1_u8;
        }

        let addresses_for_bx: Vec<Address> =
            all_bx_addresses_in_block.into_iter().unique().collect();
        for address in addresses_for_bx {
            let existing_block_number_for_address =
                txn.get(tables::BehaviorsBySender, address.clone())?;
            if existing_block_number_for_address.is_some() {
                let mut new_entry = existing_block_number_for_address.unwrap();
                new_entry.push(block_number);
                txn.set(tables::BehaviorsBySender, address, new_entry)
                    .expect("Failed to set new value in db");
            } else {
                let mut new_entry = vec![];
                new_entry.push(block_number);
                txn.set(tables::BehaviorsBySender, address, new_entry)
                    .expect("Failed to set new value in db");
            }
        }
        bx_sender_cursor.append(block_number, senders)?;

        let storage_body = BodyForStorage {
            base_tx_id: TxIndex(tx_index.0 - (len as u64)),
            base_bx_id: TxIndex(bx_index.0 - (behavior_len as u64)),
            tx_amount: len as u64,
            bx_amount: behavior_len as u64,
            ommers: ommers.clone(),
        };

        let mut total_gas = 0_u64;
        total_gas = total_gas_cursor.last()?.map(|(_, v)| v).unwrap_or(0);
        total_gas += mined_block.header.gas_used;
        let _ = total_gas_cursor.append(block_number, total_gas);

        let mut total_tx = 0_u64;
        total_tx = total_tx_cursor.last()?.map(|(_, v)| v).unwrap_or(0);
        total_tx += mined_block.transactions.len().as_u64();
        let _ = total_tx_cursor.append(block_number, total_tx);

        let mut cursor_td = txn.cursor(tables::HeadersTotalDifficulty)?;
        let mut td = cursor_td.last()?.map(|(_, v)| v).unwrap_or(U256::ZERO);
        td += mined_block.header.difficulty;
        cursor_td.append(block_number, td.clone())?;
        let new_status = Status {
            height: block_number,
            hash: header_hash,
            total_difficulty: H256::from_uint(&primitive_types::U256::from(td.clone().as_u64())),
        };
        self.node.update_chain_head(Some(new_status)).await;

        block_body_cursor.append(block_number, storage_body)?;
        staged_sync_cursor.put(stages::ACCOUNT_HISTORY_INDEX, block_number)?;
        staged_sync_cursor.put(stages::BLOCK_HASHES, block_number)?;
        staged_sync_cursor.put(stages::BODIES, block_number)?;
        staged_sync_cursor.put(stages::CALL_TRACES, block_number)?;
        staged_sync_cursor.put(stages::EXECUTION, block_number)?;
        staged_sync_cursor.put(stages::FINISH, block_number)?;
        staged_sync_cursor.put(stages::HASH_STATE, block_number)?;
        staged_sync_cursor.put(stages::HEADERS, block_number)?;
        staged_sync_cursor.put(stages::INTERMEDIATE_HASHES, block_number)?;
        staged_sync_cursor.put(stages::PENDING_LOOKUP, block_number)?;
        staged_sync_cursor.put(stages::SENDERS, block_number)?;
        staged_sync_cursor.put(stages::STORAGE_HISTORY_INDEX, block_number)?;
        staged_sync_cursor.put(stages::TOTAL_GAS_INDEX, block_number)?;
        staged_sync_cursor.put(stages::TOTAL_TX_INDEX, block_number)?;
        staged_sync_cursor.put(stages::LOG_ADDRESS_INDEX, block_number)?;
        staged_sync_cursor.put(stages::LOG_TOPIC_INDEX, block_number)?;
        txn.commit()?;

        // Disable NewBlock message for tests
        if self.options.strategy != MiningStrategy::InternalTests {
            tokio::spawn({
                let node = self.node.clone();
                let block_to_send = mined_block.clone();
                let ommers = ommers.clone();
                async move {
                    let msg = p2p::types::Message::NewBlock(Box::new(NewBlock {
                        block: Block {
                            header: block_to_send.header.clone(),
                            transactions: block_to_send.transactions,
                            behaviors: block_to_send.behaviors,
                            ommers,
                        },
                        total_difficulty: td.as_u128(),
                    }));

                    node.send_message(msg, PeerFilter::All).await;
                }
            });
        }
        info!(
            "Mined block #{:?} (including {:?} txs, {:?} bxs)",
            block_number, len, behavior_len
        );

        Ok(mined_block)
    }

    /// Starting with the miner's behavior, tries to compute a VDF before anyone else produces a valid
    /// block on the chain. If it fails to be the first to produce it it returns an error.
    async fn compute_vdf_parallel(
        &self,
        parent_block_header: &BlockHeader,
        vdf_difficulty: u64,
    ) -> anyhow::Result<Vec<u8>> {
        let db = self.db.clone();
        let _tx = db.begin().unwrap();

        let parent_block_number = parent_block_header.number.0;
        let mut chain_tip = self.node.chain_tip.clone();
        let challenge: Vec<u8> = Vec::from(parent_block_header.hash().to_fixed_bytes());
        let execution_result: anyhow::Result<Vec<u8>> = tokio::task::spawn(async move {
            let _res = tokio::select! {
                _ = async {
                    loop {
                            let _ = chain_tip.changed().await;

                            let fork_choice = *chain_tip.borrow();
                            if parent_block_number < fork_choice.0.0
                            {
                                break;
                            }
                        };
                } => {
                    return Err(anyhow!(MinerError::DidntProduceOnTime));
                },
                res = vdf(challenge, vdf_difficulty) => {

                    return Ok(res.unwrap());
                }
            };
        })
        .await
        .unwrap();
        if execution_result.is_err() {
            return Err(anyhow!(MinerError::DidntProduceOnTime));
        }
        let proof = execution_result.unwrap();
        Ok(proof)
    }

    /// Prepare to block to be mine in the form of an OpenBlock.
    /// Perform validations on pending transactions and behaviors and execute them.
    pub fn prepare_block(
        &mut self,
        pending_txs: Vec<MessageWithSignature>,
        pending_bxs: Vec<BehaviorWithSignature>,
        parent_block_header: BlockHeader,
        network_difficulty: U256,
    ) -> anyhow::Result<OpenBlock> {
        // TODO get ommers from DB (but not stored yet)
        let ommers: ArrayVec<BlockHeader, 2> = Default::default();

        let txn = self.db.begin()?;
        // TODO Check if a block is already in preparation
        // TODO remove pending tx, bx already included in others blocks ??

        let transactions: Vec<MessageWithSender> = pending_txs
            .clone()
            .into_iter()
            .map(|tx| {
                let sender = tx.recover_sender().map_err(|e| anyhow!(e)).unwrap();

                MessageWithSender {
                    message: tx.message,
                    sender,
                }
            })
            .collect();

        let behaviors: Vec<BehaviorWithSender> = pending_bxs
            .clone()
            .into_iter()
            .map(|bx| {
                let sender = bx.recover_sender().map_err(|e| anyhow!(e)).unwrap();

                BehaviorWithSender {
                    behavior: bx.behavior,
                    sender,
                }
            })
            .collect();

        // Create open block
        let mut open_block = OpenBlock::new(
            &parent_block_header,
            self.params.read().author,
            self.params.read().extra_data.clone(),
            ommers.to_vec(),
        );

        open_block.set_difficulty(network_difficulty);

        //TODO : temporary fix for sync and state_root computation, refactor so close function does not lose its semantic
        open_block.set_transactions(pending_txs);
        open_block.set_behaviors(pending_bxs);

        open_block.block.header.behavior_total_quantity = open_block
            .block
            .behaviors
            .clone()
            .into_iter()
            .fold(U256::ZERO, |sum, b: BehaviorWithSignature| {
                sum.add(b.quantify())
            });

        open_block.set_timestamp_from_parent_block(parent_block_header.timestamp);

        // Execute block
        let txn = self.db.begin_mutable()?;
        let mut buffer = Buffer::new(&txn, Some(parent_block_header.number));
        // Create tmp block body
        let block_body = BlockBodyWithSenders {
            transactions: transactions.clone(),
            behaviors: behaviors.clone(),
            ommers: ommers.clone(),
        };
        let binding = open_block.get_header();
        let processor = ExecutionProcessor::new(
            &mut buffer,
            &mut self.tracer,
            self.analysis_cache,
            &mut *self.engine,
            &binding,
            &block_body,
            self.block_spec,
        );
        // Execute block without header checks
        let receipts_result = processor.execute_without_check_and_write_block();
        if receipts_result.is_err() {
            let _ = reverse_demurrage_for_block_on_all_account(
                &txn,
                self.block_spec.params.desired_demurrage_percent_per_day,
                self.block_spec.params.desired_average_mining_time_in_ms,
            );
            txn.commit()?;
            return Err(anyhow!("Couldn't execute block"));
        }
        let receipts_result = receipts_result.unwrap();
        // println!("Block preparation receipts: {:?}", receipts);
        open_block.set_receipts(receipts_result);

        buffer.write_to_db()?;
        debug!(
            "Block prepared to be mine ({:?} txs, {:?} bxs, {:?} ommers)",
            transactions.len(),
            behaviors.len(),
            ommers.len()
        );
        txn.commit()?;
        Ok(open_block)
    }
}

/// Prepare work
/// Compute the time delay for the VDF from block params
/// ---
/// Note: useful in case of multiple miner behaviors to select the smaller time delay
pub fn prepare_work(parent_hash: H256, pob_m: Behavior, network_difficulty: u64) -> u64 {
    let hashed_parent_block_and_pob_m =
        keccak256([parent_hash.as_bytes(), pob_m.hash().as_bytes()].concat());

    compute_vdf_difficulty(
        network_difficulty,
        hashed_parent_block_and_pob_m,
        pob_m.quantity,
    )
}

/// Clear pool from invalid behaviors
pub fn clean_bx_pool<SE: EnvironmentKind>(
    txn: &MdbxTransaction<RW, SE>,
    block_spec: &BlockExecutionSpec,
    now: u64,
) -> anyhow::Result<()> {
    let mut iterator = txn.cursor(tables::PendingBehavior)?.walk(None);
    let mut pending_behavior_cursor = txn.cursor(tables::PendingBehavior)?;
    (loop {
        let value = iterator.next();
        if value.is_none() {
            break;
        } else if value.is_some() {
            let (index, behavior) = value.unwrap().unwrap();
            if check_behavior_expiration(block_spec, now.clone(), behavior.behavior.clone())
                .is_err()
                || behavior.input.is_empty()
            {
                pending_behavior_cursor
                    .seek(index)
                    .expect("Did not find entry for key");
                pending_behavior_cursor.delete_current()?;
                warn!("Invalid behavior : {:?}, removed from pool", &behavior);
            }
        }
    });
    Ok(())
}

/// Get the latest header of the blockchain
pub fn get_latest_parent_header<K: TransactionKind, SE: EnvironmentKind>(
    txn: &MdbxTransaction<K, SE>,
) -> anyhow::Result<BlockHeader> {
    let block_number = helpers::resolve_block_number(txn, BlockNumber::Latest)?;

    // .ok_or_else(|| format_err!("No canonical hash found for block {}", block_number))?;
    let block_hash = accessors::chain::canonical_hash::read(txn, block_number)?
        .ok_or_else(|| format_err!("No canonical hash found for block {}", block_number))?;

    accessors::chain::header::read(txn, block_number)?
        .ok_or_else(|| format_err!("Header not found: {}/{:?}", block_number, block_hash))
}

/// Check Transactions and Behaviors validity:
/// - min tx value for each transaction
/// - block size
/// - tx/bx ratio
/// - behavior expiration
/// - prevalidate every transaction and behavior
pub fn check_pending_tx_bx<SE: EnvironmentKind>(
    db: Arc<MdbxWithDirHandle<SE>>,
    block_spec: &BlockExecutionSpec,
    block_header: BlockHeader,
    timestamp: u64,
    pending_txs: Vec<(TxIndex, MessageWithSignature)>,
    pending_bxs: Vec<(TxIndex, BehaviorWithSignature)>,
) -> anyhow::Result<()> {
    check_block_size(pending_txs.clone(), pending_bxs.clone())?;
    check_block_txbx_ratio(pending_txs.clone(), pending_bxs.clone())?;

    for (index, tx) in pending_txs {
        validate_transaction(db.clone(), &tx, block_spec.params.chain_id, &index)?;
        let _tx_gas_fees = check_min_tx_value_sent(block_spec, tx.message)?;
    }
    // TODO reject invalid tx & notify pool?

    // Verify pending BXs
    for (index, bx) in pending_bxs {
        validate_behavior(
            db.clone(),
            &bx,
            bx.chain_id.clone(),
            &index,
            block_spec,
            timestamp,
            &block_header,
        )?;
    }
    // TODO reject invalid bx & notify pool?
    Ok(())
}

/// Check that the tx value is greater than the minimum expected
///
/// From whitepaper:
///  fee_rate*min_tx_value_sent > transaction reward > min_coin
pub fn check_min_tx_value_sent(
    block_spec: &BlockExecutionSpec,
    message: Message,
) -> anyhow::Result<U256> {
    let tx_gas_fees = message
        .value()
        .mul(&U256::new(
            block_spec.params.transaction_fees_percentage.into(),
        ))
        .div(U256::new(10_000_u128));

    if tx_gas_fees.ge(&block_spec.params.miner_fixed_reward_per_transaction) {
        return Ok(tx_gas_fees);
    }
    Err(anyhow!(crate::consensus::ValidationError::TxValueTooLow {
        min: block_spec.params.miner_fixed_reward_per_transaction,
        got: tx_gas_fees,
    },))
}

pub const MAX_BXS_PER_BLOCK: usize = 100_000;

/// Check block size
/// TODO: no size defined yet
pub fn check_block_size(
    _txs: Vec<(TxIndex, MessageWithSignature)>,
    bxs: Vec<(TxIndex, BehaviorWithSignature)>,
) -> anyhow::Result<()> {
    // Only limit bxs are txs are limited by txbx ratio
    if bxs.len() > MAX_BXS_PER_BLOCK {
        return Err(anyhow!(crate::consensus::ValidationError::BlockTooBig()));
    }
    Ok(())
}

pub fn check_block_size_for_senders(
    _txs: Vec<MessageWithSender>,
    bxs: Vec<BehaviorWithSender>,
) -> anyhow::Result<()> {
    // Only limit bxs are txs are limited by txbx ratio
    if bxs.len() > MAX_BXS_PER_BLOCK {
        return Err(anyhow!(crate::consensus::ValidationError::BlockTooBig()));
    }
    Ok(())
}

/// Check the ratio between transactions and behaviors in a block.
/// Avoid miners to select only most profitable txs or bxs and exclude the rest.
pub fn check_block_txbx_ratio(
    _txs: Vec<(TxIndex, MessageWithSignature)>,
    _bxs: Vec<(TxIndex, BehaviorWithSignature)>,
) -> anyhow::Result<()> {
    // if txs.len() > bxs.len() {
    //     return Err(DuoError::Validation(
    //         crate::consensus::ValidationError::BadTxBxRatio {
    //             tx_len: txs.len().as_u64(),
    //             bx_len: bxs.len().as_u64(),
    //         },
    //     ));
    // }
    Ok(())
}

pub fn check_block_txbx_ratio_for_senders(
    _txs: Vec<MessageWithSender>,
    _bxs: Vec<BehaviorWithSender>,
) -> anyhow::Result<()> {
    // if txs.len() > bxs.len() {
    //     return Err(DuoError::Validation(
    //         crate::consensus::ValidationError::BadTxBxRatio {
    //             tx_len: txs.len().as_u64(),
    //             bx_len: bxs.len().as_u64(),
    //         },
    //     ));
    // }
    Ok(())
}

/// Check behavior time in the past and future
pub fn check_behavior_expiration(
    block_spec: &BlockExecutionSpec,
    timestamp: u64,
    behavior: Behavior,
) -> anyhow::Result<()> {
    if behavior.timestamp > timestamp {
        return Err(anyhow!(crate::consensus::ValidationError::FutureBlock {
            now: timestamp,
            got: behavior.timestamp,
        }));
    }
    if (timestamp - behavior.timestamp) > block_spec.params.expiration_time_limit {
        return Err(anyhow!(crate::consensus::ValidationError::PoBError(
            PoBError::ExpiredBehaviorByTimestamp {
                now: timestamp,
                got: behavior.timestamp,
            },
        )));
    }
    Ok(())
}

/// Check that a behavior will be valid from a block header's timestamp perspective (usually latest block)
pub fn check_behavior_expiration_from_block(
    block_spec: &BlockExecutionSpec,
    block_header: BlockHeader,
    behavior: Behavior,
) -> anyhow::Result<()> {
    if block_header.number > 0
        && (behavior.timestamp + block_spec.params.expiration_time_limit < block_header.timestamp)
    {
        return Err(anyhow!(crate::consensus::ValidationError::PoBError(
            PoBError::ExpiredBehaviorByBlock {
                current: block_header.number,
                limit: 0
            },
        )));
    }
    Ok(())
}

// Only check latests blocks as other will be expired
pub fn check_behavior_for_duplicates<SE: EnvironmentKind>(
    txn: &MdbxTransaction<RO, SE>,
    behavior: Behavior,
) -> anyhow::Result<()> {
    // TODO Check behaviors' input data

    let result = bl::read(txn, behavior.hash())?;
    if result == None {
        Ok(())
    } else {
        return Err(anyhow!(crate::consensus::ValidationError::PoBError(
            PoBError::DuplicateBehavior {
                block_number: result.unwrap()
            },
        )));
    }
}

#[cfg(test)]
mod tests {
    use std::{ops::Sub, sync::Arc};

    use bytes::Bytes;
    use bytes_literal::bytes;
    use ethereum_types::BloomInput;
    use hex_literal::hex;
    use mdbx::WriteMap;

    use crate::{
        accessors::*,
        consensus::{
            demurrage::demurrage::{compute_multiplier_factor_per_block, compute_new_balance},
            engine_factory,
        },
        execution::tracer::NoopTracer,
        kv::{new_mem_chaindata, tables},
        models::{behavior::BehaviorSignature, *},
        res::chainspec::{CLERMONT, ECOMOBICOIN, LOCAL},
        stages, InMemoryState, StateWriter,
    };

    use super::*;

    const CHAIN_ID_CLERMONT: ChainId = ChainId(6363);

    const BEHAVIOR_INPUT_EXAMPLE: Bytes = bytes!("015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240");

    #[tokio::test]
    async fn start_from_pob_m_empty_block_should_work() {
        // ARRANGE
        // Create db
        let db: Arc<MdbxWithDirHandle<WriteMap>> = Arc::new(new_mem_chaindata().unwrap());
        let txn = db.begin_mutable().unwrap();

        // Set sync finished to block 0
        txn.set(tables::SyncStage, stages::FINISH, BlockNumber(0))
            .unwrap();

        // Add balances to test accounts
        let sender = Address::from(hex!("ab5ed3862a756ae7ac3c23fb9f190699d9065f7b"));
        let recipient1 = Address::from(hex!("f4148309cc30f2dd4ba117122cad6be1e3ba0e2b"));
        let mut state = InMemoryState::default();
        let sender_account = Account {
            balance: ETHER.into(),
            ..Default::default()
        };
        state.update_account(sender, None, Some(sender_account));
        txn.set(tables::Account, sender, sender_account).unwrap();

        let miner = Address::from(hex!("2c7536e3605d9c16a7a3d7b1898e529396a65c23"));
        let mut state = InMemoryState::default();
        let miner_account = Account {
            balance: ETHER.into(),
            ..Default::default()
        };
        state.update_account(miner, None, Some(miner_account));
        txn.set(tables::Account, miner, miner_account).unwrap();

        // Add block to db
        let block1 = BodyForStorage {
            base_tx_id: 1.into(),
            base_bx_id: 0.into(),
            tx_amount: 1,
            bx_amount: 0,
            ommers: Default::default(),
        };
        let tx1_1 = MessageWithSignature {
            message: Message::Legacy {
                chain_id: Some(CHAIN_ID_CLERMONT),
                nonce: 0,
                gas_price: 1_000_000.as_u256(),
                gas_limit: 0,
                // gas_limit: 21_000,
                action: TransactionAction::Call(recipient1),
                value: 1_000_000.as_u256(),
                input: Bytes::new(),
            },
            signature: MessageSignature::new(
                false,
                H256::from(hex!(
                    "11d244ae19e3bb96d1bb864aa761d48e957984a154329f0de757cd105f9c7ac4"
                )),
                H256::from(hex!(
                    "0e3828d13eed24036941eb5f7fd65de57aad1184342f2244130d2941554342ba"
                )),
            )
            .unwrap(),
        };
        let mut parent_block_header = BlockHeader::default();
        parent_block_header.timestamp = 1679068855;
        parent_block_header.difficulty = U256::new(10);
        parent_block_header.number = BlockNumber(0);
        parent_block_header.parent_hash = H256::zero();
        txn.set(tables::Header, 0.into(), parent_block_header.clone())
            .unwrap();
        chain::tx_pending::write(&txn, block1.base_tx_id, &[tx1_1]).unwrap();
        chain::storage_body::write(&txn, 0, &block1).unwrap();
        txn.set(
            tables::CanonicalHeader,
            0.into(),
            parent_block_header.hash(),
        )
        .unwrap();
        txn.commit().unwrap();

        // Create chain spec & miner
        let chain_spec = CLERMONT.clone();

        let mut analysis_cache = AnalysisCache::default();
        let mut tracer = NoopTracer;
        let node = Arc::new(Node::default());
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        let engine = engine_factory(None, chain_spec, None).unwrap();

        let mut miner = Miner::<WriteMap>::new(
            AuthoringParams::default(),
            MinerOptions {
                strategy: MiningStrategy::InternalTests,
            },
            &mut tracer,
            &mut analysis_cache,
            engine,
            &block_spec,
            db.clone(),
            node,
        );

        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        // Create fake miner behavior
        let miner_behavior = BehaviorWithSignature {
            behavior: Behavior {
                chain_id: CHAIN_ID_CLERMONT,
                timestamp: now - 2 * 3600,
                quantity: U256::from((100_000u64 << 53) / 6500),
                input: BEHAVIOR_INPUT_EXAMPLE,
            },
            signature: BehaviorSignature::new(
                true,
                hex!("60b75b096728dcfa81a294c2d795b19988099bb1a1c856f044186176dc074638"),
                hex!("7d77e2cbd1a18705337e4a38a94a51694b98261d4e8dab7956f38f2a6206f106"),
            )
            .unwrap(),
        };

        // ACT
        let mined_block_result = miner.start_from_pob_m(miner_behavior);
        // ASSERT
        let mined_block = mined_block_result.await.unwrap();
        assert_eq!(
            mined_block.header.number.0,
            parent_block_header.number.0 + 1
        );
        // println!("Mined block: {:?}", mined_block);
        // assert_eq!(parent_block_header.hash(), mined_block.header.parent_hash);
        assert_eq!(Address::zero(), mined_block.header.beneficiary);
        assert_eq!(Bytes::default(), mined_block.header.extra_data);
        assert_ne!(H256::zero(), mined_block.header.transactions_root);
        // assert_ne!(0, mined_block.header.transactions_number);
        assert_ne!(H256::zero(), mined_block.header.behaviors_root);
        assert_eq!(
            U256::from((100_000u64 << 53) / 6500),
            mined_block.header.behavior_total_quantity
        );
        assert_ne!(Bloom::zero(), mined_block.header.behaviors_bloom);
        assert_ne!(Bloom::zero(), mined_block.header.logs_bloom);
        assert_ne!(H256::zero(), mined_block.header.receipts_root);
        assert_ne!(0, mined_block.header.timestamp);
        assert_ne!(U256::ZERO, mined_block.header.difficulty);
        // assert_ne!(Address::zero(), executed_block.header.beneficiary);
        assert_eq!(7357, mined_block.header.vdf_difficulty);
        assert_ne!([0 as u8; 32], mined_block.header.vdf_proof_challenge);
        // Proof with zeroes as dry run enabled
        assert_ne!([0 as u8; 516], mined_block.header.vdf_proof_result);
        assert_ne!(H256::zero(), mined_block.header.state_root);

        // check DB update
        let txn = db.begin().unwrap();
        // let canonical_header = txn.get(tables::CanonicalHeader, key);
        let db_block_body = txn.get(tables::BlockBody, mined_block.header.number);
        assert!(db_block_body.is_ok());

        let db_canonical_header = txn.get(tables::CanonicalHeader, mined_block.header.number);
        assert_eq!(
            db_canonical_header.unwrap().unwrap(),
            mined_block.header.hash()
        );
        // This is how RPC get the block number.
        let db_block_number_rpc = txn.get(tables::SyncStage, stages::FINISH).unwrap();
        assert_eq!(db_block_number_rpc.unwrap().0, mined_block.header.number.0);
    }

    #[tokio::test]
    async fn start_from_pob_m_should_work() {
        // ARRANGE
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        // Create db
        let db = Arc::new(new_mem_chaindata().unwrap());
        let txn = db.begin_mutable().unwrap();

        // Set sync finished to block 0
        txn.set(tables::SyncStage, stages::FINISH, BlockNumber(0))
            .unwrap();

        // Create miner behavior
        let bx_m_quantity = U256::from(210000u128);
        let miner_behavior = BehaviorWithSignature {
            behavior: Behavior {
                chain_id: CHAIN_ID_CLERMONT,
                timestamp: now - 2 * 3600,
                // timestamp: 1694596380,
                quantity: bx_m_quantity,
                input: BEHAVIOR_INPUT_EXAMPLE,
            },
            signature: BehaviorSignature::new(
                true,
                hex!("60b75b096728dcfa81a294c2d795b19988099bb1a1c856f044186176dc074638"),
                hex!("7d77e2cbd1a18705337e4a38a94a51694b98261d4e8dab7956f38f2a6206f106"),
            )
            .unwrap(),
        };

        // Add balances to test accounts
        let tx_sender = Address::from(hex!("cfedc0d75fb4c5efe273a98c54ed693819506d95"));
        let _bx_sender = Address::from(hex!("c6ebf273e1a4ee9ed8594da8ac395297b0288729"));
        let recipient1 = Address::from(hex!("f4148309cc30f2dd4ba117122cad6be1e3ba0e2b"));
        // need funds before sending tx so clermont.ron was updated.
        // let tx_sender_account = Account {
        //     balance: ETHER.into(),
        //     ..Default::default()
        // };
        // let mut state = InMemoryState::default();
        // state.update_account(tx_sender, None, Some(tx_sender_account));
        // txn.set(tables::Account, tx_sender, tx_sender_account)
        //     .unwrap();

        // let miner_addr = Address::from(hex!("2c7536e3605d9c16a7a3d7b1898e529396a65c23"));
        let miner_addr = miner_behavior.recover_sender().unwrap();
        let miner_account = Account {
            balance: ETHER.into(),
            ..Default::default()
        };
        let mut state = InMemoryState::default();
        state.update_account(miner_addr, None, Some(miner_account));
        txn.set(tables::Account, miner_addr, miner_account).unwrap();

        // Add block in db
        let block1 = BodyForStorage {
            base_tx_id: 0.into(),
            base_bx_id: 0.into(),
            tx_amount: 1,
            bx_amount: 0,
            ommers: Default::default(),
        };
        let tx1_value = 1_000_000.as_u256();
        let tx1_1 = MessageWithSignature {
            message: Message::Legacy {
                chain_id: Some(CHAIN_ID_CLERMONT),
                nonce: 0,
                gas_price: 1_000_000.as_u256(),
                gas_limit: 0,
                // gas_limit: 21_000,
                action: TransactionAction::Call(recipient1),
                value: tx1_value,
                input: Bytes::new(),
            },
            signature: MessageSignature::new(
                false,
                H256::from(hex!(
                    "11d244ae19e3bb96d1bb864aa761d48e957984a154329f0de757cd105f9c7ac4"
                )),
                H256::from(hex!(
                    "0e3828d13eed24036941eb5f7fd65de57aad1184342f2244130d2941554342ba"
                )),
            )
            .unwrap(),
        };
        let mut parent_block_header = BlockHeader::default();
        parent_block_header.timestamp = 1679068855;
        parent_block_header.difficulty = U256::new(10);
        parent_block_header.number = BlockNumber(0);
        parent_block_header.parent_hash = H256::zero();
        txn.set(tables::Header, 0.into(), parent_block_header.clone())
            .unwrap();
        chain::tx_pending::write(&txn, block1.base_tx_id, &[tx1_1.clone()]).unwrap();
        chain::storage_body::write(&txn, 0, &block1).unwrap();
        txn.set(
            tables::CanonicalHeader,
            0.into(),
            parent_block_header.hash(),
        )
        .unwrap();
        txn.commit().unwrap();

        let txn2 = db.begin_mutable().unwrap();
        // Add tx and bx to pools
        // behavior from 0xc6ebf273e1a4ee9ed8594da8ac395297b0288729
        let bx_quantity = U256::from(100_000_u128 << 16);
        let bx1_1: BehaviorWithSignature = BehaviorWithSignature {
            behavior: Behavior {
                chain_id: CHAIN_ID_CLERMONT,
                timestamp: now - 3600,
                quantity: bx_quantity,
                input: BEHAVIOR_INPUT_EXAMPLE,
            },
            signature: BehaviorSignature::new(
                true,
                hex!("70bcb39ac6f540498c3adfdf3a23ecce5cf7b4f75b0674c157da02350edf8ed4"),
                hex!("40e997c09def486888c34e77565cce82b348d0035e2ea36bf125252f7895ff3c"),
            )
            .unwrap(),
        };
        let _ = chain::bx_pending::write(&txn2, 0, &[bx1_1.clone()]);

        txn2.commit().unwrap();

        // Create chain spec & miner
        let chain_spec = CLERMONT.clone();
        let mut analysis_cache = AnalysisCache::default();
        let mut tracer = NoopTracer;
        let node = Arc::new(Node::default());
        let block_spec = chain_spec.clone().collect_block_spec(BlockNumber(0));
        let engine = engine_factory(Some(Arc::clone(&db)), chain_spec.clone(), None).unwrap();

        let mut miner = Miner::<WriteMap>::new(
            AuthoringParams::from(AuthoringParams {
                author: miner_addr,
                extra_data: b"LIMOS".to_vec().into(),
            }),
            MinerOptions {
                strategy: MiningStrategy::InternalTests,
            },
            &mut tracer,
            &mut analysis_cache,
            engine,
            &block_spec,
            Arc::clone(&db),
            node,
        );

        // ACT
        let mined_block_result = miner.start_from_pob_m(miner_behavior).await;
        // ASSERT
        let mined_block = mined_block_result.unwrap();
        assert_eq!(
            mined_block.header.number.0,
            parent_block_header.number.0 + 1
        );
        assert_eq!(parent_block_header.hash(), mined_block.header.parent_hash);
        assert_eq!(miner_addr, mined_block.header.beneficiary);
        assert_eq!(
            <Vec<u8> as Into<Bytes>>::into(b"LIMOS".to_vec()),
            mined_block.header.extra_data
        );

        assert_eq!(2, mined_block.behaviors.len());
        assert_eq!(
            bx_quantity + bx_m_quantity,
            mined_block.header.behavior_total_quantity
        );
        assert_ne!(H256::zero(), mined_block.header.behaviors_root);
        // update me
        assert_ne!(Bloom::zero(), mined_block.header.behaviors_bloom);
        assert_ne!(Bloom::zero(), mined_block.header.logs_bloom);
        // Don't check exact difficulty as timestamp change and so do the behavior hash that is involved in difficulty computation
        assert_eq!(7357, mined_block.header.vdf_difficulty);
        // Proof with dry run enabled
        assert_ne!([0 as u8; 32], mined_block.header.vdf_proof_challenge);
        assert_ne!([0 as u8; 516], mined_block.header.vdf_proof_result);
        assert_ne!(H256::zero(), mined_block.header.transactions_root);
        assert_ne!(H256::zero(), mined_block.header.receipts_root);
        assert_ne!(0, mined_block.header.timestamp);
        assert_ne!(U256::ZERO, mined_block.header.difficulty);
        // STATE SHOULD CHANGE AS ACCOUNTS HAVE BALANCE
        assert_ne!(H256::zero(), mined_block.header.state_root);

        // ############################
        // Check stuff saved in DB
        // ############################
        let txn = db.begin().unwrap();

        // BLOCK BODY
        let db_block_body = txn
            .get(tables::BlockBody, mined_block.header.number)
            .unwrap()
            .unwrap();
        assert_eq!(
            mined_block.transactions.len() as u64,
            db_block_body.tx_amount
        );
        assert_eq!(0, db_block_body.base_tx_id.0);
        assert_eq!(ArrayVec::new(), db_block_body.ommers);

        // CANONICAL HEADER
        let db_canonical_header = txn
            .get(tables::CanonicalHeader, mined_block.header.number)
            .unwrap()
            .unwrap();
        assert_eq!(db_canonical_header, mined_block.header.hash());
        // HEADER
        let db_header = txn
            .get(tables::Header, mined_block.header.number)
            .unwrap()
            .unwrap();

        assert_eq!(mined_block.header.hash(), db_header.hash());
        assert_eq!(mined_block.header, db_header);

        // HEADER NUMBER
        let db_header_number = txn
            .get(tables::HeaderNumber, mined_block.header.hash())
            .unwrap()
            .unwrap();
        assert_eq!(mined_block.header.number, db_header_number);

        // BLOCK TX
        let db_tx = txn
            .get(tables::BlockTransaction, TxIndex::from(0))
            .unwrap()
            .unwrap();
        assert_eq!(mined_block.transactions[0], db_tx);

        // TODO add tests for all txs

        // BLOCK BX
        let db_behavior = txn
            .get(tables::BlockBehavior, TxIndex::from(0))
            .unwrap()
            .unwrap();
        assert_eq!(bx_quantity, db_behavior.quantity);
        assert_eq!(mined_block.behaviors[0], db_behavior);
        // TODO add tests for all bxs

        // SYNC STAGE
        // This is how RPC get the block number.
        let db_block_number_rpc = txn.get(tables::SyncStage, stages::FINISH).unwrap().unwrap();
        assert_eq!(db_block_number_rpc.0, mined_block.header.number.0);
        assert_eq!(db_block_number_rpc.0, parent_block_header.number.0 + 1);

        // ############################
        // Check balances
        // ############################

        let block_spec = chain_spec.collect_block_spec(parent_block_header.number);
        let multiplier_factor_per_block = compute_multiplier_factor_per_block(
            chain_spec.params.desired_demurrage_percent_per_day,
            chain_spec.params.desired_average_mining_time_in_ms,
        );
        let desired_balance = compute_new_balance(
            &multiplier_factor_per_block,
            &(U256::from(ETHER)
                // PoB_m reward for quantity (see clermont.ron)
                + bx_m_quantity.mul(U256::from(
                block_spec.miner_behavior_reward_per_quantity
            ))
                // fees on bxs included in block
                + (bx_quantity * 1000)
                // reward for 1 tx included in block
                + U256::from(chain_spec.params.miner_fixed_reward_per_transaction)),
        );
        assert_eq!(
            // Genesis balance
            desired_balance,
            txn.get(tables::Account, miner_addr)
                .unwrap()
                .unwrap()
                .balance
        );
        assert_eq!(
            U256::from(1000000_u128),
            txn.get(tables::Account, recipient1)
                .unwrap()
                .unwrap()
                .balance
        );
        // Time change sender address as it's recovered from signature. That's why we use recover_sender().
        assert_eq!(
            bx_quantity.mul(U256::from(1000000000000000000_u128)),
            txn.get(tables::Account, bx1_1.recover_sender().unwrap())
                .unwrap()
                .unwrap()
                .balance
        );
        assert_eq!(
            U256::from(ETHER)
                // Miner Tax
                .sub(100_000_u128)
                // tx value
                .sub(1_000_000_u128),
            txn.get(tables::Account, tx_sender)
                .unwrap()
                .unwrap()
                .balance
        );

        // ############################
        // Block validation for sync
        // ############################
        let mut engine = engine_factory(Some(Arc::clone(&db)), chain_spec.clone(), None).unwrap();
        // validate header
        engine
            .validate_header_parallel(&mined_block.header)
            .unwrap();
        engine
            .validate_block_header(&mined_block.header, &parent_block_header, true)
            .unwrap();
        // check block
        let block_body_w_senders = &BlockBodyWithSenders {
            transactions: mined_block
                .transactions
                .iter()
                .map(|tx| {
                    let sender = tx.recover_sender()?;
                    Ok(MessageWithSender {
                        message: tx.message.clone(),
                        sender,
                    })
                })
                .collect::<anyhow::Result<_>>()
                .unwrap(),
            behaviors: mined_block
                .behaviors
                .iter()
                .map(|bx| {
                    let sender = bx.recover_sender()?;
                    Ok(BehaviorWithSender {
                        behavior: bx.behavior.clone(),
                        sender,
                    })
                })
                .collect::<anyhow::Result<_>>()
                .unwrap(),
            ommers: Default::default(),
        };
        let mut state = InMemoryState::default();
        let mut analysis_cache = AnalysisCache::default();
        let mut tracer = NoopTracer;
        let mut processor = ExecutionProcessor::new(
            &mut state,
            &mut tracer,
            &mut analysis_cache,
            engine.as_mut(),
            &mined_block.header,
            block_body_w_senders,
            &block_spec,
        );
        processor.execute_and_check_block().unwrap();
    }

    #[ignore = "Time-dependent test"]
    #[tokio::test]
    async fn state_root_should_work() {
        const CHAIN_ID_LOCAL: ChainId = ChainId(636363);
        // ARRANGE
        let _now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        // Create db
        let db = Arc::new(new_mem_chaindata().unwrap());
        let txn = db.begin_mutable().unwrap();

        // Set sync finished to block 0
        txn.set(tables::SyncStage, stages::FINISH, BlockNumber(0))
            .unwrap();

        // Add balances to test accounts
        let miner_addr = Address::from(hex!("57db4b909b98b6b2e0c443fc7cdbc3ae82b41c3d"));
        let sender_addr = Address::from(hex!("2c7536e3605d9c16a7a3d7b1898e529396a65c23"));
        let _tx_recipient_addr = Address::from(hex!("0000000000000000000000000000000000000000"));
        let _recipient1 = Address::from(hex!("6e04da1f76268713322d5a6e8631a0cfa41c034f"));

        let mut state = InMemoryState::default();
        let sender_account = Account {
            balance: U256::from(500000_u128),
            nonce: 0,
            ..Default::default()
        };
        state.update_account(sender_addr, None, Some(sender_account));
        txn.set(tables::Account, sender_addr, sender_account)
            .unwrap();
        let miner_account = Account {
            balance: U256::from(500000_u128),
            nonce: 0,
            ..Default::default()
        };
        state.update_account(miner_addr, None, Some(miner_account));
        txn.set(tables::Account, miner_addr, miner_account).unwrap();

        // Add genesis block in db ?
        let block1 = BodyForStorage {
            base_tx_id: 0.into(),
            base_bx_id: 0.into(),
            tx_amount: 0,
            bx_amount: 0,
            ommers: Default::default(),
        };
        let mut parent_block_header = BlockHeader::default();
        parent_block_header.timestamp = 1679068855;
        parent_block_header.difficulty = U256::new(10);
        parent_block_header.number = BlockNumber(0);
        parent_block_header.parent_hash = H256::zero();
        txn.set(tables::Header, 0.into(), parent_block_header.clone())
            .unwrap();
        chain::storage_body::write(&txn, 0, &block1).unwrap();
        txn.set(
            tables::CanonicalHeader,
            0.into(),
            parent_block_header.hash(),
        )
        .unwrap();
        txn.commit().unwrap();

        let pending_txs = vec![
            // hash: 0xc722eb323c82355d10c13180c96498cbcd8b872078765a2de7f5f5f61d17108f
            MessageWithSignature {
                message: Message::Legacy {
                    chain_id: Some(CHAIN_ID_LOCAL),
                    value: U256::from(210000_u128),
                    nonce: 0,
                    gas_price: U256::from(0_u128),
                    gas_limit: 0,
                    action: TransactionAction::Call(H160::zero()),
                    input: Bytes::default(),
                },
                signature: MessageSignature::new(
                    false,
                    hex!("d1bf4e6bcfcc5c6ebd281bcdac2082eceab2c2de0571f11e3f0c90b5ba1042a0"),
                    hex!("1075a90962faf209b72e1645ac75c33c10761cff10e6ee174abac225d8edb99c"),
                )
                .unwrap(),
            },
        ];
        // hash: 0x35c0ac85051e71332ea9436e2844bfac2de3e4f2b7bfdc722e2951b8900ae1ae
        let pending_bxs = vec![
            BehaviorWithSignature {
                behavior: Behavior {
                    chain_id: CHAIN_ID_LOCAL,
                    timestamp: 1693989270,
                    quantity: U256::from(210000_u128),
                    input: bytes!("015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240"),
                },
                signature: BehaviorSignature::new(
                    true,
                    hex!("92b8a0fabd2138c81acfc9e6ce639ef13d77f030820d368f9dd3aa5a35a2741d"),
                    hex!("75ee07aecbaa6f55f3264409761b88b515cb339e48ccde7dde2c31e8ead3d3c4"),
                )
                    .unwrap(),
            },
        ];

        // Create chain spec & miner
        let chain_spec = LOCAL.clone();

        let mut analysis_cache = AnalysisCache::default();
        let mut tracer = NoopTracer;
        let node = Arc::new(Node::default());
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        let engine = engine_factory(Some(Arc::clone(&db)), chain_spec.clone(), None).unwrap();

        let mut miner = Miner::<WriteMap>::new(
            AuthoringParams::from(AuthoringParams {
                author: miner_addr,
                extra_data: b"LIMOS_CNRS_UCA".to_vec().into(),
            }),
            MinerOptions {
                strategy: MiningStrategy::InternalTests,
            },
            &mut tracer,
            &mut analysis_cache,
            engine,
            &block_spec,
            Arc::clone(&db),
            node,
        );

        // Create fake miner behavior
        let _bx_m_quantity = U256::from((100_000u128 << 53) / 6500);
        let input: [u8; 123] = [
            1, 2, 3, 4, 184, 117, 2, 248, 114, 4, 26, 132, 89, 104, 47, 0, 132, 89, 104, 47, 13,
            130, 82, 8, 148, 97, 129, 87, 116, 56, 48, 153, 226, 72, 16, 171, 131, 42, 91, 42, 84,
            37, 193, 84, 213, 136, 41, 162, 36, 26, 246, 44, 0, 0, 128, 192, 1, 160, 89, 230, 182,
            127, 72, 251, 50, 231, 229, 112, 223, 177, 30, 4, 43, 90, 210, 229, 94, 60, 227, 206,
            156, 217, 137, 199, 224, 110, 7, 254, 234, 253, 160, 1, 107, 131, 244, 249, 128, 105,
            78, 210, 238, 228, 209, 6, 103, 36, 43, 31, 64, 220, 64, 105, 1, 179, 65, 37, 176, 8,
            211, 52, 212, 116, 105,
        ];
        let miner_behavior = BehaviorWithSignature {
            behavior: Behavior {
                chain_id: CHAIN_ID_LOCAL,
                timestamp: 1693991209,
                quantity: U256::from(2441406250_u128),
                input: Bytes::copy_from_slice(&input),
            },
            signature: BehaviorSignature::new(
                true,
                hex!("70bcb39ac6f540498c3adfdf3a23ecce5cf7b4f75b0674c157da02350edf8ed4"),
                hex!("40e997c09def486888c34e77565cce82b348d0035e2ea36bf125252f7895ff3c"),
            )
            .unwrap(),
        };

        // ACT
        let mined_block_result = miner
            .mine(
                parent_block_header,
                pending_txs,
                pending_bxs,
                miner_behavior,
                U256::ONE,
                vec![0; 516],
                0_u64,
            )
            .await;

        // ASSERT
        let block = mined_block_result.unwrap();
        // Check state_root past_progress = genesis
        assert_eq!(
            H256::from(hex!(
                //  miner state_root
                // "545f5cd5a97e865e98cfcc26362c2c0a555f1e4236f67be9cae22b73ec5f4425"
                // IntermediateHashes' state_root
                "e1298a18d8925898d1069b4a3f805e1d6726523845c72b74228caa5f4105ab6d"
            )),
            block.header.state_root
        );
        // Check state_root past_progress != genesis
    }

    #[test]
    fn prepare_block_should_work() {
        // ARRANGE
        // Create db
        let db = Arc::new(new_mem_chaindata().unwrap());

        // let db_test = Arc::clone(&db);
        // let mut txn = db_test.begin_mutable().unwrap();
        let chain_spec = CLERMONT.clone();

        let mut analysis_cache = AnalysisCache::default();
        let mut tracer = NoopTracer;
        let node = Arc::new(Node::default());
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        let engine = engine_factory(None, chain_spec.clone(), None).unwrap();

        let mut miner = Miner::<WriteMap>::new(
            AuthoringParams::default(),
            MinerOptions {
                strategy: MiningStrategy::EqualProportions,
            },
            &mut tracer,
            &mut analysis_cache,
            engine,
            &block_spec,
            db.clone(),
            node,
        );
        let pending_txs = vec![];
        let pending_bxs = vec![];
        let mut parent_block_header = BlockHeader::default();
        parent_block_header.difficulty = U256::ONE;

        // ACT
        let result = miner.prepare_block(
            pending_txs,
            pending_bxs,
            parent_block_header.clone(),
            U256::ONE,
        );
        // ASSERT
        assert!(result.is_ok());
        let open_block = result.unwrap();
        assert_eq!(1, open_block.get_number().0);
        let open_block_header = open_block.get_header().clone();
        assert_eq!(parent_block_header.hash(), open_block_header.parent_hash);
        assert_eq!(Address::zero(), open_block_header.beneficiary);
        assert_eq!(Bytes::new(), open_block_header.extra_data);
        assert_ne!(0, open_block_header.difficulty);
        assert_ne!(0, open_block_header.timestamp);
    }

    #[tokio::test]
    async fn mine_block_should_work() {
        // ARRANGE
        let pob_m = BehaviorWithSignature {
            behavior: Behavior {
                chain_id: ChainId(64),
                timestamp: 1672534861,
                quantity: U256::from((100_000u64 << 53) / 6500),
                input: BEHAVIOR_INPUT_EXAMPLE,
            },
            signature: BehaviorSignature::new(
                true,
                // TODO replace with correct signature
                hex!("70bcb39ac6f540498c3adfdf3a23ecce5cf7b4f75b0674c157da02350edf8ed4"),
                hex!("40e997c09def486888c34e77565cce82b348d0035e2ea36bf125252f7895ff3c"),
            )
            .unwrap(),
        };
        let mut parent_block_header = BlockHeader::default();
        parent_block_header.number = BlockNumber(1337);
        parent_block_header.timestamp = 1;
        parent_block_header.beneficiary = Address::random();
        parent_block_header.parent_hash = H256::random();

        let mut open_block = OpenBlock::default();
        // https://etherscan.io/block/13143465
        let transactions = vec![
            MessageWithSignature {
                message: Message::EIP1559 {
                    chain_id: CHAIN_ID_CLERMONT,
                    nonce: 20369,
                    max_priority_fee_per_gas: 0x50a3d0b5d_u64.into(),
                    max_fee_per_gas: 0x23a9e38cf8_u64.into(),
                    gas_limit: 1_200_000,
                    action: TransactionAction::Call(hex!("a57bd00134b2850b2a1c55860c9e9ea100fdd6cf").into()),
                    value: U256::ZERO,
                    input: hex!("1cff79cd000000000000000000000000aa2ec16d77cfc057fb9c516282fef9da9de1e987000000000000000000000000000000000000000000000000000000000000004000000000000000000000000000000000000000000000000000000000000001844f0c7c0a00000000000000000000000000000000000000000000000000000000000001f4000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc2000000000000000000000000a0b86991c6218b36c1d19d4a2e9eb0ce3606eb4800000000000000000000000056178a0d5f301baf6cf3e1cd53d9863437345bf9000000000000000000000000000000000000000000000000002386f26fc100000000000000000000000000000000000000000000000000a2a15d09519be00000000000000000000000000000000000000000000000daadf45a4bb347757560000000000000000000000000000000000000000000000000003453af3f6dd960000000000000000000000000000000000000003f994c7f39b6af041a3c553270000000000000000000000000000000000000000000000000000de0b6b3a76400000000000000000000000000000000000000000000000000000000000061303192000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000").to_vec().into(),
                    access_list: vec![],
                },
                signature: MessageSignature::new(false, hex!("9a8548ba3759730fe25be0412c0b183ec975d15da2f12653d5f0a2016ca01f27"), hex!("6d93f2176bfda918c06365e507c6c66a16d30b9e76d2d8e5a7f2802e3bcc6593")).unwrap(),
            }
        ];
        open_block.set_transactions(transactions);
        let behaviors = vec![BehaviorWithSignature {
            behavior: Behavior {
                chain_id: CHAIN_ID_CLERMONT,
                timestamp: 1672534861, // 2023/01/01 01:01:01
                quantity: U256::from(10_000_u128),
                input: hex!(
                    "a0712d680000000000000000000000000000000000000000000000000000000000000002"
                )
                .to_vec()
                .into(),
            },
            signature: BehaviorSignature::new(
                true,
                // TODO replace with correct signature
                hex!("70bcb39ac6f540498c3adfdf3a23ecce5cf7b4f75b0674c157da02350edf8ed4"),
                hex!("40e997c09def486888c34e77565cce82b348d0035e2ea36bf125252f7895ff3c"),
            )
            .unwrap(),
        }];
        open_block.set_behaviors(behaviors.clone());
        let receipts = vec![
            // Behavior receipt
            Receipt {
                tx_type: TxType::EcoMobiCoinBehavior,
                success: true,
                cumulative_gas_used: 0,
                bloom: Bloom::from(BloomInput::Hash(behaviors[0].hash().as_fixed_bytes())),
                logs: vec![],
            },
            Receipt {
                tx_type: TxType::EIP1559,
                success: true,
                cumulative_gas_used: 1,
                bloom: Bloom::from(BloomInput::Hash(&[1 as u8; 32])),
                logs: vec![],
            },
        ];
        open_block.set_receipts(receipts);
        open_block.set_difficulty(U256::ONE);
        open_block.set_timestamp(1);
        // Create db
        let db = Arc::new(new_mem_chaindata().unwrap());
        // Create chain spec & miner
        let chain_spec = CLERMONT.clone();

        let mut analysis_cache = AnalysisCache::default();
        let mut tracer = NoopTracer;
        let node = Arc::new(Node::default());
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        let engine = engine_factory(None, chain_spec, None).unwrap();

        let miner = Miner::<WriteMap>::new(
            AuthoringParams::default(),
            MinerOptions {
                strategy: MiningStrategy::InternalTests,
            },
            &mut tracer,
            &mut analysis_cache,
            engine,
            &block_spec,
            db.clone(),
            node,
        );

        // ACT
        let result = miner
            .mine_block(pob_m, open_block, parent_block_header, vec![0; 516], 19500)
            .await;
        // ASSERT
        assert!(result.is_ok());
        let executed_block = result.unwrap();

        assert_ne!(H256::zero(), executed_block.header.transactions_root);
        assert_ne!(H256::zero(), executed_block.header.behaviors_root);
        assert_ne!(0, executed_block.header.behavior_total_quantity);
        assert_ne!(Bloom::zero(), executed_block.header.behaviors_bloom);
        assert_ne!(Bloom::zero(), executed_block.header.logs_bloom);
        assert_ne!(H256::zero(), executed_block.header.receipts_root);
        // Done in prepare
        // assert_ne!(0, executed_block.header.timestamp);
        // assert_ne!(U256::ZERO, executed_block.header.difficulty);
        //
        // assert_ne!(Address::zero(), executed_block.header.beneficiary);
        assert_ne!(0, executed_block.header.vdf_difficulty);
        assert_ne!([0 as u8; 32], executed_block.header.vdf_proof_challenge);
        // Proof with zeroes as dry run enabled
        // assert_ne!([0 as u8; 516], executed_block.header.vdf_proof_result);
        // assert_ne!(H256::zero(), executed_block.header.state_root);
    }

    #[test]
    fn prepare_work_should_work() {
        // ARRANGE
        let parent_hash = H256::from(hex!(
            "0e3828d13eed24036941eb5f7fd65de57aad1184342f2244130d2941554342ba"
        ));
        let pob_m = Behavior {
            chain_id: ChainId(63),
            timestamp: 1672534861,
            quantity: U256::from(100_000u64),
            input: BEHAVIOR_INPUT_EXAMPLE,
        };
        let network_difficulty = 1_000_000;
        // ACT
        let result = prepare_work(parent_hash, pob_m, network_difficulty);
        // ASSERT
        assert_eq!(155, result);
    }

    #[test]
    fn check_min_tx_value_sent_test_too_low() {
        // ARRANGE
        let message = Message::Legacy {
            chain_id: Some(ChainId(63)),
            nonce: 1,
            gas_price: 1_000_000.as_u256(),
            gas_limit: 21_000,
            action: TransactionAction::Call(Address::from(hex!(
                "f4148309cc30f2dd4ba117122cad6be1e3ba0e2b"
            ))),
            value: 10.as_u256(),
            input: Bytes::new(),
        };
        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));

        // ACT
        let result = check_min_tx_value_sent(&block_spec, message);
        // ASSERT
        assert!(result.is_err())
    }

    #[test]
    fn check_min_tx_value_sent_test() {
        // ARRANGE
        let message = Message::Legacy {
            chain_id: Some(ChainId(63)),
            nonce: 1,
            gas_price: 1_000_000.as_u256(),
            gas_limit: 21_000,
            action: TransactionAction::Call(Address::from(hex!(
                "f4148309cc30f2dd4ba117122cad6be1e3ba0e2b"
            ))),
            value: U256::new(210_000),
            input: Bytes::new(),
        };
        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));

        // ACT
        let result = check_min_tx_value_sent(&block_spec, message);
        // ASSERT
        assert!(result.is_ok())
    }

    #[test]
    fn check_block_size_test() {
        // ARRANGE
        let txs = vec![MessageWithSender {
            message: Message::Legacy {
                chain_id: Some(ChainId(63)),
                nonce: 1,
                gas_price: 1_000_000.as_u256(),
                gas_limit: 21_000,
                action: TransactionAction::Call(Address::from(hex!(
                    "f4148309cc30f2dd4ba117122cad6be1e3ba0e2b"
                ))),
                value: U256::new(21_000),
                input: Bytes::new(),
            },
            sender: Address::zero(),
        }];
        let bxs = vec![];
        // ACT
        let result = check_block_size_for_senders(txs, bxs);
        // ASSERT
        assert!(result.is_ok());
    }

    #[test]
    fn check_block_size_test_too_big() {
        // ARRANGE
        let txs = vec![];
        let bxs = vec![BehaviorWithSender::default(); 200_000];
        // ACT
        let result = check_block_size_for_senders(txs, bxs);
        // ASSERT
        // assert_eq!(result.err(), ValidationError::BlockTooBig())
        assert!(result.is_err());
    }

    #[test]
    fn check_block_txbx_ratio_test() {
        // ARRANGE
        let txs = vec![MessageWithSender {
            message: Message::Legacy {
                chain_id: Some(ChainId(63)),
                nonce: 1,
                gas_price: 1_000_000.as_u256(),
                gas_limit: 21_000,
                action: TransactionAction::Call(Address::from(hex!(
                    "f4148309cc30f2dd4ba117122cad6be1e3ba0e2b"
                ))),
                value: 10.as_u256(),
                input: Bytes::new(),
            },
            sender: Address::zero(),
        }];
        let bxs = vec![BehaviorWithSender::default()];
        // ACT
        let result = check_block_txbx_ratio_for_senders(txs, bxs);
        // ASSERT
        assert!(result.is_ok());
    }

    #[ignore]
    #[test]
    fn check_block_txbx_ratio_test_bad_ratio() {
        // ARRANGE
        let txs = vec![
            MessageWithSender {
                message: Message::Legacy {
                    chain_id: Some(ChainId(63)),
                    nonce: 1,
                    gas_price: 1_000_000.as_u256(),
                    gas_limit: 21_000,
                    action: TransactionAction::Call(Address::from(hex!(
                        "f4148309cc30f2dd4ba117122cad6be1e3ba0e2b"
                    ))),
                    value: 10.as_u256(),
                    input: Bytes::new(),
                },
                sender: Address::zero(),
            },
            MessageWithSender {
                message: Message::Legacy {
                    chain_id: Some(ChainId(63)),
                    nonce: 2,
                    gas_price: 1_000_000.as_u256(),
                    gas_limit: 21_000,
                    action: TransactionAction::Call(Address::from(hex!(
                        "f4148309cc30f2dd4ba117122cad6be1e3ba0e2b"
                    ))),
                    value: 10.as_u256(),
                    input: Bytes::new(),
                },
                sender: Address::zero(),
            },
        ];
        let bxs = vec![BehaviorWithSender::default()];
        // ACT
        let result = check_block_txbx_ratio_for_senders(txs, bxs);
        // ASSERT
        assert!(result.is_err());
    }

    #[ignore]
    #[test]
    fn check_block_txbx_ratio_test_zero_bx() {
        // ARRANGE
        let txs = vec![MessageWithSender {
            message: Message::Legacy {
                chain_id: Some(ChainId(63)),
                nonce: 1,
                gas_price: 1_000_000.as_u256(),
                gas_limit: 21_000,
                action: TransactionAction::Call(Address::from(hex!(
                    "f4148309cc30f2dd4ba117122cad6be1e3ba0e2b"
                ))),
                value: 10.as_u256(),
                input: Bytes::new(),
            },
            sender: Address::zero(),
        }];
        let bxs = vec![];
        // ACT
        let result = check_block_txbx_ratio_for_senders(txs, bxs);
        // ASSERT
        assert!(result.is_err());
    }

    #[test]
    fn check_behavior_expiration_test() {
        // ARRANGE
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        let mut behavior = Behavior::default();
        behavior.timestamp = now - (12 * 3600);

        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        // ACT
        let result = check_behavior_expiration(&block_spec, now, behavior);
        // ASSERT
        assert!(result.is_ok());
    }

    #[test]
    fn check_behavior_expiration_test_expired() {
        // ARRANGE
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        let mut behavior = Behavior::default();
        behavior.timestamp = now - (25 * 3600); //a behavior 25 hours in the past
        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        // ACT
        let result = check_behavior_expiration(&block_spec, now, behavior);
        // ASSERT
        assert!(result.is_err());
    }

    #[test]
    fn check_behavior_expiration_test_in_future() {
        // ARRANGE
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        let mut behavior = Behavior::default();
        behavior.timestamp = now + 2 * 60; //a behavior 2 min in the future

        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));

        // ACT
        let result = check_behavior_expiration(&block_spec, now, behavior);
        // ASSERT
        assert!(result.is_err());
    }

    #[test]
    fn check_behavior_expiration_from_block_test_genesis() {
        // ARRANGE
        let mut parent_block_header = BlockHeader::default();
        parent_block_header.number = BlockNumber(0);
        parent_block_header.timestamp = 1696927718;

        let mut behavior = Behavior::default();
        behavior.timestamp = 1696927718;

        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        // ACT
        let result =
            check_behavior_expiration_from_block(&block_spec, parent_block_header, behavior);
        // ASSERT
        assert!(result.is_ok());
    }

    #[test]
    fn check_behavior_expiration_from_block_test() {
        // ARRANGE
        let mut parent_block_header = BlockHeader::default();
        parent_block_header.number = BlockNumber(1337);
        parent_block_header.timestamp = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();

        let mut behavior = Behavior::default();
        behavior.timestamp = parent_block_header.timestamp - (12 * 3600);

        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        // ACT
        let result =
            check_behavior_expiration_from_block(&block_spec, parent_block_header, behavior);
        // ASSERT
        assert!(result.is_ok());
    }

    #[test]
    fn check_behavior_expiration_from_block_test_expired() {
        // ARRANGE
        let mut parent_block_header = BlockHeader::default();
        parent_block_header.number = BlockNumber(1337);
        parent_block_header.timestamp = 1696927718;

        let mut behavior = Behavior::default();
        behavior.timestamp = 1696927718 - (24 * 3600) - 1;

        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        // ACT
        let result =
            check_behavior_expiration_from_block(&block_spec, parent_block_header, behavior);
        // ASSERT
        assert!(result.is_err());
    }

    #[test]
    fn check_behavior_expiration_from_block_test_old_block() {
        // ARRANGE
        let mut parent_block_header = BlockHeader::default();
        parent_block_header.number = BlockNumber(1337);
        parent_block_header.timestamp = 1696927718;

        let mut behavior = Behavior::default();
        behavior.timestamp = 1665391717;

        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        // ACT
        let result =
            check_behavior_expiration_from_block(&block_spec, parent_block_header, behavior);
        // ASSERT
        assert!(result.is_err());
    }

    #[test]
    fn check_behavior_for_duplicates_test() {
        // ARRANGE
        let db = Arc::new(new_mem_chaindata().unwrap());
        let txn: MdbxTransaction<'_, RO, WriteMap> = db.begin().unwrap();

        let mut behavior = Behavior::default();
        behavior.timestamp = 1696927718;

        // ACT
        let result = check_behavior_for_duplicates(&txn, behavior);
        // ASSERT
        assert!(result.is_ok());
    }

    #[test]
    fn check_behavior_for_duplicates_one_duplicate() {
        // ARRANGE
        let db = Arc::new(new_mem_chaindata().unwrap());
        let txn: MdbxTransaction<'_, RW, WriteMap> = db.begin_mutable().unwrap();

        let mut behavior = Behavior::default();
        behavior.timestamp = 1696927718;

        bl::write(&txn, behavior.hash(), BlockNumber(3)).unwrap();

        let _ = txn.commit();
        let txn: MdbxTransaction<'_, RO, WriteMap> = db.begin().unwrap();

        // ACT
        let result = check_behavior_for_duplicates(&txn, behavior);
        // ASSERT
        assert!(result.is_err());
    }
}
