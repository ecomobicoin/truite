use std::time::{Duration, Instant};

use ethereum_types::H256;
use ethnum::U256;
use tracing::log::debug;
use vdf::{InvalidIterations, InvalidProof, VDFParams, WesolowskiVDFParams, VDF};

use crate::h256_to_u256;

// 10 sec = 65000 on modern server CPU (E5-2630 v3 @ 2.40GHz)
// almost the same for Pietrzak & Wesolowski
const VDF_DIFFICULTY_PER_SEC: u64 = 6_500;
const INT_SIZE_BITS: u16 = 2048;

// vdf-0.1.0/src/proof_wesolowski.rs:271:5
// const MAXIMUM_ITERATIONS: u64 = (1u64 << 53) - 1;
const MAXIMUM_ITERATIONS: u64 = 800_000_000; //approximately one day

/// Compute the delay parameter in seconds of a VDF to run
pub fn compute_vdf_difficulty(network_difficulty: u64, hash: H256, quantity: U256) -> u64 {
    let hashed_part = h256_to_u256(hash).as_u64() % MAXIMUM_ITERATIONS;
    let vdf_difficulty = (network_difficulty.max(1).wrapping_mul(hashed_part)) / quantity.as_u64();
    vdf_difficulty
}

/// Launch a VDF and returns its output.
/// Ensure vdf_difficulty is not too high before launching the VDF.
/// `vdf_difficulty` for Wesolowski needs to be between **1u128 << 53** (9_007_199_254_740_992) and **usize::MAX as u64** (18_446_744_073_709_551_615usize)
pub async fn vdf<'a>(
    challenge: Vec<u8>,
    vdf_difficulty: u64,
) -> Result<Vec<u8>, InvalidIterations> {
    // An instance of the VDF.
    // TODO:  Instances can be used arbitrarily many times.
    let wesolowski_vdf = WesolowskiVDFParams(INT_SIZE_BITS).new();
    tokio::spawn(async move {
        let difficulty_check = wesolowski_vdf.check_difficulty(vdf_difficulty);
        if difficulty_check.is_err() {
            return Err(difficulty_check.err().unwrap());
        }
        debug!(
            "Solving wesolowski_vdf, estimated time: {:?} (={:?}min)",
            Duration::from_secs(vdf_difficulty / VDF_DIFFICULTY_PER_SEC),
            vdf_difficulty / VDF_DIFFICULTY_PER_SEC / 60
        );
        let start = Instant::now();
        let result = wesolowski_vdf.solve(&*challenge, vdf_difficulty);
        debug!(
            "Solved wesolowski_vdf in {:?} (estimated: {:?})",
            start.elapsed(),
            Duration::from_secs(vdf_difficulty / VDF_DIFFICULTY_PER_SEC)
        );
        result
    })
    .await
    .unwrap()
}

/// Verify a VDF output
pub fn verify(
    vdf_proof_challenge: &[u8; 32],
    vdf_difficulty: u64,
    vdf_proof_result: &[u8; 516],
) -> Result<(), InvalidProof> {
    let wesolowski_vdf = WesolowskiVDFParams(INT_SIZE_BITS).new();
    wesolowski_vdf.verify(vdf_proof_challenge, vdf_difficulty, vdf_proof_result)
}

type IterationValue = u32;
type HashOutput = [u8];

#[cfg(test)]
mod tests {
    use hex_literal::hex;

    use super::*;

    #[test]
    fn compute_vdf_difficulty_should_work() {
        assert_eq!(
            601,
            compute_vdf_difficulty(
                1,
                H256::from(hex!(
                    "9fa752911d55c3a1246133fe280785afbdba41f357e9cae1131d5f5b0a078b9c"
                )),
                U256::from(100_000_u128),
            )
        );
        assert_eq!(
            416,
            compute_vdf_difficulty(
                100_000,
                H256::from(hex!(
                    "977c701ceeaeeca4a3693141b0016c1e0a743f465b2d560023a1cbb1eb1f5cd6"
                )),
                U256::from(1_210_000_u128),
            )
        );
    }

    #[test]
    fn compute_vdf_difficulty_overflow() {
        assert_eq!(
            2,
            compute_vdf_difficulty(u64::MAX - 1, H256::repeat_byte(0xFF), U256::ONE)
        )
    }

    #[test]
    fn compute_vdf_difficulty_max() {
        assert_eq!(
            MAXIMUM_ITERATIONS - 1,
            compute_vdf_difficulty(
                1,
                H256::from(hex!(
                    "000000000000000000000000000000000000000000000000000000002FAF07FF"
                )),
                U256::ONE,
            )
        )
    }

    #[tokio::test]
    async fn vdf_should_work() {
        // ARRANGE
        // ACT
        let proof = vdf(b"\xaaadf".to_vec(), 63).await.unwrap();

        // ASSERT
        assert!(hex::encode(proof).len() > 0);
    }

    #[tokio::test]
    // Wesolowski works for any kind of difficulty.
    #[ignore]
    async fn vdf_should_not_work_int_size_too_high() {
        // ARRANGE
        // ACT
        let result = vdf(b"\xaaadf".to_vec(), 63).await;

        // ASSERT
        assert!(result.is_err());
    }

    // #[test]
    // fn vdf_from_delay_should_work() {
    //     // ARRANGE
    //     // ACT
    //     let proof = vdf_from_delay(b"\xaaadf", 1).unwrap();

    //     // ASSERT
    //     assert!(hex::encode(proof).len() > 0);
    // }

    // #[ignore]
    // #[test]
    // fn vdf_from_delay_should_overflow() {
    //     // ARRANGE
    //     // ACT
    //     let proof = vdf_from_delay(b"\xaaadf", u64::MAX).unwrap();

    //     // ASSERT
    //     assert!(hex::encode(proof).len() > 0);
    // }
}
