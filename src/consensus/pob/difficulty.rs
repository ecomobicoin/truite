use std::cmp;

use bitvec::macros::internal::funty::Fundamental;

use crate::models::*;

pub struct BlockDifficultyBombData {
    pub delay_to: BlockNumber,
}

/// Check if network difficulty should be updated for the current block
pub fn should_update_network_difficulty(
    block_spec: &BlockExecutionSpec,
    block_number: BlockNumber,
) -> bool {
    block_number.as_u64() % block_spec.params.difficulty_adjustment_period == 0
}

/// Update Network Difficulty δ, adjusted every given time period P (number of blocks) to reach desired average mining time.
pub fn update_network_difficulty(
    block_spec: &BlockExecutionSpec,
    prev_network_difficulty: U256,
    avg_mining_time: u64,
) -> U256 {
    let prev_network_difficulty_u64: u64 = prev_network_difficulty.as_u64();
    let updated_network_difficulty = prev_network_difficulty_u64
        * block_spec.params.desired_average_mining_time_in_ms
        / avg_mining_time;

    if updated_network_difficulty > prev_network_difficulty_u64 {
        U256::new(
            cmp::min(
                updated_network_difficulty,
                prev_network_difficulty_u64
                    * block_spec.params.max_update_network_difficulty_percent
                    / 100,
            )
            .into(),
        )
    } else {
        U256::new(
            cmp::max(
                prev_network_difficulty_u64
                    * block_spec.params.min_update_network_difficulty_percent
                    / 100,
                updated_network_difficulty,
            )
            .into(),
        )
    }
}

/// Compute average block time from block timestamps.
///
/// If less than 2 blocks timestamps, returns `DESIRED_MINING_TIME_IN_MS`,
///  so difficulty won't change from previous one in `update_network_difficulty`.
pub fn compute_average_block_time_or_desired(
    desired_average_mining_time_in_ms: u64,
    previous_timestamps: Vec<u64>,
) -> u64 {
    let mut block_time_acc = 0;
    if previous_timestamps.len() > 1 {
        for n in 0..(previous_timestamps.len() - 1) {
            block_time_acc += previous_timestamps[n + 1] - previous_timestamps[n];
        }

        block_time_acc / (previous_timestamps.len() - 1).as_u64()
    } else {
        desired_average_mining_time_in_ms
    }
}

#[cfg(test)]
mod tests {
    use crate::res::chainspec::ECOMOBICOIN;

    use super::*;

    const DESIRED_AVERAGE_MINING_TIME_IN_MS: u64 = 10000;

    #[test]
    fn should_update_network_difficulty_test_yes() {
        // ARRANGE
        let prev_block_number = BlockNumber(10);
        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(prev_block_number);

        // ACT
        let result = should_update_network_difficulty(&block_spec, prev_block_number);

        // ASSERT
        assert_eq!(true, result);
    }

    #[test]
    fn should_update_network_difficulty_test_no() {
        // ARRANGE
        let block_number = BlockNumber(2);
        let chain_spec = ECOMOBICOIN.clone();
        let mut block_spec = chain_spec.collect_block_spec(block_number);
        block_spec.params.difficulty_adjustment_period = 10;

        // ACT
        let result = should_update_network_difficulty(&block_spec, block_number);

        // ASSERT
        assert_eq!(false, result);
    }

    #[test]
    fn update_network_difficulty_test_no_change() {
        // ARRANGE
        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));

        // ACT
        let result = update_network_difficulty(&block_spec, U256::new(100_000), 10_000);

        // ASSERT
        assert_eq!(U256::new(100_000), result);
    }

    #[test]
    fn update_network_difficulty_test_increase() {
        // ARRANGE
        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));

        // ACT
        let result = update_network_difficulty(&block_spec, U256::new(10_000), 10_000 * 50 / 100);

        // ASSERT
        assert_eq!(U256::new(20_000), result);
    }

    #[test]
    fn update_network_difficulty_test_decrease() {
        // ARRANGE
        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));

        // ACT
        let result = update_network_difficulty(&block_spec, U256::new(100_000), 5_000);

        // ASSERT
        assert_eq!(U256::new(200_000), result);
    }

    #[test]
    fn update_network_difficulty_test_min() {
        // ARRANGE
        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        // ACT
        let result = update_network_difficulty(&block_spec, U256::new(100_000), 10_000 * 10 / 100);

        // ASSERT
        assert_eq!(U256::new(400_000), result);
    }

    #[test]
    fn update_network_difficulty_test_max() {
        // ARRANGE
        let chain_spec = ECOMOBICOIN.clone();
        let block_spec = chain_spec.collect_block_spec(BlockNumber(0));
        // ACT
        let result = update_network_difficulty(&block_spec, U256::new(100_000), 10_000 * 5);

        // ASSERT
        assert_eq!(U256::new(25_000), result);
    }

    #[test]
    fn compute_average_block_time_or_desired_test_no_previous_timestamps() {
        // ARRANGE
        // ACT
        let result =
            compute_average_block_time_or_desired(DESIRED_AVERAGE_MINING_TIME_IN_MS, vec![]);

        // ASSERT
        assert_eq!(DESIRED_AVERAGE_MINING_TIME_IN_MS, result);
    }

    #[test]
    fn compute_average_block_time_or_desired_test() {
        // ARRANGE
        // ACT
        let result = compute_average_block_time_or_desired(
            DESIRED_AVERAGE_MINING_TIME_IN_MS,
            vec![
                1677628800, // Wednesday, 1 March 2023 00:00:00
                1677715199, // Wednesday, 1 March 2023 23:59:59
            ],
        );

        // ASSERT
        assert_eq!(3600 * 24 - 1, result);
    }

    #[test]
    fn compute_average_block_time_or_desired_test2() {
        // ARRANGE
        // ACT
        let result = compute_average_block_time_or_desired(
            DESIRED_AVERAGE_MINING_TIME_IN_MS,
            vec![
                1677628800, // Wednesday, 1 March 2023 00:00:00
                1677715199, // Wednesday, 1 March 2023 23:59:59
                1677758400, // Thursday, 2 March 2023 12:00:00
            ],
        );

        // ASSERT
        assert_eq!(3600 * 18, result);
    }

    #[test]
    fn compute_average_block_time_or_desired_test3() {
        // ARRANGE
        // ACT
        let result = compute_average_block_time_or_desired(
            DESIRED_AVERAGE_MINING_TIME_IN_MS,
            vec![
                1677628800, // Wednesday, 1 March 2023 00:00:00
                1677715199, // Wednesday, 1 March 2023 23:59:59
                1677758400, // Thursday, 2 March 2023 12:00:00
                1677801599, // Thursday, 2 March 2023 23:59:59
            ],
        );

        // ASSERT
        assert_eq!(3600 * 16 - 1, result);
    }
}
