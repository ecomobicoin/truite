use std::{ops::Mul, sync::Arc};

use parking_lot::Mutex;

use crate::BlockReader;

use super::{base::ConsensusEngineBase, *};

use self::vdf::verify;

pub mod demurrage;
pub mod difficulty;
pub mod miner;
mod vdf;

/// A behavior, no matter with type, must satisfy these conditions to be included in the chain.
/// This trait allows the code to be agnostic of which type of behavior is used
pub trait Quantifiable<T> {
    fn quantify(&self) -> U256;
}

pub trait Hashable<T> {
    fn to_hash(&self) -> H256;
}

pub trait Verifiable<T> {
    fn verify(&self) -> bool;
}

/// This struct is PoB consensus processor, with part of the parameters used for consensus
/// and reward calculations.
#[derive(Debug)]
pub struct Pob {
    base: ConsensusEngineBase,
    int_size_bits: u64,

    miner_behavior_reward_per_quantity: BlockSchedule<U256>,
    // k
    behavior_reward_per_quantity: BlockSchedule<U256>,
    // k'
    miner_included_behavior_reward_per_quantity: BlockSchedule<U256>, // k''

    skip_pow_verification: bool,
    fork_choice_graph: Arc<Mutex<ForkChoiceGraph>>,
}

impl Pob {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        chain_id: ChainId,
        eip1559_block: Option<BlockNumber>,
        int_size_bits: u64,
        miner_behavior_reward_per_quantity: BlockSchedule<U256>,
        behavior_reward_per_quantity: BlockSchedule<U256>,
        miner_included_behavior_reward_per_quantity: BlockSchedule<U256>,
        skip_pow_verification: bool,
    ) -> Self {
        Self {
            // FIXME Some(32) as max_extra_data_length?
            base: ConsensusEngineBase::new(chain_id, eip1559_block, None),
            int_size_bits,
            skip_pow_verification,
            fork_choice_graph: Arc::new(Mutex::new(ForkChoiceGraph::new())),
            miner_behavior_reward_per_quantity,
            behavior_reward_per_quantity,
            miner_included_behavior_reward_per_quantity,
        }
    }

    pub fn compute_miner_reward_for_behaviors_in_block(
        &self,
        block_number: BlockNumber,
        behavior_quantity: &U256,
    ) -> U256 {
        behavior_quantity.mul(
            self.miner_included_behavior_reward_per_quantity
                .for_block(block_number),
        )
    }

    pub fn compute_behavior_reward_per_quantity(
        behavior_reward_per_quantity: U256,
        quantity: U256,
    ) -> U256 {
        quantity.mul(behavior_reward_per_quantity)
    }
    pub fn compute_miner_behavior_reward_per_quantity(
        miner_behavior_reward_per_quantity: U256,
        quantity: U256,
        miner_included_behavior_reward: U256,
    ) -> U256 {
        quantity.mul(miner_behavior_reward_per_quantity)
            - quantity.mul(miner_included_behavior_reward) // Will be added again as part of the total quantity in block
    }
}

/// Implements necessary consensus functions for our PoB consensus
impl Consensus for Pob {
    fn fork_choice_mode(&self) -> ForkChoiceMode {
        ForkChoiceMode::Difficulty(self.fork_choice_graph.clone())
    }

    fn pre_validate_block(&self, block: &Block, state: &dyn BlockReader) -> Result<(), DuoError> {
        self.base.pre_validate_block(block)?;

        if block.ommers.len() > 2 {
            return Err(ValidationError::TooManyOmmers.into());
        }

        if block.ommers.len() == 2 && block.ommers[0] == block.ommers[1] {
            return Err(ValidationError::DuplicateOmmer.into());
        }

        let _parent =
            state
                .read_parent_header(&block.header)?
                .ok_or(ValidationError::UnknownParent {
                    number: block.header.number,
                    parent_hash: block.header.parent_hash,
                })?;

        for ommer in &block.ommers {
            let ommer_parent =
                state
                    .read_parent_header(ommer)?
                    .ok_or(ValidationError::OmmerUnknownParent {
                        number: ommer.number,
                        parent_hash: ommer.parent_hash,
                    })?;

            self.base
                .validate_block_header(ommer, &ommer_parent, false)
                .map_err(|e| match e {
                    DuoError::Internal(e) => DuoError::Internal(e),
                    DuoError::Validation(e) => {
                        DuoError::Validation(ValidationError::InvalidOmmerHeader {
                            inner: Box::new(e),
                        })
                    }
                })?;
            // let mut old_ommers = vec![];
            // if !self.base.is_kin(
            //     ommer,
            //     &parent,
            //     block.header.parent_hash,
            //     6,
            //     state,
            //     &mut old_ommers,
            // )? {
            //     return Err(ValidationError::NotAnOmmer.into());
            // }
            // for oo in old_ommers {
            //     if oo == *ommer {
            //         return Err(ValidationError::DuplicateOmmer.into());
            //     }
            // }
        }

        Ok(())
    }

    fn validate_block_header(
        &self,
        header: &BlockHeader,
        parent: &BlockHeader,
        with_future_timestamp_check: bool,
    ) -> Result<(), DuoError> {
        self.validate_header_parallel(header)?;

        self.base
            .validate_block_header(header, parent, with_future_timestamp_check)?;

        // let _parent_has_uncles = parent.ommers_hash != EMPTY_LIST_HASH;

        // if difficulty != header.difficulty {
        // return Err(ValidationError::WrongDifficulty.into());
        // }

        Ok(())
    }
    fn finalize(
        &self,
        block: &BlockHeader,
        _ommers: &[BlockHeader],
    ) -> anyhow::Result<Vec<FinalizationChange>> {
        let mut miner_reward: U256 = U256::ZERO;
        let mut finalization_changes = vec![];

        let reward_from_behaviors = Self::compute_miner_reward_for_behaviors_in_block(
            self,
            block.number,
            &block.behavior_total_quantity,
        );

        miner_reward += reward_from_behaviors;
        // reward from txs is done in execution
        // no reward for ommers
        finalization_changes.push(FinalizationChange::Reward {
            address: block.beneficiary,
            amount: miner_reward,
            ommer: false,
        });
        Ok(finalization_changes)
    }

    fn needs_parallel_validation(&self) -> bool {
        true
    }

    fn validate_header_parallel(&self, header: &BlockHeader) -> Result<(), DuoError> {
        if verify(
            &header.vdf_proof_challenge,
            header.vdf_difficulty,
            &header.vdf_proof_result,
        )
        .is_err()
        {
            return Err(ValidationError::InvalidSeal.into());
        }

        Ok(())
    }

    // fn validate_block(&self, header: &BlockHeader) -> Result<(), DuoError> {
    // Ok(())
    // }
}
