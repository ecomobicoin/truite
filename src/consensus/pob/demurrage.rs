/// Optional temporal monetary demurrage
/// > Demurrage is money quantity diminishing with time.
/// See Gesell, Silvio: The natural economic order. Owen London (1958)
pub mod demurrage {
    use std::collections::HashMap;

    use ethereum_types::Address;
    use ethnum::U256;
    use itertools::Itertools;
    use mdbx::{EnvironmentKind, RW};
    use num_traits::Inv;
    use tracing::{info, trace};

    use crate::{
        kv::{mdbx::MdbxTransaction, tables},
        models::Account,
    };

    /// For an account, read it's history and apply the demurrage per day percent to all it's past
    /// earnings, write the changes in db
    /// Used for test purpose only
    pub fn apply_demurrage_for_block_on_account<SE: EnvironmentKind>(
        address: Address,
        txn: &MdbxTransaction<RW, SE>,
        demurrage_per_day_percent: u32, //Transformed into demurrage for block
        desired_average_mining_time_in_ms: u64,
    ) -> anyhow::Result<()> {
        let mut account: Account = txn
            .get(tables::Account, address.clone())?
            .expect("Couldn't find account for address in database");

        // n-th root of the demurrage multiplier factor per day
        let multiplier_factor_per_block = compute_multiplier_factor_per_block(
            demurrage_per_day_percent,
            desired_average_mining_time_in_ms,
        );

        account.balance = compute_new_balance(&multiplier_factor_per_block, &account.balance);
        let _ = txn.set(tables::Account, address, account)?;
        Ok(())
    }

    /// First computes the demurrage percent for the block by estimating the number of block per day
    /// from the desired time between blocks and matching it with the desired percent of demurrage
    /// for every day.
    /// This percentage is then applied on all balances of accounts with positive number of coins.
    pub fn apply_demurrage_for_block_on_all_account<SE: EnvironmentKind>(
        txn: &MdbxTransaction<RW, SE>,
        demurrage_per_day_percent: u32, //Transformed into demurrage for block
        desired_average_mining_time_in_ms: u64,
    ) -> anyhow::Result<()> {
        info!("Applied demurrage on all accounts");
        if demurrage_per_day_percent > 0 {
            let multiplier_factor_per_block = compute_multiplier_factor_per_block(
                demurrage_per_day_percent,
                desired_average_mining_time_in_ms,
            );
            let account_cursor = txn.cursor(tables::Account)?;
            let values = account_cursor.walk(None).into_iter().collect_vec();
            for entry in values {
                if entry.is_ok() {
                    let (address, mut account) = entry.unwrap();
                    let new_balance =
                        compute_new_balance(&multiplier_factor_per_block, &account.balance);
                    trace!("Account : {:?}, new balance = {:?}", &account, &new_balance);
                    account.balance = new_balance;

                    let _ = txn.set(tables::Account, address, account);
                }
            }
        }
        Ok(())
    }

    /// In case of unwinding (error in the chain, blocks are reversed), we also reverse the demurrage that
    /// was applied to accounts, s.t. when you reach tip of the main chain after unwinding bad blocks
    /// yours accounts are perfectly synced (same number of coins) with other nodes
    pub fn reverse_demurrage_for_block_on_all_account<SE: EnvironmentKind>(
        txn: &MdbxTransaction<RW, SE>,
        demurrage_per_day_percent: u32, //Transformed into demurrage for block
        desired_average_mining_time_in_ms: u64,
    ) -> anyhow::Result<()> {
        info!("Reversed demurrage on all accounts");
        if demurrage_per_day_percent > 0 {
            let multiplier_factor_per_block = compute_multiplier_factor_per_block(
                demurrage_per_day_percent,
                desired_average_mining_time_in_ms,
            );
            let inverse = multiplier_factor_per_block.inv();
            let account_cursor = txn.cursor(tables::Account)?;
            let values = account_cursor.walk(None).into_iter().collect_vec();
            for entry in values {
                if entry.is_ok() {
                    let (address, mut account) = entry.unwrap();
                    let new_balance = compute_new_balance(&inverse, &account.balance);
                    trace!(
                        "Account : {:?}, new balance = {:?}\n",
                        &account,
                        &new_balance + 1
                    );
                    account.balance = new_balance + 1; //Rounded down in execution, rounded up in unwind to match

                    let _ = txn.set(tables::Account, address, account);
                }
            }
        }
        Ok(())
    }

    /// Applies demurrage in the very specific case of genesis where accounts and their balance are
    /// retrieved differently than in later execution of the chain
    pub fn compute_balances_with_demurrage_applied(
        balances: &HashMap<Address, U256>,
        demurrage_per_day_percent: u32, //Transformed into demurrage for block
        desired_average_mining_time_in_ms: u64,
    ) -> HashMap<Address, U256> {
        info!("Compute balance changes on block spec collection");
        let mut result: HashMap<Address, U256> = HashMap::new();
        if demurrage_per_day_percent > 0 {
            let multiplier_factor_per_block = compute_multiplier_factor_per_block(
                demurrage_per_day_percent,
                desired_average_mining_time_in_ms,
            );
            for (address, balance) in balances {
                let new_balance = compute_new_balance(&multiplier_factor_per_block, &balance);
                trace!("Account : {:?}, new balance = {:?}", &address, &new_balance);
                result.insert(address.clone(), new_balance);
            }
        } else {
            return balances.clone();
        }
        result
    }

    pub fn compute_new_balance(multiplier_factor_per_block: &f64, old_balance: &U256) -> U256 {
        // We multiply with 1 million the balance so it rounds down the balance after demurrage, since we are working with integers
        // We also multiply the factor by 1 million since we are working with integers and still want enough precision
        let new_balance = ((old_balance * U256::from(1_000_000_000 as u64))
            * U256::from((multiplier_factor_per_block * 1_000_000_000.0) as u64))
            / U256::from(1_000_000_000_000_000_000 as u64);
        new_balance
    }

    /// Computes the n-th root of the demurrage multiplier factor per day, n being the supposed number
    /// of blocks per day, estimated from the desired average mining time
    pub fn compute_multiplier_factor_per_block(
        demurrage_per_day_percent: u32,
        desired_average_mining_time_in_ms: u64,
    ) -> f64 {
        let multiplier_factor_per_day: f64 = 1.0 - (demurrage_per_day_percent as f64 / 10000.0);
        let desired_blocks_in_a_day: f64 = 86_400_000.0 / desired_average_mining_time_in_ms as f64;
        let multiplier_factor_per_block =
            f64::powf(multiplier_factor_per_day, 1.0 / desired_blocks_in_a_day); // n-th root of the demurrage multiplier factor per day
        multiplier_factor_per_block
    }
}

#[cfg(test)]
pub mod test {
    use std::sync::Arc;

    use ethereum_types::Address;
    use ethnum::U256;
    use mdbx::WriteMap;

    use crate::{
        consensus::demurrage::demurrage::{
            apply_demurrage_for_block_on_account, apply_demurrage_for_block_on_all_account,
        },
        kv::{new_mem_chaindata, tables, MdbxWithDirHandle},
        models::Account,
    };

    #[test]
    fn apply_demurrage_for_block_on_account_should_work() {
        // ARRANGE
        let account = Account {
            nonce: 0,
            balance: U256::from(100_000 as u64),
            code_hash: Default::default(),
        };
        let db: Arc<MdbxWithDirHandle<WriteMap>> = Arc::new(new_mem_chaindata().unwrap());
        let txn = db.begin_mutable().unwrap();
        txn.set(tables::Account, Address::zero(), account.clone())
            .expect("Failed write in db");

        // ACT
        let _success = apply_demurrage_for_block_on_account(Address::zero(), &txn, 10_00, 10_000);
        txn.commit().expect("Failed to commit transaction");

        //ASSERT
        let txn = db.begin().unwrap();
        let new_account = txn.get(tables::Account, Address::zero());
        let updated_balance = new_account.unwrap().unwrap().balance;
        assert!(updated_balance < account.balance);
        assert_eq!(updated_balance, U256::from(99_998u128));
    }

    #[test]
    fn apply_demurrage_for_block_on_all_account_should_work() {
        // ARRANGE
        let account = Account {
            nonce: 0,
            balance: U256::from(100_000 as u64),
            code_hash: Default::default(),
        };
        let db: Arc<MdbxWithDirHandle<WriteMap>> = Arc::new(new_mem_chaindata().unwrap());
        let txn = db.begin_mutable().unwrap();
        txn.set(tables::Account, Address::zero(), account.clone())
            .expect("Failed write in db");

        // ACT
        let _success = apply_demurrage_for_block_on_all_account(&txn, 10_00, 10_000);
        txn.commit().expect("Failed to commit transaction");

        // ASSERT
        let txn = db.begin().unwrap();
        let new_account = txn.get(tables::Account, Address::zero());
        let updated_balance = new_account.unwrap().unwrap().balance;
        assert!(updated_balance < account.balance);
        assert_eq!(updated_balance, U256::from(99_998u128));
    }

    #[test]
    fn apply_demurrage_for_block_on_all_account_should_work_no_demurrage() {
        // ARRANGE
        let account = Account {
            nonce: 0,
            balance: U256::from(100_000 as u64),
            code_hash: Default::default(),
        };
        let db: Arc<MdbxWithDirHandle<WriteMap>> = Arc::new(new_mem_chaindata().unwrap());
        let txn = db.begin_mutable().unwrap();
        txn.set(tables::Account, Address::zero(), account.clone())
            .expect("Failed write in db");

        // ACT
        let _success = apply_demurrage_for_block_on_all_account(&txn, 0, 10_000);
        txn.commit().expect("Failed to commit transaction");

        // ASSERT
        let txn = db.begin().unwrap();
        let new_account = txn.get(tables::Account, Address::zero());
        let updated_balance = new_account.unwrap().unwrap().balance;
        assert_eq!(updated_balance, account.balance);
    }
}
