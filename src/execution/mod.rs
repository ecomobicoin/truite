use crate::{
    consensus::{self, DuoError},
    models::*,
    State,
};

use self::{analysis_cache::AnalysisCache, processor::ExecutionProcessor, tracer::NoopTracer};

pub mod address;
pub mod analysis_cache;
pub mod evm;
pub mod evmglue;
pub mod precompiled;
pub mod processor;
pub mod tracer;

pub fn execute_block<S: State>(
    state: &mut S,
    config: &ChainSpec,
    header: &BlockHeader,
    block: &BlockBodyWithSenders,
) -> Result<Vec<Receipt>, DuoError> {
    let mut analysis_cache = AnalysisCache::default();
    let mut engine = consensus::engine_factory(None, config.clone(), None)?;
    let mut tracer = NoopTracer;

    let config = config.collect_block_spec(header.number);
    ExecutionProcessor::new(
        state,
        &mut tracer,
        &mut analysis_cache,
        &mut *engine,
        header,
        block,
        &config,
    )
    .execute_and_write_block()
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use bitvec::macros::internal::funty::Fundamental;
    use ethereum_types::BloomInput;
    use hex_literal::hex;

    use crate::{
        crypto::keccak256,
        models::behavior::{Behavior, BehaviorWithSender},
        res::chainspec::CLERMONT,
        trie::root_hash,
        InMemoryState, StateReader, StateWriter,
    };

    use super::{address::create_address, *};

    #[test]
    fn compute_receipt_root() {
        let receipts = vec![
            Receipt::new(TxType::Legacy, true, 21_000, vec![]),
            Receipt::new(TxType::Legacy, true, 42_000, vec![]),
            Receipt::new(
                TxType::Legacy,
                true,
                65_092,
                vec![Log {
                    address: hex!("8d12a197cb00d4747a1fe03395095ce2a5cc6819").into(),
                    topics: vec![hex!(
                        "f341246adaac6f497bc2a656f546ab9e182111d630394f0c57c710a59a2cb567"
                    )
                        .into()],
                    data: hex!("000000000000000000000000000000000000000000000000000000000000000000000000000000000000000043b2126e7a22e0c288dfb469e3de4d2c097f3ca0000000000000000000000000000000000000000000000001195387bce41fd4990000000000000000000000000000000000000000000000000000000000000000").to_vec().into(),
                }],
            ),
        ];

        assert_eq!(
            root_hash(&receipts),
            hex!("7ea023138ee7d80db04eeec9cf436dc35806b00cc5fe8e5f611fb7cf1b35b177").into()
        )
    }

    #[test]
    fn execute_two_blocks() {
        const MINER_FIXED_REWARD_PER_TX: u128 = 21_000;
        const CHAIN_ID: ChainId = ChainId(6363);

        // ---------------------------------------
        // Prepare
        // ---------------------------------------

        let block_number = 13_500_001.into();
        let miner = hex!("5a0b54d5dc17e0aadc383d2db43b0a0d3e029c4c").into();
        let cyclist = hex!("f39Fd6e51aad88F6F4ce6aB8827279cffFb92266").into();

        let default_value = U256::new(630_010);
        let gas_used = 63_001;
        let mut receipts = vec![Receipt {
            tx_type: TxType::EIP1559,
            success: true,
            cumulative_gas_used: gas_used,
            bloom: Bloom::zero(),
            logs: vec![],
        }];

        let header = PartialHeader {
            number: block_number,
            beneficiary: miner,
            gas_limit: 63_001, // might need more if intrisic gas is restored e.g. 200_000,
            gas_used,
            receipts_root: root_hash(&receipts),
            timestamp: 1672564870, // 2023/01/01 9:21:10 GMT
            ..PartialHeader::empty()
        };
        let header = BlockHeader::new(
            header,
            EMPTY_LIST_HASH,
            EMPTY_ROOT,
            EMPTY_ROOT,
            [0; 32],
            [0; 516],
            0,
        );

        // This contract initially sets its 0th storage to 0x2a
        // and its 1st storage to 0x01c9.
        // When called, it updates its 0th storage to the input provided.
        let contract_code = hex!("600035600055");
        let deployment_code = std::iter::empty()
            .chain(&hex!("602a6000556101c960015560068060166000396000f3") as &[u8])
            .chain(&contract_code)
            .copied()
            .collect::<Vec<u8>>();

        let sender = hex!("b685342b8c54347aad148e1f22eff3eb3eb29391").into();

        let t = |action, input, nonce, max_priority_fee_per_gas: u128| MessageWithSender {
            message: Message::EIP1559 {
                input,
                max_priority_fee_per_gas: max_priority_fee_per_gas.as_u256(),
                action,
                nonce,
                gas_limit: header.gas_limit,
                max_fee_per_gas: U256::from(20 * GIGA),
                chain_id: CHAIN_ID,
                value: default_value,
                access_list: Default::default(),
            },
            sender,
        };

        let tx = (t)(TransactionAction::Create, deployment_code.into(), 0, 0);

        let _b_sender = |timestamp, quantity, input| BehaviorWithSender {
            behavior: Behavior {
                chain_id: CHAIN_ID,
                timestamp,
                quantity,
                input,
            },
            sender,
        };
        let b_miner = |timestamp, quantity, input| BehaviorWithSender {
            behavior: Behavior {
                chain_id: CHAIN_ID,
                timestamp,
                quantity,
                input,
            },
            sender: miner,
        };
        let b_cyclist = |timestamp, quantity, input| BehaviorWithSender {
            behavior: Behavior {
                chain_id: CHAIN_ID,
                timestamp,
                quantity,
                input,
            },
            sender: cyclist,
        };

        let mut state = InMemoryState::default();

        let sender_account = Account {
            balance: ETHER.into(),
            ..Default::default()
        };
        state.update_account(sender, None, Some(sender_account));

        // ---------------------------------------
        // Execute first block
        // ---------------------------------------

        execute_block(
            &mut state,
            &CLERMONT,
            &header,
            &BlockBodyWithSenders {
                transactions: vec![tx],
                behaviors: vec![],
                ommers: Default::default(),
            },
        )
        .unwrap();

        let contract_address = create_address(sender, 0);
        let contract_account = state.read_account(contract_address).unwrap().unwrap();

        let code_hash = keccak256(contract_code);
        assert_eq!(contract_account.code_hash, code_hash);

        let storage_key0 = U256::ZERO;
        let storage0 = state.read_storage(contract_address, storage_key0).unwrap();
        assert_eq!(storage0, 0x2a);

        let storage_key1 = 0x01.as_u256();
        let storage1 = state.read_storage(contract_address, storage_key1).unwrap();
        assert_eq!(storage1, 0x01c9);

        let sender_account = state.read_account(sender).unwrap().unwrap();
        assert_eq!(
            sender_account.balance,
            ETHER - default_value - gas_used.as_u128()
        );
        // warning: impossible in theory as block don't have a miner's behavior
        let miner_account = state.read_account(miner).unwrap().unwrap();
        assert_eq!(miner_account.balance, MINER_FIXED_REWARD_PER_TX);

        // ---------------------------------------
        // Execute second block
        // ---------------------------------------

        let new_val = 0x3e;

        let block_number = 13_500_002.into();
        let mut header = header.clone();

        let cyclist_account = Account {
            balance: U256::ZERO,
            ..Default::default()
        };
        state.update_account(cyclist, None, Some(cyclist_account));

        header.number = block_number;

        header.gas_used = gas_used;

        let tx = (t)(
            TransactionAction::Call(contract_address),
            new_val.as_u256().to_be_bytes().to_vec().into(),
            1,
            0_u128 * u128::from(GIGA),
        );

        let bx = (b_cyclist)(
            1672534861, // 2023/01/01 01:01:01
            U256::from(10_u128),
            hex!("a0712d680000000000000000000000000000000000000000000000000000000000000002")
                .to_vec()
                .into(),
        );
        let bxm = (b_miner)(
            1672534861, // 2023/01/01 01:01:01
            U256::from(3_u128),
            hex!("a0712d680000000000000000000000000000000000000000000000000000000000000002")
                .to_vec()
                .into(),
        );
        header.behavior_total_quantity = bxm.behavior.quantity + bx.behavior.quantity;

        receipts = vec![
            // Behavior receipt
            Receipt {
                tx_type: TxType::EcoMobiCoinBehavior,
                success: true,
                cumulative_gas_used: 0,
                bloom: Bloom::from(BloomInput::Hash(bx.hash().as_fixed_bytes())),
                logs: vec![],
            },
            Receipt {
                tx_type: TxType::EcoMobiCoinBehavior,
                success: true,
                cumulative_gas_used: 0,
                bloom: Bloom::from(BloomInput::Hash(bxm.hash().as_fixed_bytes())),
                logs: vec![],
            },
            Receipt {
                tx_type: TxType::EIP1559,
                success: true,
                cumulative_gas_used: gas_used,
                bloom: Bloom::zero(),
                logs: vec![],
            },
        ];
        receipts[2].cumulative_gas_used = gas_used;
        header.receipts_root = root_hash(&receipts);
        header.logs_bloom = Bloom::from_str("00000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000000000100000000000000000000000000000000000000000000000000000000000000000000000000020000000000000000200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000000000000000000000000000000000000000000000000000000000000000000000000000000000000").unwrap();

        let result = execute_block(
            &mut state,
            &CLERMONT,
            &header,
            &BlockBodyWithSenders {
                transactions: vec![tx],
                behaviors: vec![bx, bxm],
                ommers: Default::default(),
            },
        )
        .unwrap();
        assert!(result.len() == 3); // 3 receipts (2bx + 1 tx)
        let storage0 = state.read_storage(contract_address, storage_key0).unwrap();
        assert_eq!(new_val, storage0);

        let miner_account = state.read_account(miner).unwrap().unwrap();
        let cyclist_account = state.read_account(cyclist).unwrap().unwrap();
        assert_eq!(10 * ETHER, cyclist_account.balance); // 10 ETHER
        assert_eq!(
            (
                3 * 2 * ETHER // His behavior
                    + 2 * MINER_FIXED_REWARD_PER_TX // For 1 transaction in this block + 1 tx in the 1st block of this test
                    + 10_000
                // Reward for including cyclist's behavior
            ),
            miner_account.balance
        );
    }
}
