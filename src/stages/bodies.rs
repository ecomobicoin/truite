use std::{
    collections::{HashSet, VecDeque},
    sync::{
        atomic::{AtomicBool, AtomicUsize, Ordering},
        Arc,
    },
    time::{Duration, Instant},
};

use anyhow::format_err;
use async_trait::async_trait;
use futures::stream::FuturesUnordered;
use hashbrown::HashMap;
use itertools::Itertools;
use mdbx::{EnvironmentKind, RW};
use parking_lot::{Mutex, RwLock};
use rand::prelude::*;
use rayon::iter::{ParallelDrainRange, ParallelIterator};
use tokio::{select, sync::watch};
use tokio_stream::StreamExt;
use tracing::*;

use crate::{
    consensus::{Consensus, DuoError},
    kv::{mdbx::MdbxTransaction, tables, traits::ttw},
    models::*,
    p2p::{
        node::{Node, NodeStream},
        types::{BlockBodies, Message},
    },
    stagedsync::stage::*,
    StageId,
};

const STAGE_UPPER_BOUND: usize = 90_000;
const REQUEST_INTERVAL: Duration = Duration::from_secs(2);
const INTERVAL_TWEAK_STEP: Duration = Duration::from_secs(2);
const MIN_SEND_INTERVAL: Duration = Duration::from_secs(2);
const MAX_SEND_INTERVAL: Duration = Duration::from_secs(60);

pub const BODIES: StageId = StageId("Bodies");

#[derive(Debug)]
pub struct BodyDownload {
    /// Node is a interface for interacting with p2p.
    pub node: Arc<Node>,
    /// Consensus engine used.
    pub consensus: Arc<dyn Consensus>,
}

enum DownloadError {
    ExitEarly(BlockNumber),
    Other(Box<StageError>),
}

impl From<anyhow::Error> for DownloadError {
    fn from(e: anyhow::Error) -> Self {
        DownloadError::Other(Box::new(StageError::from(e)))
    }
}

impl From<StageError> for DownloadError {
    fn from(e: StageError) -> Self {
        DownloadError::Other(Box::new(e))
    }
}

#[async_trait]
impl<'db, E> Stage<'db, E> for BodyDownload
where
    E: EnvironmentKind,
{
    fn id(&self) -> StageId {
        BODIES
    }

    /// Retrieves bodies that correspond to the headers retrieved in first stage
    async fn execute<'tx>(
        &mut self,
        txn: &'tx mut MdbxTransaction<'db, RW, E>,
        input: StageInput,
    ) -> Result<ExecOutput, StageError>
    where
        'db: 'tx,
    {
        let prev_progress = input.stage_progress.unwrap_or_default();
        let prev_stage_progress = input
            .previous_stage
            .map(|(_, v)| v)
            .ok_or_else(|| format_err!("cannot be first stage"))?;

        let (mut target, mut done) = if prev_stage_progress > prev_progress + STAGE_UPPER_BOUND {
            (prev_progress + STAGE_UPPER_BOUND, false)
        } else {
            (prev_stage_progress, true)
        };

        if target > prev_progress {
            info!("Downloading blocks up to {target}");
            let starting_block = prev_progress + 1;

            let mut stream = self.node.stream_bodies().await;
            match self
                .download_bodies(&mut stream, txn, starting_block, target, done)
                .await
            {
                Ok(_) => {}
                Err(DownloadError::ExitEarly(new_target)) => {
                    target = new_target;
                    done = true;
                }
                Err(DownloadError::Other(e)) => return Err(*e),
            }
        }

        Ok(ExecOutput::Progress {
            stage_progress: target,
            done,
            reached_tip: true,
        })
    }

    async fn unwind<'tx>(
        &mut self,
        txn: &'tx mut MdbxTransaction<'db, RW, E>,
        input: UnwindInput,
    ) -> anyhow::Result<UnwindOutput>
    where
        'db: 'tx,
    {
        let mut block_body_cur = txn.cursor(tables::BlockBody)?;
        let mut block_tx_cur = txn.cursor(tables::BlockTransaction)?;
        let mut block_bx_cur = txn.cursor(tables::BlockBehavior)?;

        while let Some((number, body)) = block_body_cur.last()? {
            if number <= input.unwind_to {
                break;
            }

            block_body_cur.delete_current()?;
            let mut deleted_tx = 0;
            while deleted_tx < body.tx_amount {
                let to_delete = body.base_tx_id + deleted_tx;
                if block_tx_cur.seek_exact(to_delete)?.is_some() {
                    block_tx_cur.delete_current()?;
                }

                deleted_tx += 1;
            }
            let mut deleted_bx = 0;
            while deleted_bx < body.bx_amount {
                let to_delete = body.base_bx_id + deleted_bx;
                if block_bx_cur.seek_exact(to_delete)?.is_some() {
                    block_bx_cur.delete_current()?;
                }

                deleted_bx += 1;
            }
        }

        Ok(UnwindOutput {
            stage_progress: input.unwind_to,
        })
    }
}

#[derive(Debug)]
pub struct PendingResponses {
    inner: HashSet<u64>,
    watch_sender: watch::Sender<usize>,
}

impl PendingResponses {
    pub fn new() -> (Self, watch::Receiver<usize>) {
        let (watch_sender, receiver) = watch::channel(0);
        (
            Self {
                inner: Default::default(),
                watch_sender,
            },
            receiver,
        )
    }

    pub fn notify(&mut self) {
        let _ = self.watch_sender.send(self.inner.len());
    }

    pub fn get_id(&mut self) -> u64 {
        loop {
            let id = rand::thread_rng().gen::<u64>();

            if self.inner.insert(id) {
                self.notify();
                return id;
            }
        }
    }

    pub fn remove(&mut self, request_id: u64) {
        self.inner.remove(&request_id);
        self.notify();
    }

    pub fn count(&mut self) -> usize {
        self.inner.len()
    }

    pub fn clear(&mut self) {
        self.inner.clear();
        self.notify();
    }
}

struct DownloadSession {
    handler: Arc<Node>,
    requests: RwLock<HashMap<(H256, H256, H256), (BlockNumber, H256)>>,
    pending_responses: Mutex<PendingResponses>,
    exit_early: AtomicBool,
}

impl BodyDownload {
    async fn download_bodies<E: EnvironmentKind>(
        &mut self,
        stream: &mut NodeStream,
        txn: &mut MdbxTransaction<'_, RW, E>,
        starting_block: BlockNumber,
        target: BlockNumber,
        will_reach_tip: bool,
    ) -> Result<(), DownloadError> {
        let (pending_responses, mut pending_responses_watch) = PendingResponses::new();
        let session = Arc::new(DownloadSession {
            handler: self.node.clone(),
            requests: RwLock::new(Self::prepare_requests(txn, starting_block, target)?),
            pending_responses: Mutex::new(pending_responses),
            exit_early: AtomicBool::new(false),
        });

        let mut bodies = {
            let _requester_task = tokio::spawn({
                let session = session.clone();

                async move {
                    let send_interval = REQUEST_INTERVAL;
                    let mut cycles_without_progress = 0;
                    let mut last_left_requests = session.requests.read().len();
                    loop {
                        let left_requests = session
                            .requests
                            .read()
                            .values()
                            .copied()
                            .collect::<Vec<_>>();

                        if left_requests.is_empty() {
                            break;
                        }

                        // Apply additional delay on no progress to prevent busy loop
                        cycles_without_progress = if left_requests.len() == last_left_requests {
                            if cycles_without_progress % 3 == 0 && cycles_without_progress > 0 {
                                tokio::time::sleep(send_interval).await;
                            }

                            if cycles_without_progress > 9 {
                                session.exit_early.store(true, Ordering::SeqCst);
                                break;
                            }

                            cycles_without_progress + 1
                        } else {
                            0
                        };

                        last_left_requests = left_requests.len();
                        let chunk = 16;

                        let mut will_reach_tip_this_cycle = false;

                        let mut total_chunks_this_cycle = 32 * session.handler.total_peers().await;
                        let left_chunks = (left_requests.len() + chunk - 1) / chunk;
                        if left_chunks < total_chunks_this_cycle {
                            total_chunks_this_cycle = left_chunks;
                            will_reach_tip_this_cycle = true;
                        }

                        info!(
                            "Sending {total_chunks_this_cycle} requests for {} out of {} block bodies, will receive for {send_interval:?}",
                            std::cmp::min(total_chunks_this_cycle * chunk, left_requests.len()),
                            left_requests.len()
                        );

                        let total_sent = Arc::new(AtomicUsize::new(total_chunks_this_cycle));
                        left_requests
                            .chunks(chunk)
                            .take(total_chunks_this_cycle)
                            .map(|chunk| {
                                let session = session.clone();
                                let total_sent = total_sent.clone();

                                async move {
                                    let _ = tokio::time::timeout(send_interval, async move {
                                        let request_id = session.pending_responses.lock().get_id();
                                        if session
                                            .handler
                                            .send_block_request(
                                                request_id,
                                                chunk,
                                                will_reach_tip_this_cycle,
                                            )
                                            .await
                                            .is_none()
                                        {
                                            session.pending_responses.lock().remove(request_id);
                                            total_sent.fetch_sub(1, Ordering::SeqCst);
                                        } else {
                                            debug!("Sent block request with id {request_id}");
                                        }
                                    })
                                        .await;
                                }
                            })
                            .collect::<FuturesUnordered<_>>()
                            .map(|_| ())
                            .collect::<()>()
                            .await;

                        let timeout = tokio::time::sleep(send_interval);
                        tokio::pin!(timeout);
                        loop {
                            tokio::select! {
                                _ = pending_responses_watch.changed() => {
                                    let next_cycle_threshold = total_sent.load(Ordering::SeqCst) / 5;
                                    if *pending_responses_watch.borrow() < next_cycle_threshold {
                                    }
                                    break;
                                }
                                _ = &mut timeout => {
                                    break;
                                }
                            }
                        }
                        session.pending_responses.lock().clear();
                    }
                }
            }).await;

            let mut bodies = HashMap::with_capacity(session.requests.read().len());
            let mut stats = VecDeque::new();
            let mut total_received = 0;
            let started_at = Instant::now();
            loop {
                let requests_length = session.requests.read().len();
                if requests_length == 0 {
                    break;
                };
                let batch_size = match requests_length * 5 / 256 {
                    v if v <= 2 => 8,
                    v => v,
                };

                let mut pending_bodies = Vec::with_capacity(batch_size);

                let receive_window = tokio::time::sleep(Duration::from_secs(1));
                tokio::pin!(receive_window);

                let notified = session.handler.block_cache_notify.notified();
                tokio::pin!(notified);

                loop {
                    select! {
                        msg = stream.next() => {
                            if let Some(msg) = msg {
                                if let Message::BlockBodies(BlockBodies{request_id, bodies}) = msg.msg.clone() {
                                    let mut pending_responses = session.pending_responses.lock();
                                    pending_responses.remove(request_id);
                                    debug!("Accepted block bodies with id {request_id}");
                                    pending_bodies.push(bodies);

                                    if pending_responses.count() == 0 {
                                        break;
                                    }
                                }
                            } else {
                                break;
                            }
                        }
                        _ = &mut notified, if will_reach_tip => {
                            let cached_blocks: Vec<BlockBody> = session.handler.block_cache.lock().drain().map(|(
                                _,
                                (
                                    _,
                                    _,
                                    Block {
                                        transactions,
                                        behaviors,
                                        ommers,
                                        ..
                                    },
                                ),
                            )| BlockBody {
                                transactions,
                                behaviors,
                                ommers,
                            }).collect();

                            if !cached_blocks.is_empty() {
                                pending_bodies.push(cached_blocks);
                            }
                            break;
                        }

                        _ = &mut receive_window => {
                            break;
                        }
                    }
                }

                let mut received = 0;
                if !pending_bodies.is_empty() {
                    let tmp = pending_bodies
                        .par_drain(..)
                        .flatten()
                        .map(|body| {
                            (
                                (
                                    body.ommers_hash(),
                                    body.behaviors_root(),
                                    body.transactions_root(),
                                ),
                                body,
                            )
                        })
                        .collect::<Vec<_>>();

                    let mut requests = session.requests.write();
                    for (key, value) in tmp {
                        if let Some((number, hash)) = requests.remove(&key) {
                            bodies.insert(number, (hash, value));
                            received += 1;
                        } else {
                            trace!("Block {key:?} was not requested, ignored");
                        }
                    }
                }

                total_received += received;
                let elapsed = started_at.elapsed().as_secs();
                stats.push_back((total_received, elapsed));

                if stats.len() > 20 {
                    stats.pop_front();
                }

                let (total_received_sum, elapsed_sum) = stats.iter().fold(
                    (0, 0),
                    |(total_received_sum, elapsed_sum), &(total_received, elapsed)| {
                        (total_received_sum + total_received, elapsed_sum + elapsed)
                    },
                );

                if received > 0 {
                    info!(
                        "Received {received} block bodies{}",
                        if elapsed_sum > 0 {
                            format!(" ({} blk/sec)", total_received_sum / elapsed_sum)
                        } else {
                            String::new()
                        }
                    );
                }
            }
            bodies
        };

        info!("Saving {} block bodies", bodies.len());

        let undownloaded_bodies = session
            .requests
            .read()
            .iter()
            .map(|(_, (b, _))| *b)
            .collect::<HashSet<_>>();

        let mut cursor = txn.cursor(tables::BlockBody)?;
        let mut header_cur = txn.cursor(tables::Header)?;
        let mut block_tx_cursor = txn.cursor(tables::BlockTransaction)?;
        let mut block_bx_cursor = txn.cursor(tables::BlockBehavior)?;
        let mut tx_sender_cursor = txn.cursor(tables::TxSender)?;
        let mut bx_sender_cursor = txn.cursor(tables::BxSender)?;
        let mut base_tx_id = cursor
            .last()?
            .map(|(_, body)| *body.base_tx_id + body.tx_amount)
            .unwrap();
        let mut base_bx_id = cursor
            .last()?
            .map(|(_, body)| *body.base_bx_id + body.bx_amount)
            .unwrap();
        for block_number in starting_block..=target {
            if undownloaded_bodies.contains(&block_number) {
                return Err(DownloadError::ExitEarly(BlockNumber(
                    block_number.saturating_sub(1),
                )));
            }

            let body = bodies
                .remove(&block_number)
                .map(|(_, body)| body)
                .unwrap_or_default();

            let block = Block {
                header: header_cur.seek_exact(block_number).unwrap().unwrap().1,
                transactions: body.transactions,
                behaviors: body.behaviors,
                ommers: body.ommers,
            };

            self.consensus
                .pre_validate_block(&block, txn)
                .map_err(|e| match e {
                    DuoError::Validation(error) => StageError::Validation {
                        block: block.header.number,
                        error,
                    },
                    DuoError::Internal(error) => StageError::Internal(error),
                })?;

            cursor.append(
                block_number,
                BodyForStorage {
                    base_tx_id: TxIndex(base_tx_id),
                    base_bx_id: TxIndex(base_bx_id),
                    tx_amount: block.transactions.len() as u64,
                    bx_amount: block.behaviors.len() as u64,
                    ommers: block.ommers,
                },
            )?;

            let mut senders = vec![];
            for transaction in &block.transactions {
                senders.push(transaction.recover_sender()?);
            }
            let senders = senders.into_iter().unique().collect();
            let _ = tx_sender_cursor.append(block_number, senders);

            let mut all_tx_addresses_in_block: Vec<Address> = vec![];
            for transaction in block.transactions {
                let receiver = transaction
                    .message
                    .action()
                    .into_address()
                    .unwrap_or(Address::default());

                base_tx_id += 1;
                block_tx_cursor.append(TxIndex(base_tx_id), transaction.clone())?;
                all_tx_addresses_in_block.push(transaction.recover_sender()?);
                all_tx_addresses_in_block.push(receiver);
            }

            let addresses_for_tx: Vec<Address> =
                all_tx_addresses_in_block.into_iter().unique().collect();
            for address in addresses_for_tx {
                let existing_block_number_for_address =
                    txn.get(tables::TransactionsBySender, address.clone())?;
                if existing_block_number_for_address.is_some() {
                    let mut new_entry = existing_block_number_for_address.unwrap();
                    new_entry.push(block_number);
                    txn.set(tables::TransactionsBySender, address, new_entry)
                        .expect("Failed to set new value in db");
                } else {
                    let mut new_entry = vec![];
                    new_entry.push(block_number);
                    txn.set(tables::TransactionsBySender, address, new_entry)
                        .expect("Failed to set new value in db");
                }
            }

            let mut senders = vec![];
            for behavior in &block.behaviors {
                senders.push(behavior.recover_sender()?);
            }
            let senders = senders.into_iter().unique().collect();
            bx_sender_cursor.append(block_number, senders)?;

            let mut all_bx_addresses_in_block: Vec<Address> = vec![];
            for behavior in block.behaviors {
                all_bx_addresses_in_block.push(behavior.recover_sender()?);
                block_bx_cursor.append(TxIndex(base_bx_id), behavior)?;
                base_bx_id += 1;
            }
            let addresses_for_bx: Vec<Address> =
                all_bx_addresses_in_block.into_iter().unique().collect();
            for address in addresses_for_bx {
                let existing_block_number_for_address =
                    txn.get(tables::BehaviorsBySender, address.clone())?;
                if existing_block_number_for_address.is_some() {
                    let mut new_entry = existing_block_number_for_address.unwrap();
                    new_entry.push(block_number);
                    txn.set(tables::BehaviorsBySender, address, new_entry)
                        .expect("Failed to set new value in db");
                } else {
                    let mut new_entry = vec![];
                    new_entry.push(block_number);
                    txn.set(tables::BehaviorsBySender, address, new_entry)
                        .expect("Failed to set new value in db");
                }
            }
        }

        Ok(())
    }

    fn prepare_requests<E: EnvironmentKind>(
        txn: &mut MdbxTransaction<'_, RW, E>,
        starting_block: BlockNumber,
        target: BlockNumber,
    ) -> anyhow::Result<HashMap<(H256, H256, H256), (BlockNumber, H256)>> {
        let cap = match target.0.saturating_sub(starting_block.0) + 1 {
            0 => return Ok(HashMap::new()),
            cap => cap as usize,
        };
        let mut map = HashMap::with_capacity(cap);

        let mut header_cursor = txn
            .cursor(tables::Header)?
            .walk(Some(starting_block))
            .take_while(ttw(|&(block_number, _)| block_number <= target));

        while let Some(Ok((block_number, header))) = header_cursor.next() {
            let hash = crate::accessors::chain::canonical_hash::read(txn, block_number)?.unwrap();
            map.insert(
                (
                    header.ommers_hash,
                    header.behaviors_root,
                    header.transactions_root,
                ),
                (block_number, hash),
            );
        }

        Ok(map)
    }
}
