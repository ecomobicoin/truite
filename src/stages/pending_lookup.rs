use std::{collections::BTreeMap, sync::Arc, time::Duration};

use async_trait::async_trait;
use itertools::Itertools;
use rand::Rng;
use tempfile::TempDir;
use tokio_stream::StreamExt;
use tracing::*;

use crate::{
    consensus::miner::get_latest_parent_header,
    kv::{mdbx::*, tables},
    models::{behavior::BehaviorWithSignature, BlockNumber, MessageWithSignature, TxIndex},
    p2p::{
        node::{Node, NodeStream},
        types::{PooledTransactions, PooledTransactionsRequest, *},
    },
    stagedsync::stage::*,
    StageId,
};

pub const PENDING_LOOKUP: StageId = StageId("Pending Lookup");

#[derive(Debug)]
pub struct PendingLookup {
    pub temp_dir: Arc<TempDir>,
    pub node: Arc<Node>,
}

#[async_trait]
impl<'db, E> Stage<'db, E> for PendingLookup
where
    E: EnvironmentKind,
{
    fn id(&self) -> StageId {
        PENDING_LOOKUP
    }

    /// Asks P2P nodes if they have txs/bxs in their pool, waiting to be included
    async fn execute<'tx>(
        &mut self,
        tx: &'tx mut MdbxTransaction<'db, RW, E>,
        input: StageInput,
    ) -> Result<ExecOutput, StageError>
    where
        'db: 'tx,
    {
        let last_header = get_latest_parent_header(tx);
        let mut prev_stage = input
            .previous_stage
            .map(|(_, b)| b)
            .unwrap_or(BlockNumber(0));
        if last_header.is_ok() {
            let number = last_header.unwrap().number;
            if number > prev_stage {
                prev_stage = number;
            }
        }
        let mut stream = self.node.stream_transactions().await;
        let reverse_download_result = match self
            .reverse_download_linear(
                &mut stream,
                &self.node.stash.get_pending_transactions_db_state()?,
                &self.node.stash.get_pending_behavior_db_state()?,
            )
            .await
        {
            LinearDownloadResult::Done((buffered_transactions, buffered_behaviors)) => {
                let txs = buffered_transactions.into_values().collect_vec();
                let bxs = buffered_behaviors.into_values().collect_vec();
                (txs, bxs)
            }
            LinearDownloadResult::NoResponse => {
                return Ok(ExecOutput::Progress {
                    stage_progress: prev_stage,
                    done: false,
                    reached_tip: false,
                });
            }
        };

        let mut pending_cursor = tx.cursor(tables::PendingTransaction)?;
        let mut bx_pending_cursor = tx.cursor(tables::PendingBehavior)?;
        for tx in reverse_download_result.0 {
            pending_cursor.put(TxIndex(rand::thread_rng().gen()), tx)?;
        }
        for bx in reverse_download_result.1 {
            bx_pending_cursor.put(TxIndex(rand::thread_rng().gen()), bx)?;
        }
        Ok(ExecOutput::Progress {
            stage_progress: prev_stage,
            done: true,
            reached_tip: true,
        })
    }

    async fn unwind<'tx>(
        &mut self,
        tx: &'tx mut MdbxTransaction<'db, RW, E>,
        _input: UnwindInput,
    ) -> anyhow::Result<UnwindOutput>
    where
        'db: 'tx,
    {
        tx.clear_table(tables::PendingTransaction)?;
        tx.clear_table(tables::PendingBehavior)?;
        Ok(UnwindOutput {
            stage_progress: BlockNumber(0),
        })
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum LinearDownloadResult {
    Done(
        (
            BTreeMap<TxIndex, MessageWithSignature>,
            BTreeMap<TxIndex, BehaviorWithSignature>,
        ),
    ),
    NoResponse,
}

impl PendingLookup {
    async fn reverse_download_linear(
        &self,
        stream: &mut NodeStream,
        transactions_already_in_db: &Vec<primitive_types::H256>,
        behaviors_already_in_db: &Vec<primitive_types::H256>,
    ) -> LinearDownloadResult {
        let mut buffered_transactions = BTreeMap::<TxIndex, MessageWithSignature>::new();
        let mut buffered_behaviors = BTreeMap::<TxIndex, BehaviorWithSignature>::new();
        let _remaining_attempts = Some(10_usize);
        loop {
            let _limit = 1024;

            let sent_request_id = rand::thread_rng().gen();
            if let Ok(_sent) = tokio::time::timeout(
                Duration::from_secs(5),
                self.node.send_pooled_transactions_request(
                    Some(sent_request_id),
                    PooledTransactionsRequest {
                        txs_already_present_in_db: transactions_already_in_db.to_vec(),
                        bxs_already_present_in_db: behaviors_already_in_db.to_vec(),
                    },
                ),
            )
            .await
            {
                let timeout = tokio::time::sleep(Duration::from_secs(5));
                tokio::pin!(timeout);

                let res = loop {
                    tokio::select! {
                        msg = stream.next() => {
                            if let Some(msg) = msg {
                                if let Message::PooledTransactions(PooledTransactions{ request_id, transactions, behaviors}) = msg.msg.clone() {
                                    if sent_request_id == request_id{
                                        info!("Received {} transactions from peer {}/{}", transactions.len(), msg.sentry_id, msg.peer_id);
                                        info!("Received {} behaviors from peer {}/{}", behaviors.len(), msg.sentry_id, msg.peer_id);

                                        break Some((transactions, behaviors));
                                    }
                                }
                                debug!("Ignoring unsolicited message from {}/{}: {:?}", msg.sentry_id, msg.peer_id, msg);
                            }
                        }
                        _ = &mut timeout => {
                            break None;
                        }
                    }
                };

                if let Some((transactions, behaviors)) = res {
                    for tx in transactions {
                        buffered_transactions.insert(TxIndex(rand::thread_rng().gen()), tx);
                    }
                    for bx in behaviors {
                        buffered_behaviors.insert(TxIndex(rand::thread_rng().gen()), bx);
                    }
                    return LinearDownloadResult::Done((buffered_transactions, buffered_behaviors));
                }
            }
        }
    }
}
