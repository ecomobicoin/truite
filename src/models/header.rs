use anyhow::{bail, format_err};
use bytes::{Buf, Bytes, BytesMut};
use modular_bitfield::prelude::*;

use serde_big_array::BigArray;

use crate::crypto::*;

use super::{util::*, *};

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
/// Ethereum block header definition.
pub struct BlockHeader {
    pub parent_hash: H256,
    pub ommers_hash: H256,
    pub beneficiary: H160,
    pub state_root: H256,
    pub transactions_root: H256,
    pub behaviors_root: H256,
    pub receipts_root: H256,
    pub logs_bloom: Bloom,
    pub behaviors_bloom: Bloom,
    pub difficulty: U256,
    pub number: BlockNumber,
    pub gas_limit: u64,
    pub gas_used: u64,
    pub timestamp: u64,
    pub behavior_total_quantity: U256,
    pub mix_hash: H256,
    pub nonce: H64,
    pub base_fee_per_gas: Option<U256>,
    pub vdf_difficulty: u64,
    #[serde(with = "BigArray")]
    pub vdf_proof_challenge: [u8; 32],
    #[serde(with = "BigArray")]
    pub vdf_proof_result: [u8; 516],
    pub extra_data: Bytes,
}

impl Default for BlockHeader {
    fn default() -> BlockHeader {
        BlockHeader {
            parent_hash: Default::default(),
            ommers_hash: Default::default(),
            beneficiary: Default::default(),
            state_root: Default::default(),
            transactions_root: Default::default(),
            behaviors_root: Default::default(),
            receipts_root: Default::default(),
            logs_bloom: Default::default(),
            behaviors_bloom: Default::default(),
            difficulty: Default::default(),
            number: Default::default(),
            gas_limit: 0,
            gas_used: 0,
            timestamp: 0,
            behavior_total_quantity: U256::ZERO,
            mix_hash: Default::default(),
            nonce: Default::default(),
            base_fee_per_gas: None,
            vdf_difficulty: 0,
            vdf_proof_challenge: [0; 32],
            vdf_proof_result: [0; 516],
            extra_data: Default::default(),
        }
    }
}

#[bitfield]
#[derive(Clone, Copy, Debug, Default)]
struct HeaderFlags {
    ommers_hash: bool,
    transactions_root: bool,
    behaviors_root: bool,
    receipts_root: bool,
    logs_bloom: bool,
    behaviors_bloom: bool,
    mix_hash: bool,
    nonce: bool,
    difficulty_len: B5,
    block_number_len: B3,
    gas_limit_len: B3,
    gas_used_len: B3,
    timestamp_len: B3,
    behavior_total_quantity_len: B5,
    vdf_difficulty_len: B4, // need 4 bits for 8 = max encoded length ?!?
    base_fee_per_gas_len: B5,
    #[skip]
    unused: B1,
}

impl BlockHeader {
    pub fn compact_encode(&self) -> Vec<u8> {
        let mut buffer = vec![];

        let mut flags = HeaderFlags::default();
        if self.ommers_hash != EMPTY_LIST_HASH {
            flags.set_ommers_hash(true);
        }
        if self.transactions_root != EMPTY_ROOT {
            flags.set_transactions_root(true);
        }
        if self.behaviors_root != EMPTY_ROOT {
            flags.set_behaviors_root(true);
        }
        if self.receipts_root != EMPTY_ROOT {
            flags.set_receipts_root(true);
        }
        if !self.logs_bloom.is_zero() {
            flags.set_logs_bloom(true);
        }
        if !self.behaviors_bloom.is_zero() {
            flags.set_behaviors_bloom(true);
        }

        let difficulty_encoded = variable_to_compact(self.difficulty);
        flags.set_difficulty_len(difficulty_encoded.len() as u8);

        let block_number_encoded = variable_to_compact(self.number.0);
        flags.set_block_number_len(block_number_encoded.len() as u8);

        let gas_limit_encoded = variable_to_compact(self.gas_limit);
        flags.set_gas_limit_len(gas_limit_encoded.len() as u8);

        let gas_used_encoded = variable_to_compact(self.gas_used);
        flags.set_gas_used_len(gas_used_encoded.len() as u8);

        let timestamp_encoded = variable_to_compact(self.timestamp);
        flags.set_timestamp_len(timestamp_encoded.len() as u8);

        let behavior_total_quantity_encoded = variable_to_compact(self.behavior_total_quantity);
        flags.set_behavior_total_quantity_len(behavior_total_quantity_encoded.len() as u8);

        let vdf_difficulty_encoded = variable_to_compact(self.vdf_difficulty);
        flags.set_vdf_difficulty_len(vdf_difficulty_encoded.len() as u8);

        let base_fee_per_gas_encoded =
            variable_to_compact(self.base_fee_per_gas.unwrap_or(U256::ZERO));
        flags.set_base_fee_per_gas_len(base_fee_per_gas_encoded.len() as u8);

        if !self.mix_hash.is_zero() {
            flags.set_mix_hash(true);
        }
        if !self.nonce.is_zero() {
            flags.set_nonce(true);
        }

        let fs = flags.into_bytes();
        buffer.extend_from_slice(&fs[..]);

        buffer.extend_from_slice(&self.parent_hash[..]);
        if flags.ommers_hash() {
            buffer.extend_from_slice(&self.ommers_hash[..]);
        }
        buffer.extend_from_slice(&self.beneficiary[..]);
        buffer.extend_from_slice(&self.state_root[..]);
        if flags.transactions_root() {
            buffer.extend_from_slice(&self.transactions_root[..]);
        }
        if flags.behaviors_root() {
            buffer.extend_from_slice(&self.behaviors_root[..]);
        }
        if flags.receipts_root() {
            buffer.extend_from_slice(&self.receipts_root[..]);
        }
        if flags.logs_bloom() {
            buffer.extend_from_slice(&self.logs_bloom[..]);
        }
        if flags.behaviors_bloom() {
            buffer.extend_from_slice(&self.behaviors_bloom[..]);
        }

        buffer.extend_from_slice(&difficulty_encoded[..]);
        buffer.extend_from_slice(&block_number_encoded[..]);
        buffer.extend_from_slice(&gas_limit_encoded[..]);
        buffer.extend_from_slice(&gas_used_encoded[..]);
        buffer.extend_from_slice(&timestamp_encoded[..]);
        buffer.extend_from_slice(&behavior_total_quantity_encoded[..]);
        buffer.extend_from_slice(&base_fee_per_gas_encoded[..]);

        if flags.mix_hash() {
            buffer.extend_from_slice(&self.mix_hash[..]);
        }

        if flags.nonce() {
            buffer.extend_from_slice(&self.nonce[..]);
        }

        buffer.extend_from_slice(&vdf_difficulty_encoded[..]);
        buffer.extend_from_slice(&self.vdf_proof_challenge);
        buffer.extend_from_slice(&self.vdf_proof_result);

        buffer.extend_from_slice(&self.extra_data);

        buffer
    }
    pub fn compact_decode(mut buf: &[u8]) -> anyhow::Result<Self> {
        if buf.len() < 5 {
            bail!("input too short");
        }

        let flags = HeaderFlags::from_bytes([
            buf.get_u8(),
            buf.get_u8(),
            buf.get_u8(),
            buf.get_u8(),
            buf.get_u8(),
        ]);

        let parent_hash;
        (parent_hash, buf) = h256_from_compact(buf)?;

        let mut ommers_hash = EMPTY_LIST_HASH;
        if flags.ommers_hash() {
            (ommers_hash, buf) = h256_from_compact(buf)?;
        }

        let beneficiary;
        (beneficiary, buf) = h160_from_compact(buf)?;

        let state_root;
        (state_root, buf) = h256_from_compact(buf)?;

        let mut transactions_root = EMPTY_ROOT;
        if flags.transactions_root() {
            (transactions_root, buf) = h256_from_compact(buf)?;
        }
        let mut behaviors_root = EMPTY_ROOT;
        if flags.behaviors_root() {
            (behaviors_root, buf) = h256_from_compact(buf)?;
        }

        let mut receipts_root = EMPTY_ROOT;
        if flags.receipts_root() {
            (receipts_root, buf) = h256_from_compact(buf)?;
        }

        let mut logs_bloom = Bloom::zero();
        if flags.logs_bloom() {
            fn bloom_from_compact(mut buf: &[u8]) -> anyhow::Result<(Bloom, &[u8])> {
                let v = Bloom::from_slice(
                    buf.get(..256)
                        .ok_or_else(|| format_err!("input too short"))?,
                );
                buf.advance(256);
                Ok((v, buf))
            }

            (logs_bloom, buf) = bloom_from_compact(buf)?;
        }
        let mut behaviors_bloom = Bloom::zero();
        if flags.behaviors_bloom() {
            fn bloom_from_compact(mut buf: &[u8]) -> anyhow::Result<(Bloom, &[u8])> {
                let v = Bloom::from_slice(
                    buf.get(..256)
                        .ok_or_else(|| format_err!("input too short"))?,
                );
                buf.advance(256);
                Ok((v, buf))
            }

            (behaviors_bloom, buf) = bloom_from_compact(buf)?;
        }

        let difficulty;
        (difficulty, buf) = variable_from_compact(buf, flags.difficulty_len())?;

        let number;
        (number, buf) = variable_from_compact(buf, flags.block_number_len())?;
        let number = BlockNumber(number);

        let gas_limit;
        (gas_limit, buf) = variable_from_compact(buf, flags.gas_limit_len())?;

        let gas_used;
        (gas_used, buf) = variable_from_compact(buf, flags.gas_used_len())?;

        let timestamp;
        (timestamp, buf) = variable_from_compact(buf, flags.timestamp_len())?;

        let behavior_total_quantity;
        (behavior_total_quantity, buf) =
            variable_from_compact(buf, flags.behavior_total_quantity_len())?;

        let base_fee_per_gas;
        (base_fee_per_gas, buf) = variable_from_compact(buf, flags.base_fee_per_gas_len())?;
        let base_fee_per_gas = if base_fee_per_gas == 0 {
            None
        } else {
            Some(base_fee_per_gas)
        };

        let mut mix_hash = H256::zero();
        if flags.mix_hash() {
            (mix_hash, buf) = h256_from_compact(buf)?;
        }

        let mut nonce = H64::zero();
        if flags.nonce() {
            fn h64_from_compact(mut buf: &[u8]) -> anyhow::Result<(H64, &[u8])> {
                let v =
                    H64::from_slice(buf.get(..8).ok_or_else(|| format_err!("input too short"))?);
                buf.advance(8);
                Ok((v, buf))
            }

            (nonce, buf) = h64_from_compact(buf)?;
        }

        let vdf_difficulty;
        (vdf_difficulty, buf) = variable_from_compact(buf, flags.vdf_difficulty_len())?;

        let vdf_challenge = Self::clone_into_array(buf.get(..32).unwrap());
        buf.advance(32);
        let vdf_result = Self::clone_into_array_of_size_516(buf.get(..516).unwrap());
        buf.advance(516);

        let extra_data = buf[..].to_vec().into();

        Ok(Self {
            parent_hash,
            ommers_hash,
            beneficiary,
            state_root,
            transactions_root,
            behaviors_root,
            receipts_root,
            logs_bloom,
            behaviors_bloom,
            difficulty,
            number,
            gas_limit,
            gas_used,
            timestamp,
            behavior_total_quantity,
            mix_hash,
            nonce,
            base_fee_per_gas,
            vdf_proof_challenge: vdf_challenge,
            vdf_proof_result: vdf_result,
            vdf_difficulty,
            extra_data,
        })
    }
    fn clone_into_array<A, T>(slice: &[T]) -> A
    where
        A: Default + AsMut<[T]>,
        T: Clone,
    {
        let mut a = A::default();
        <A as AsMut<[T]>>::as_mut(&mut a).clone_from_slice(slice);
        a
    }

    fn clone_into_array_of_size_516(slice: &[u8]) -> [u8; 516] {
        let mut a = [0; 516];
        a.clone_from_slice(&(*slice));
        a
    }
    fn rlp_header(&self) -> Header {
        let mut rlp_head = Header {
            list: true,
            payload_length: 0,
        };

        rlp_head.payload_length += KECCAK_LENGTH + 1; // parent_hash
        rlp_head.payload_length += KECCAK_LENGTH + 1; // ommers_hash
        rlp_head.payload_length += ADDRESS_LENGTH + 1; // beneficiary
        rlp_head.payload_length += KECCAK_LENGTH + 1; // state_root
        rlp_head.payload_length += KECCAK_LENGTH + 1; // transactions_root
        rlp_head.payload_length += KECCAK_LENGTH + 1; // behaviors_root
        rlp_head.payload_length += KECCAK_LENGTH + 1; // receipts_root
        rlp_head.payload_length += BLOOM_BYTE_LENGTH + length_of_length(BLOOM_BYTE_LENGTH); // logs_bloom
        rlp_head.payload_length += BLOOM_BYTE_LENGTH + length_of_length(BLOOM_BYTE_LENGTH); // behaviors_bloom
        rlp_head.payload_length += self.difficulty.length(); // difficulty
        rlp_head.payload_length += self.number.length(); // block height
        rlp_head.payload_length += self.gas_limit.length(); // gas_limit
        rlp_head.payload_length += self.gas_used.length(); // gas_used
        rlp_head.payload_length += self.timestamp.length(); // timestamp
        rlp_head.payload_length += self.behavior_total_quantity.length(); // behavior_total_quantity
        rlp_head.payload_length += self.vdf_difficulty.length(); // vdf_difficulty
        rlp_head.payload_length += self.vdf_proof_challenge.length(); // vdf_proof_challenge
        rlp_head.payload_length += self.vdf_proof_result.length(); // vdf_proof_result
        rlp_head.payload_length += self.extra_data.length(); // extra_data

        rlp_head.payload_length += KECCAK_LENGTH + 1; // mix_hash
        rlp_head.payload_length += 8 + 1; // nonce

        if let Some(base_fee_per_gas) = self.base_fee_per_gas {
            rlp_head.payload_length += base_fee_per_gas.length();
        }

        rlp_head
    }
}

impl Encodable for BlockHeader {
    fn encode(&self, out: &mut dyn BufMut) {
        self.rlp_header().encode(out);
        Encodable::encode(&self.parent_hash, out);
        Encodable::encode(&self.ommers_hash, out);
        Encodable::encode(&self.beneficiary, out);
        Encodable::encode(&self.state_root, out);
        Encodable::encode(&self.transactions_root, out);
        Encodable::encode(&self.behaviors_root, out);
        Encodable::encode(&self.receipts_root, out);
        Encodable::encode(&self.logs_bloom, out);
        Encodable::encode(&self.behaviors_bloom, out);
        Encodable::encode(&self.difficulty, out);
        Encodable::encode(&self.number, out);
        Encodable::encode(&self.gas_limit, out);
        Encodable::encode(&self.gas_used, out);
        Encodable::encode(&self.timestamp, out);
        Encodable::encode(&self.behavior_total_quantity, out);
        Encodable::encode(&self.extra_data, out);
        Encodable::encode(&self.mix_hash, out);
        Encodable::encode(&self.nonce, out);
        Encodable::encode(&self.vdf_difficulty, out);
        Encodable::encode(&self.vdf_proof_challenge, out);
        Encodable::encode(&self.vdf_proof_result, out);
        if let Some(base_fee_per_gas) = self.base_fee_per_gas {
            Encodable::encode(&base_fee_per_gas, out);
        }
    }
    fn length(&self) -> usize {
        let rlp_head = self.rlp_header();
        length_of_length(rlp_head.payload_length) + rlp_head.payload_length
    }
}

impl Decodable for BlockHeader {
    fn decode(buf: &mut &[u8]) -> Result<Self, DecodeError> {
        let rlp_head = Header::decode(buf)?;
        if !rlp_head.list {
            return Err(DecodeError::UnexpectedString);
        }
        let leftover = buf.len() - rlp_head.payload_length;
        let parent_hash = Decodable::decode(buf)?;
        let ommers_hash = Decodable::decode(buf)?;
        let beneficiary = Decodable::decode(buf)?;
        let state_root = Decodable::decode(buf)?;
        let transactions_root = Decodable::decode(buf)?;
        let behaviors_root = Decodable::decode(buf)?;
        let receipts_root = Decodable::decode(buf)?;
        let logs_bloom = Decodable::decode(buf)?;
        let behaviors_bloom = Decodable::decode(buf)?;
        let difficulty = Decodable::decode(buf)?;
        let number = Decodable::decode(buf)?;
        let gas_limit = Decodable::decode(buf)?;
        let gas_used = Decodable::decode(buf)?;
        let timestamp = Decodable::decode(buf)?;
        let behavior_total_quantity = Decodable::decode(buf)?;
        let extra_data = Decodable::decode(buf)?;
        let mix_hash = Decodable::decode(buf)?;
        let nonce = Decodable::decode(buf)?;
        let vdf_difficulty = Decodable::decode(buf)?;
        let vdf_proof_challenge = Decodable::decode(buf)?;
        let vdf_proof_result = Decodable::decode(buf)?;
        let base_fee_per_gas = if buf.len() > leftover {
            Some(Decodable::decode(buf)?)
        } else {
            None
        };

        Ok(Self {
            parent_hash,
            ommers_hash,
            beneficiary,
            state_root,
            transactions_root,
            behaviors_root,
            receipts_root,
            logs_bloom,
            behaviors_bloom,
            difficulty,
            number,
            gas_limit,
            gas_used,
            timestamp,
            behavior_total_quantity,
            extra_data,
            mix_hash,
            nonce,
            base_fee_per_gas,
            vdf_proof_challenge,
            vdf_proof_result,
            vdf_difficulty,
        })
    }
}

impl BlockHeader {
    #[must_use]
    pub fn new(
        partial_header: PartialHeader,
        ommers_hash: H256,
        transactions_root: H256,
        behaviors_root: H256,
        vdf_proof_challenge: [u8; 32],
        vdf_proof_result: [u8; 516],
        vdf_difficulty: u64,
    ) -> Self {
        Self {
            parent_hash: partial_header.parent_hash,
            ommers_hash,
            beneficiary: partial_header.beneficiary,
            state_root: partial_header.state_root,
            transactions_root,
            behaviors_root,
            receipts_root: partial_header.receipts_root,
            logs_bloom: partial_header.logs_bloom,
            behaviors_bloom: partial_header.behaviors_bloom,
            difficulty: partial_header.difficulty,
            number: partial_header.number,
            gas_limit: partial_header.gas_limit,
            gas_used: partial_header.gas_used,
            timestamp: partial_header.timestamp,
            behavior_total_quantity: partial_header.behavior_total_quantity,
            extra_data: partial_header.extra_data,
            mix_hash: partial_header.mix_hash,
            nonce: partial_header.nonce,
            base_fee_per_gas: partial_header.base_fee_per_gas,
            vdf_difficulty,
            vdf_proof_challenge,
            vdf_proof_result,
        }
    }

    #[cfg(test)]
    pub(crate) const fn empty() -> Self {
        Self {
            parent_hash: H256::zero(),
            ommers_hash: H256::zero(),
            beneficiary: Address::zero(),
            state_root: H256::zero(),
            transactions_root: H256::zero(),
            behaviors_root: H256::zero(),
            receipts_root: H256::zero(),
            logs_bloom: Bloom::zero(),
            behaviors_bloom: Bloom::zero(),
            difficulty: U256::ZERO,
            number: BlockNumber(0),
            gas_limit: 0,
            gas_used: 0,
            timestamp: 0,
            behavior_total_quantity: U256::ZERO,
            extra_data: Bytes::new(),
            mix_hash: H256::zero(),
            nonce: H64::zero(),
            base_fee_per_gas: None,
            vdf_difficulty: 0,
            vdf_proof_challenge: [0; 32],
            vdf_proof_result: [0; 516],
        }
    }

    #[must_use]
    pub fn hash(&self) -> H256 {
        let mut out = BytesMut::new();
        Encodable::encode(self, &mut out);
        keccak256(&out[..])
    }

    #[must_use]
    pub fn truncated_hash(&self) -> H256 {
        struct TruncatedHeader {
            parent_hash: H256,
            ommers_hash: H256,
            beneficiary: H160,
            state_root: H256,
            transactions_root: H256,
            behaviors_root: H256,
            receipts_root: H256,
            logs_bloom: Bloom,
            behaviors_bloom: Bloom,
            difficulty: U256,
            number: BlockNumber,
            gas_limit: u64,
            gas_used: u64,
            timestamp: u64,
            behavior_total_quantity: U256,
            extra_data: Bytes,
            base_fee_per_gas: Option<U256>,
        }

        impl TruncatedHeader {
            fn rlp_header(&self) -> Header {
                let mut rlp_head = Header {
                    list: false,
                    payload_length: 0,
                };

                rlp_head.payload_length += KECCAK_LENGTH + 1; // parent_hash
                rlp_head.payload_length += KECCAK_LENGTH + 1; // ommers_hash
                rlp_head.payload_length += ADDRESS_LENGTH + 1; // beneficiary
                rlp_head.payload_length += KECCAK_LENGTH + 1; // state_root
                rlp_head.payload_length += KECCAK_LENGTH + 1; // transactions_root
                rlp_head.payload_length += KECCAK_LENGTH + 1; // behaviors_root
                rlp_head.payload_length += KECCAK_LENGTH + 1; // receipts_root
                rlp_head.payload_length += BLOOM_BYTE_LENGTH + length_of_length(BLOOM_BYTE_LENGTH); // logs_bloom
                rlp_head.payload_length += BLOOM_BYTE_LENGTH + length_of_length(BLOOM_BYTE_LENGTH); // behaviors_bloom
                rlp_head.payload_length += self.difficulty.length(); // difficulty
                rlp_head.payload_length += self.number.length(); // block height
                rlp_head.payload_length += self.gas_limit.length(); // gas_limit
                rlp_head.payload_length += self.gas_used.length(); // gas_used
                rlp_head.payload_length += self.timestamp.length(); // timestamp
                rlp_head.payload_length += self.behavior_total_quantity.length(); // behavior_total_quantity
                rlp_head.payload_length += self.extra_data.length(); // extra_data

                if let Some(base_fee_per_gas) = self.base_fee_per_gas {
                    rlp_head.payload_length += base_fee_per_gas.length();
                }

                rlp_head
            }
        }

        impl Encodable for TruncatedHeader {
            fn encode(&self, out: &mut dyn BufMut) {
                self.rlp_header().encode(out);
                Encodable::encode(&self.parent_hash, out);
                Encodable::encode(&self.ommers_hash, out);
                Encodable::encode(&self.beneficiary, out);
                Encodable::encode(&self.state_root, out);
                Encodable::encode(&self.transactions_root, out);
                Encodable::encode(&self.behaviors_root, out);
                Encodable::encode(&self.receipts_root, out);
                Encodable::encode(&self.logs_bloom, out);
                Encodable::encode(&self.behaviors_bloom, out);
                Encodable::encode(&self.difficulty, out);
                Encodable::encode(&self.number, out);
                Encodable::encode(&self.gas_limit, out);
                Encodable::encode(&self.gas_used, out);
                Encodable::encode(&self.timestamp, out);
                Encodable::encode(&self.behavior_total_quantity, out);
                Encodable::encode(&self.extra_data, out);
                if let Some(base_fee_per_gas) = self.base_fee_per_gas {
                    Encodable::encode(&base_fee_per_gas, out);
                }
            }
            fn length(&self) -> usize {
                let rlp_head = self.rlp_header();
                length_of_length(rlp_head.payload_length) + rlp_head.payload_length
            }
        }

        let mut buffer = BytesMut::new();

        TruncatedHeader {
            parent_hash: self.parent_hash,
            ommers_hash: self.ommers_hash,
            beneficiary: self.beneficiary,
            state_root: self.state_root,
            transactions_root: self.transactions_root,
            behaviors_root: self.behaviors_root,
            receipts_root: self.receipts_root,
            logs_bloom: self.logs_bloom,
            behaviors_bloom: self.behaviors_bloom,
            difficulty: self.difficulty,
            number: self.number,
            gas_limit: self.gas_limit,
            gas_used: self.gas_used,
            timestamp: self.timestamp,
            behavior_total_quantity: self.behavior_total_quantity,
            extra_data: self.extra_data.clone(),
            base_fee_per_gas: self.base_fee_per_gas,
        }
        .encode(&mut buffer);

        keccak256(&buffer[..])
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
/// Partial header definition without ommers hash, transactions root and behaviors root.
pub struct PartialHeader {
    pub parent_hash: H256,
    pub beneficiary: H160,
    pub state_root: H256,
    pub receipts_root: H256,
    pub logs_bloom: Bloom,
    pub behaviors_bloom: Bloom,
    pub difficulty: U256,
    pub number: BlockNumber,
    pub gas_limit: u64,
    pub gas_used: u64,
    pub timestamp: u64,
    pub behavior_total_quantity: U256,
    pub extra_data: Bytes,
    pub mix_hash: H256,
    pub nonce: H64,
    pub base_fee_per_gas: Option<U256>,
}

impl From<BlockHeader> for PartialHeader {
    fn from(header: BlockHeader) -> Self {
        Self {
            parent_hash: header.parent_hash,
            beneficiary: header.beneficiary,
            state_root: header.state_root,
            receipts_root: header.receipts_root,
            logs_bloom: header.logs_bloom,
            behaviors_bloom: header.behaviors_bloom,
            difficulty: header.difficulty,
            number: header.number,
            gas_limit: header.gas_limit,
            gas_used: header.gas_used,
            timestamp: header.timestamp,
            behavior_total_quantity: header.behavior_total_quantity,
            extra_data: header.extra_data,
            mix_hash: header.mix_hash,
            nonce: header.nonce,
            base_fee_per_gas: header.base_fee_per_gas,
        }
    }
}

impl PartialHeader {
    #[cfg(test)]
    pub(crate) const fn empty() -> Self {
        Self {
            parent_hash: H256::zero(),
            beneficiary: Address::zero(),
            state_root: H256::zero(),
            receipts_root: H256::zero(),
            logs_bloom: Bloom::zero(),
            behaviors_bloom: Bloom::zero(),
            difficulty: U256::ZERO,
            number: BlockNumber(0),
            gas_limit: 0,
            gas_used: 0,
            timestamp: 0,
            behavior_total_quantity: U256::ZERO,
            extra_data: Bytes::new(),
            mix_hash: H256::zero(),
            nonce: H64::zero(),
            base_fee_per_gas: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    

    #[test]
    fn encode_decode_block_header() {
        let mut block_header = BlockHeader::default();
        block_header.vdf_difficulty = 43690;
        block_header.vdf_proof_challenge = [1; 32];
        block_header.vdf_proof_result = [2; 516];
        block_header.extra_data = b"LIMOS".to_vec().into();

        let encoded_block_header = block_header.compact_encode();
        // println!("{:?}", encoded_block_header);
        let result: BlockHeader = BlockHeader::compact_decode(&encoded_block_header).unwrap();

        // Additional assertions as there was a problem when encoding/decoding
        assert_eq!(encoded_block_header, result.compact_encode());
        assert_eq!(block_header.extra_data.len(), result.extra_data.len());

        assert_eq!(block_header, result);
    }
}
