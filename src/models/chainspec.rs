use std::{
    collections::{BTreeMap, BTreeSet, HashMap, HashSet},
    time::Duration,
};

use bytes::Bytes;
use serde::*;

use crate::{
    consensus::{
        demurrage::demurrage::compute_balances_with_demurrage_applied, BeneficiaryFunction,
        BlockSchedule,
    },
    models::*,
    util::*,
};

type NodeUrl = String;

/// Imported from .ron file
#[derive(Debug, PartialEq, Eq)]
pub struct BlockExecutionSpec {
    pub revision: Revision,
    pub active_transitions: HashSet<Revision>,
    pub params: Params,
    pub system_contract_changes: HashMap<Address, Contract>,
    pub balance_changes: HashMap<Address, U256>,
    // imported from pob
    pub miner_behavior_reward_per_quantity: U256,
    pub behavior_reward_per_quantity: U256,
    pub miner_included_behavior_reward_per_quantity: U256,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(deny_unknown_fields)]
pub struct ChainSpec {
    pub name: String,
    pub consensus: ConsensusParams,
    #[serde(default)]
    pub upgrades: Upgrades,
    pub params: Params,
    pub genesis: Genesis,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub contracts: BTreeMap<BlockNumber, HashMap<Address, Contract>>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub balances: BTreeMap<BlockNumber, HashMap<Address, U256>>,
    pub p2p: P2PParams,
}

impl ChainSpec {
    #[allow(unused_variables)]
    pub fn collect_block_spec(&self, block_number: impl Into<BlockNumber>) -> BlockExecutionSpec {
        let block_number = block_number.into();
        let mut revision = Revision::Frontier;
        let mut active_transitions = HashSet::new();
        for (fork, r) in [
            (self.upgrades.paris, Revision::Paris),
            (self.upgrades.london, Revision::London),
            (self.upgrades.berlin, Revision::Berlin),
            (self.upgrades.istanbul, Revision::Istanbul),
            (self.upgrades.petersburg, Revision::Petersburg),
            (self.upgrades.constantinople, Revision::Constantinople),
            (self.upgrades.byzantium, Revision::Byzantium),
            (self.upgrades.spurious, Revision::Spurious),
            (self.upgrades.tangerine, Revision::Tangerine),
            (self.upgrades.homestead, Revision::Homestead),
        ] {
            if let Some(fork_block) = fork {
                if block_number >= fork_block {
                    if block_number == fork_block {
                        active_transitions.insert(r);
                    }
                    if revision == Revision::Frontier {
                        revision = r;
                    }

                    break;
                }
            }
        }
        let mut balance_changes = self
            .balances
            .get(&block_number)
            .cloned()
            .unwrap_or_default();
        if block_number.0 == 0 {
            balance_changes = compute_balances_with_demurrage_applied(
                &balance_changes,
                self.params.desired_demurrage_percent_per_day,
                self.params.desired_average_mining_time_in_ms,
            );
        }
        BlockExecutionSpec {
            revision,
            active_transitions,
            params: self.params.clone(),
            system_contract_changes: self.contracts.iter().fold(
                HashMap::new(),
                |mut acc, (bn, contracts)| {
                    if block_number >= *bn {
                        for (addr, contract) in contracts {
                            acc.insert(*addr, contract.clone());
                        }
                    }

                    acc
                },
            ),
            balance_changes,
            miner_behavior_reward_per_quantity: match &self.consensus.seal_verification {
                SealVerificationParams::Pob {
                    int_size_bits,
                    miner_behavior_reward_per_quantity,
                    behavior_reward_per_quantity,
                    miner_included_behavior_reward_per_quantity,
                    skip_pow_verification,
                } => BlockSchedule {
                    0: miner_behavior_reward_per_quantity.clone(),
                }
                .for_block(block_number),
                _ => U256::ZERO,
            },
            behavior_reward_per_quantity: match &self.consensus.seal_verification {
                SealVerificationParams::Pob {
                    int_size_bits,
                    miner_behavior_reward_per_quantity,
                    behavior_reward_per_quantity,
                    miner_included_behavior_reward_per_quantity,
                    skip_pow_verification,
                } => BlockSchedule {
                    0: behavior_reward_per_quantity.clone(),
                }
                .for_block(block_number),
                _ => U256::ZERO,
            },
            miner_included_behavior_reward_per_quantity: match &self.consensus.seal_verification {
                SealVerificationParams::Pob {
                    int_size_bits,
                    miner_behavior_reward_per_quantity,
                    behavior_reward_per_quantity,
                    miner_included_behavior_reward_per_quantity,
                    skip_pow_verification,
                } => BlockSchedule {
                    0: miner_included_behavior_reward_per_quantity.clone(),
                }
                .for_block(block_number),
                _ => U256::ZERO,
            },
        }
    }

    pub fn gather_forks(&self) -> BTreeSet<BlockNumber> {
        let mut forks = [
            self.upgrades.homestead,
            self.upgrades.tangerine,
            self.upgrades.spurious,
            self.upgrades.byzantium,
            self.upgrades.constantinople,
            self.upgrades.petersburg,
            self.upgrades.istanbul,
            self.upgrades.berlin,
            self.upgrades.london,
            // self.upgrades.paris,
        ]
        .iter()
        .copied()
        .flatten()
        .chain(self.consensus.eip1559_block)
        .chain(self.consensus.seal_verification.gather_forks())
        .chain(self.contracts.keys().copied())
        .chain(self.balances.keys().copied())
        .chain(self.params.additional_forks.iter().copied())
        .collect::<BTreeSet<BlockNumber>>();

        forks.remove(&BlockNumber(0));

        forks
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct DifficultyBomb {
    pub delays: BTreeMap<BlockNumber, BlockNumber>,
}

impl DifficultyBomb {
    pub fn get_delay_to(&self, block_number: BlockNumber) -> BlockNumber {
        self.delays
            .iter()
            .filter_map(|(&activation, &delay_to)| {
                if block_number >= activation {
                    Some(delay_to)
                } else {
                    None
                }
            })
            .last()
            .unwrap_or(BlockNumber(0))
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct ConsensusParams {
    pub seal_verification: SealVerificationParams,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub eip1559_block: Option<BlockNumber>,
}

pub fn switch_is_active(switch: Option<BlockNumber>, block_number: BlockNumber) -> bool {
    block_number >= switch.unwrap_or(BlockNumber(u64::MAX))
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum SealVerificationParams {
    Clique {
        #[serde(with = "duration_as_millis")]
        period: Duration,
        epoch: u64,
    },
    Pob {
        int_size_bits: u64,
        miner_behavior_reward_per_quantity: BTreeMap<BlockNumber, U256>,
        behavior_reward_per_quantity: BTreeMap<BlockNumber, U256>,
        miner_included_behavior_reward_per_quantity: BTreeMap<BlockNumber, U256>,
        #[serde(default)]
        skip_pow_verification: bool,
    },
    Beacon {
        #[serde(
            default,
            skip_serializing_if = "Option::is_none",
            with = "::serde_with::rust::unwrap_or_skip"
        )]
        terminal_total_difficulty: Option<U256>,
        #[serde(
            default,
            skip_serializing_if = "Option::is_none",
            with = "::serde_with::rust::unwrap_or_skip"
        )]
        terminal_block_hash: Option<H256>,
        #[serde(
            default,
            skip_serializing_if = "Option::is_none",
            with = "::serde_with::rust::unwrap_or_skip"
        )]
        terminal_block_number: Option<BlockNumber>,
        #[serde(
            default,
            skip_serializing_if = "Option::is_none",
            with = "::serde_with::rust::unwrap_or_skip"
        )]
        since: Option<BlockNumber>,
        #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
        block_reward: BTreeMap<BlockNumber, U256>,
        #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
        beneficiary: BTreeMap<BlockNumber, BeneficiaryFunction>,
    },
}

impl SealVerificationParams {
    pub fn gather_forks(&self) -> BTreeSet<BlockNumber> {
        match self {
            SealVerificationParams::Pob {
                miner_behavior_reward_per_quantity,
                behavior_reward_per_quantity,
                miner_included_behavior_reward_per_quantity,
                ..
            } => miner_behavior_reward_per_quantity
                .keys()
                .copied()
                // .flatten()
                .chain(behavior_reward_per_quantity.keys().copied())
                .chain(miner_included_behavior_reward_per_quantity.keys().copied())
                // .unique()
                .collect::<BTreeSet<BlockNumber>>(),
            SealVerificationParams::Beacon { .. } => BTreeSet::new(),
            _ => BTreeSet::new(),
        }
    }
}

// deserialize_str_as_u64
#[derive(Clone, Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct Upgrades {
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub homestead: Option<BlockNumber>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub tangerine: Option<BlockNumber>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub spurious: Option<BlockNumber>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub byzantium: Option<BlockNumber>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub constantinople: Option<BlockNumber>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub petersburg: Option<BlockNumber>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub istanbul: Option<BlockNumber>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub berlin: Option<BlockNumber>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub london: Option<BlockNumber>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub paris: Option<BlockNumber>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Params {
    pub chain_id: ChainId,
    pub network_id: NetworkId,
    #[serde(default, skip_serializing_if = "BTreeSet::is_empty")]
    pub additional_forks: BTreeSet<BlockNumber>,

    // TODO move to Duration object
    #[serde(default)]
    pub expiration_time_limit: u64, // EXP

    // Extending PoB params as needed outside consensus e.g. in processor
    // would need some refactoring in a near future
    #[serde(default)]
    pub desired_average_mining_time_in_ms: u64,
    #[serde(default)]
    pub desired_demurrage_percent_per_day: u32,
    #[serde(default)]
    pub difficulty_adjustment_period: u64,
    #[serde(default)]
    pub min_update_network_difficulty_percent: u64,
    #[serde(default)]
    pub max_update_network_difficulty_percent: u64,
    #[serde(default)]
    pub transaction_fees_percentage: u32,
    // fr
    #[serde(default)]
    pub miner_fixed_reward_per_transaction: U256,
    // tr
    // miner_behavior_reward_per_quantity: U256, // k
    // behavior_reward_per_quantity: u64,       // k'
    // miner_included_behavior_reward_per_quantity: u32,      // k''

    // Reserved for Future Use
    // demurrage_rate_per_block: u32,
    // demurrage_rate_per_day: u32,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum BlockScore {
    NoTurn = 1,
    InTurn = 2,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Seal {
    Ethash {
        #[serde(with = "hexbytes")]
        vanity: Bytes,
        difficulty: U256,
        nonce: H64,
        mix_hash: H256,
    },
    Clique {
        vanity: H256,
        score: BlockScore,
        signers: Vec<Address>,
    },
    Pob {
        #[serde(with = "hexbytes")]
        vanity: Bytes,
        difficulty: U256,
        nonce: H64,
        output: H256,
    },
}

impl Seal {
    pub fn difficulty(&self) -> U256 {
        match self {
            Seal::Ethash { difficulty, .. } => *difficulty,
            Seal::Pob { difficulty, .. } => *difficulty,
            Seal::Clique { score, .. } => (*score as u8).into(),
        }
    }

    pub fn extra_data(&self) -> Bytes {
        match self {
            Seal::Ethash { vanity, .. } => vanity.clone(),
            Seal::Pob { vanity, .. } => vanity.clone(),
            Seal::Clique {
                vanity, signers, ..
            } => {
                let mut v = Vec::new();
                v.extend_from_slice(vanity.as_bytes());
                for signer in signers {
                    v.extend_from_slice(signer.as_bytes());
                }
                v.extend_from_slice(&[0; 65]);
                v.into()
            }
        }
    }

    pub fn mix_hash(&self) -> H256 {
        match self {
            Seal::Ethash { mix_hash, .. } => *mix_hash,
            _ => H256::zero(),
        }
    }

    pub fn nonce(&self) -> H64 {
        match self {
            Seal::Ethash { nonce, .. } => *nonce,
            Seal::Pob { nonce, .. } => *nonce,
            _ => H64::zero(),
        }
    }
    pub fn output(&self) -> H256 {
        match self {
            Seal::Pob { output, .. } => *output,
            _ => H256::zero(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Genesis {
    pub number: BlockNumber,
    pub author: Address,
    pub gas_limit: u64,
    pub timestamp: u64,
    pub seal: Seal,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub base_fee_per_gas: Option<U256>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Contract {
    Contract {
        #[serde(with = "hexbytes")]
        code: Bytes,
    },
    Precompile(Precompile),
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum ModExpVersion {
    ModExp198,
    ModExp2565,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Precompile {
    EcRecover { base: u64, word: u64 },
    Sha256 { base: u64, word: u64 },
    Ripemd160 { base: u64, word: u64 },
    Identity { base: u64, word: u64 },
    ModExp { version: ModExpVersion },
    AltBn128Add { price: u64 },
    AltBn128Mul { price: u64 },
    AltBn128Pairing { base: u64, pair: u64 },
    Blake2F { gas_per_round: u64 },
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct P2PParams {
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub bootnodes: Vec<NodeUrl>,
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::unwrap_or_skip"
    )]
    pub dns: Option<String>,
}

fn deserialize_str_as_u64<'de, D>(deserializer: D) -> Result<u64, D::Error>
where
    D: de::Deserializer<'de>,
{
    U64::deserialize(deserializer).map(|num| num.as_u64())
}

#[cfg(test)]
mod tests {
    use bytes_literal::bytes;
    use hex_literal::hex;
    use maplit::*;

    use crate::res::chainspec::*;

    use super::*;

    #[test]
    fn load_chainspec() {
        assert_eq!(
            ChainSpec {
                name: "EcoMobiCoin".into(),
                consensus: ConsensusParams {
                    seal_verification: SealVerificationParams::Pob {
                        int_size_bits: 1_024,
                        miner_behavior_reward_per_quantity: BTreeMap::from([(BlockNumber(0), U256::from(2000000000000000000_u128))]),
                        behavior_reward_per_quantity: BTreeMap::from([(BlockNumber(0), U256::from(1000000000000000000_u128))]),
                        miner_included_behavior_reward_per_quantity: BTreeMap::from([(BlockNumber(0), U256::from(1000_u128))]),
                        skip_pow_verification: false,
                    },
                    eip1559_block: Some(0.into()),
                },
                upgrades: Upgrades::default(),
                params: Params {
                    chain_id: ChainId(63),
                    network_id: NetworkId(63),
                    additional_forks: BTreeSet::new(),
                    expiration_time_limit: 3600 * 24,
                    desired_average_mining_time_in_ms: 10000,
                    desired_demurrage_percent_per_day: 0,
                    difficulty_adjustment_period: 1,
                    min_update_network_difficulty_percent: 25,
                    max_update_network_difficulty_percent: 400,
                    transaction_fees_percentage: 1000,
                    miner_fixed_reward_per_transaction: U256::from(21000_u128),
                },
                genesis: Genesis {
                    number: BlockNumber(0),
                    author: hex!("0000000000000000000000000000000000000000").into(),
                    gas_limit: 5000,
                    timestamp: 0,
                    base_fee_per_gas: None,
                    seal: Seal::Pob {
                        vanity: bytes!("5365706f6c69612c20417468656e732c204174746963612c2047726565636521"),
                        difficulty: U256::from(0x20000_u128),
                        nonce: H64::default(),
                        output: H256::from(hex!(
                            "0000000000000000000000000000000000000000000000000000000000000000"
                        )),
                    },
                },
                contracts: Default::default(),
                balances: btreemap! {
                    BlockNumber(0) => HashMap::from([
                        (
                        Address::from(hex!("57db4b909b98b6b2e0c443fc7cdbc3ae82b41c3d")),
                        U256::from(0x7a120_u128),
                        )
                    ]),
                },
                p2p: P2PParams { bootnodes: vec!["enode://e12c160b9ad36e781a78d3040313293274fcccb9769152692077db9fcaa8bac9329095dabd0f399e9ab2be166491ba95bff3b5a937810f5158ac941126a2ca0b@192.168.79.125:30303"].into_iter().map(ToString::to_string).collect(), dns: None },
            },
            *ECOMOBICOIN,
        );
    }

    #[test]
    fn distinct_block_numbers() {
        assert_eq!(
            MAINNET.gather_forks(),
            vec![
                1_150_000, 1_920_000, 2_463_000, 2_675_000, 4_370_000, 7_280_000, 9_069_000,
                9_200_000, 12_244_000, 12_965_000, 13_773_000, 15_050_000,
            ]
            .into_iter()
            .map(BlockNumber)
            .collect()
        );
    }
}
