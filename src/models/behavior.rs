use anyhow::{bail, format_err};
use bytes::*;
use derive_more::Deref;
use ethereum_types::Address;
use modular_bitfield::prelude::*;
use secp256k1::{
    ecdsa::{RecoverableSignature, RecoveryId},
    Message as SecpMessage, SECP256K1,
};
use sha3::*;
use tracing::{debug, warn};

use crate::{
    consensus::Quantifiable,
    crypto::{is_valid_signature, keccak256},
    models::*,
    trie::*,
    util::*,
};

use super::util::{h256_from_compact, variable_from_compact, variable_to_compact};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum BehaviorType {
    Movement = 0,
}

impl TryFrom<u8> for BehaviorType {
    type Error = DecodeError;
    fn try_from(orig: u8) -> Result<Self, Self::Error> {
        match orig {
            0 => Ok(BehaviorType::Movement),
            _ => Err(DecodeError::Custom("Invalid behavior type")),
        }
    }
}

// Used for easier decoding
// TODO: remove me after rETH migration
#[bitfield]
#[derive(Clone, Copy, Debug, Default)]
pub struct BehaviorFlags {
    v_len: B5,
    ts_len: B3,
    quantity_len: B4,
    // only u64 not u256 so don't need B5
    #[skip]
    unused: B4,
}

/** # Proof of Behavior
A Proof of Behavior (PoB) is a tuple π = (b||σb)
with b a tuple b = (pk, ts, data) containing pk the identity of the producer (seen as a public key),
ts the timestamp of the behavior and data added data about the behavior itself;
and σb the signature of b by a given validator able to verify that the behavior incorated in data was done at ts by pk.
 */
#[derive(Clone, Debug, PartialEq, Eq, RlpEncodable, RlpDecodable, Serialize, Deserialize)]
pub struct Behavior {
    /// Added as EIP-155: Simple replay attack protection
    pub chain_id: ChainId,
    /// Quantified behavior using the dedicated formula for each type
    pub quantity: U256,
    /// Timestamp in seconds
    pub timestamp: u64,
    /// Behavior data (RFU)
    // #[educe(Debug(method = "write_hex_string"))]
    pub input: Bytes,
}

impl Behavior {
    pub fn new(chain_id: ChainId, quantity: U256, timestamp: u64, input: Bytes) -> Self {
        Self {
            chain_id,
            quantity,
            timestamp,
            input,
        }
    }

    // pub fn to_raw_data() {}

    pub fn hash(&self) -> H256 {
        let mut buf = BytesMut::new();

        // buf.put_u8(TxType::Behavior);
        buf.put_u8(0x3E);

        #[derive(RlpEncodable)]
        struct S<'a> {
            chain_id: ChainId,
            quantity: U256,
            timestamp: U64,
            input: &'a Bytes,
        }

        S {
            chain_id: self.chain_id,
            quantity: self.quantity,
            timestamp: U64::from(self.timestamp),
            input: &self.input,
        }
        .encode(&mut buf);

        keccak256(&buf)
    }

    pub fn chain_id(&self) -> Option<ChainId> {
        Some(self.chain_id)
    }
}

impl Default for Behavior {
    fn default() -> Self {
        Self {
            chain_id: ChainId(63),
            quantity: U256::ZERO,
            timestamp: 0,
            input: Bytes::new(),
        }
    }
}

impl Quantifiable<Behavior> for Behavior {
    fn quantify(&self) -> U256 {
        self.quantity
    }
}

#[derive(Clone, Debug, Deref, PartialEq, Eq)]
pub struct BehaviorWithSender {
    #[deref]
    pub behavior: Behavior,
    pub sender: Address,
}

impl Default for BehaviorWithSender {
    fn default() -> Self {
        BehaviorWithSender {
            behavior: Behavior::default(),
            sender: Address::zero(),
        }
    }
}

#[derive(Clone, Debug, Deref, PartialEq, Eq, Serialize, Deserialize)]
pub struct BehaviorWithSignature {
    #[deref]
    pub behavior: Behavior,
    pub signature: BehaviorSignature,
}

// Borrowed from Akula's transaction.rs
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct YParityAndChainId {
    pub odd_y_parity: bool,
    pub chain_id: Option<ChainId>,
}

impl YParityAndChainId {
    pub fn from_v(v: u64) -> Option<Self> {
        if v == 27 || v == 28 {
            // pre EIP-155
            Some(Self {
                odd_y_parity: v == 28,
                chain_id: None,
            })
        } else if v >= 35 {
            // https://eips.ethereum.org/EIPS/eip-155
            // Find chain_id and y_parity ∈ {0, 1} such that
            // v = chain_id * 2 + 35 + y_parity
            let w = v - 35;
            let chain_id = Some(ChainId(w >> 1)); // w / 2
            Some(Self {
                odd_y_parity: (w % 2) != 0,
                chain_id,
            })
        } else {
            None
        }
    }

    pub fn v(&self) -> u64 {
        if let Some(chain_id) = self.chain_id {
            chain_id.0 * 2 + 35 + self.odd_y_parity as u64
        } else {
            27 + self.odd_y_parity as u64
        }
    }
}

// Borrowed from MessageSignature in Akula's transaction.rs
// Replaced by transaction/signature.rs in rETH ?
#[derive(Default, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BehaviorSignature {
    odd_y_parity: bool,
    r: H256,
    s: H256,
}

impl BehaviorSignature {
    #[must_use]
    pub fn new(odd_y_parity: bool, r: impl Into<H256>, s: impl Into<H256>) -> Option<Self> {
        let r = r.into();
        let s = s.into();
        if is_valid_signature(r, s) {
            Some(Self { odd_y_parity, r, s })
        } else {
            None
        }
    }

    #[must_use]
    pub fn malleable(&self) -> bool {
        const HALF_N: H256 = H256(hex!(
            "7fffffffffffffffffffffffffffffff5d576e7357a4501ddfe92f46681b20a0"
        ));
        self.s > HALF_N
    }

    #[must_use]
    pub fn odd_y_parity(&self) -> bool {
        self.odd_y_parity
    }

    #[must_use]
    pub fn r(&self) -> &H256 {
        &self.r
    }

    #[must_use]
    pub fn s(&self) -> &H256 {
        &self.s
    }

    #[must_use]
    pub fn is_low_s(&self) -> bool {
        const LOWER: H256 = H256([
            0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0x5d, 0x57, 0x6e, 0x73, 0x57, 0xa4, 0x50, 0x1d, 0xdf, 0xe9, 0x2f, 0x46,
            0x68, 0x1b, 0x20, 0xa0,
        ]);

        self.s <= LOWER
    }
}

impl TrieEncode for BehaviorWithSignature {
    fn trie_encode(&self, buf: &mut dyn BufMut) {
        self.rlp_encode_inner(buf, true)
    }
}

impl Encodable for BehaviorWithSignature {
    fn encode(&self, out: &mut dyn BufMut) {
        self.rlp_encode_inner(out, false)
    }
}

impl Decodable for BehaviorWithSignature {
    fn decode(buf: &mut &[u8]) -> Result<Self, DecodeError> {
        if buf.is_empty() {
            return Err(DecodeError::Custom("no bx body"));
        }
        // Handle transaction type for Behavior
        if buf[0] == 0x3E {
            debug!("Decoding behavior 3E, {:?}", hex::encode(*buf));

            let _header = Header::decode(buf)?;
            // buf.advance(1);
            let _first = buf.get_u8();

            #[derive(RlpDecodable)]
            struct S {
                chain_id: ChainId,
                value: U256,
                timestamp: U64,
                input: Bytes,
                v: U64,
                r: U256,
                s: U256,
            }

            let S {
                chain_id,
                value,
                timestamp,
                input,
                v,
                r,
                s,
            } = S::decode(buf)?;

            let YParityAndChainId {
                odd_y_parity,
                chain_id: chain_id_from_v,
            } = YParityAndChainId::from_v(v.as_u64())
                .ok_or(DecodeError::Custom("Invalid recovery ID"))?;
            if chain_id_from_v.is_some() && chain_id.0 != chain_id_from_v.unwrap().0 {
                return Err(DecodeError::Custom("Invalid recovered chain ID"));
            }
            Ok(Self {
                behavior: Behavior {
                    chain_id,
                    quantity: value,
                    timestamp: timestamp.as_u64(),
                    input,
                },
                signature: BehaviorSignature::new(odd_y_parity, u256_to_h256(r), u256_to_h256(s))
                    .ok_or(DecodeError::Custom("Invalid behavior signature format"))?,
            })
        } else {
            warn!("Decoding a behavior outside transaction type 3E");
            let s = Self::rlp_decode(buf)?;
            Ok(s)
        }

        // Err(DecodeError::Custom("invalid bx type"))

        // if s.max_fee_per_gas()
        //     .checked_mul(s.gas_limit().into())
        //     .is_none()
        // {
        //     return Err(DecodeError::Custom("gas limit price product overflow"));
        // }
    }
}

impl BehaviorWithSignature {
    pub fn compact_encode(&self) -> Vec<u8> {
        let mut flags = BehaviorFlags::default();
        let v = YParityAndChainId {
            chain_id: Some(self.chain_id),
            odd_y_parity: self.signature.odd_y_parity,
        }
        .v();
        let v_encoded = variable_to_compact(v);
        flags.set_v_len(v_encoded.len() as u8);

        let timestamp_encoded = variable_to_compact(self.timestamp);
        flags.set_ts_len(timestamp_encoded.len() as u8);

        let quantity_encoded = variable_to_compact(self.quantity);
        flags.set_quantity_len(quantity_encoded.len() as u8);

        let mut out = flags.into_bytes().to_vec();
        out.extend_from_slice(&v_encoded);
        out.extend_from_slice(&self.signature.r[..]);
        out.extend_from_slice(&self.signature.s[..]);
        out.extend_from_slice(&timestamp_encoded);
        out.extend_from_slice(&quantity_encoded);

        out.extend_from_slice(&self.input);

        out
    }

    /// used for hash
    fn rlp_encode_inner(&self, out: &mut dyn BufMut, _standalone: bool) {
        #[derive(RlpEncodable)]
        struct S<'a> {
            chain_id: &'a ChainId,
            quantity: &'a U256,
            timestamp: &'a U64,
            input: &'a Bytes,
            v: u64,
            r: U256,
            s: U256,
        }

        S {
            chain_id: &self.behavior.chain_id,
            quantity: &U256::from(self.behavior.quantity),
            timestamp: &U64::from(self.behavior.timestamp),
            input: &self.behavior.input,
            v: YParityAndChainId {
                odd_y_parity: self.signature.odd_y_parity,
                chain_id: Some(self.behavior.chain_id),
            }
            .v(),
            r: U256::from_be_bytes(self.signature.r.0),
            s: U256::from_be_bytes(self.signature.s.0),
        }
        .encode(out);
    }

    pub fn compact_decode(mut buf: &[u8]) -> anyhow::Result<Self> {
        if buf.len() < 4 {
            bail!("input too short");
        }

        let flags = BehaviorFlags::from_bytes([buf.get_u8(), buf.get_u8()]);

        let (signature, chain_id);
        (signature, chain_id, buf) = signature_from_compact(buf, flags.v_len())?;

        let chain_id = chain_id.ok_or_else(|| format_err!("ChainId is not optional."))?;

        let timestamp;
        (timestamp, buf) = variable_from_compact(buf, flags.ts_len())?;

        let quantity;
        (quantity, buf) = variable_from_compact(buf, flags.quantity_len())?;

        let input = buf[..].to_vec().into();

        Ok(Self {
            behavior: Behavior {
                chain_id,
                timestamp,
                quantity,
                input,
            },
            signature,
        })
    }
    fn rlp_decode(buf: &mut &[u8]) -> Result<Self, DecodeError> {
        let h = Header::decode(&mut &**buf)?;

        if h.list {
            #[derive(RlpDecodable)]
            struct S {
                chain_id: ChainId,
                quantity: U256,
                timestamp: u64,
                input: Bytes,
                v: u64,
                r: U256,
                s: U256,
            }

            let S {
                chain_id,
                quantity,
                timestamp,
                input,
                v,
                r,
                s,
            } = S::decode(buf)?;

            let YParityAndChainId {
                odd_y_parity,
                chain_id: recovered_chain_id,
            } = YParityAndChainId::from_v(v).ok_or(DecodeError::Custom("Invalid recovery ID"))?;
            let signature = BehaviorSignature::new(odd_y_parity, u256_to_h256(r), u256_to_h256(s))
                .ok_or(DecodeError::Custom("Invalid behavior signature format"))?;

            if recovered_chain_id.is_some() && recovered_chain_id.unwrap() != chain_id {
                return Err(DecodeError::Custom(
                    "Chain ids between bx and signature are different.",
                ));
            }

            return Ok(Self {
                behavior: Behavior {
                    chain_id,
                    quantity,
                    timestamp,
                    input,
                },
                signature,
            });
        }

        Header::decode(buf)?;

        if buf.is_empty() {
            return Err(DecodeError::Custom("no bx body"));
        }

        Err(DecodeError::Custom("invalid bx type"))
    }
    //
    pub fn hash(&self) -> H256 {
        let mut buf = BytesMut::new();
        self.trie_encode(&mut buf);
        keccak256(buf)
    }

    pub fn v(&self) -> u64 {
        YParityAndChainId {
            odd_y_parity: self.signature.odd_y_parity,
            chain_id: Some(self.chain_id),
        }
        .v()
    }

    pub fn r(&self) -> H256 {
        self.signature.r
    }

    pub fn s(&self) -> H256 {
        self.signature.s
    }

    pub fn recover_sender(&self) -> anyhow::Result<Address> {
        let mut sig = [0u8; 64];

        sig[..32].copy_from_slice(self.r().as_bytes());
        sig[32..].copy_from_slice(self.s().as_bytes());

        let rec = RecoveryId::from_i32(self.signature.odd_y_parity() as i32)?;

        let public = &SECP256K1.recover_ecdsa(
            &SecpMessage::from_slice(self.behavior.hash().as_bytes())?,
            &RecoverableSignature::from_compact(&sig, rec)?,
        )?;

        let address_slice = &Keccak256::digest(&public.serialize_uncompressed()[1..])[12..];
        Ok(Address::from_slice(address_slice))
    }
}

impl Default for BehaviorWithSignature {
    fn default() -> Self {
        BehaviorWithSignature {
            behavior: Behavior::default(),
            signature: BehaviorSignature {
                odd_y_parity: true,
                r: H256::from(hex!(
                    "0000000000000000000000000000000000000000000000000000000000000001"
                )),
                s: H256::from(hex!(
                    "0000000000000000000000000000000000000000000000000000000000000001"
                )),
            },
        }
    }
}

fn signature_from_compact(
    mut buf: &[u8],
    v_len: u8,
) -> anyhow::Result<(BehaviorSignature, Option<ChainId>, &[u8])> {
    let (v, r, s);

    (v, buf) = variable_from_compact(buf, v_len)?;

    let YParityAndChainId {
        odd_y_parity,
        chain_id,
    } = YParityAndChainId::from_v(v).ok_or_else(|| format_err!("invalid v"))?;

    (r, buf) = h256_from_compact(buf)?;
    (s, buf) = h256_from_compact(buf)?;

    let signature = BehaviorSignature::new(odd_y_parity, r, s)
        .ok_or_else(|| format_err!("failed to reconstruct signature"))?;

    Ok((signature, chain_id, buf))
}

#[cfg(test)]
mod tests {
    use bytes_literal::bytes;

    use super::*;

    #[test]
    fn create_empty() {
        Behavior::default();
    }

    #[test]
    fn hash_behavior() {
        // ARRANGE
        let behavior = Behavior::new(ChainId(63), U256::from(1672534861_u128), 10_000,
                                     bytes!("f8650f84832156008287fb94cf7f9e66af820a19257a2108375b180b0ec491678204d2802ca035b7bfeb9ad9ece2cbafaaf8e202e706b4cfaeb233f46198f00b44d4a566a981a0612638fb29427ca33b9a3be2a0a561beecfe0269655be160d35e72d366a6a860"));
        // println!("behavior buf {:02x}", &buf);
        // f8748463b0db4d822710b867f8650f84832156008287fb94cf7f9e66af820a19257a2108375b180b0ec491678204d2802ca035b7bfeb9ad9ece2cbafaaf8e202e706b4cfaeb233f46198f00b44d4a566a981a0612638fb29427ca33b9a3be2a0a561beecfe0269655be160d35e72d366a6a8603f8080

        // ACT
        let hash = behavior.hash();

        // ASSERT
        assert_eq!(
            H256::from(hex!(
                "65096a1df14da26c8d50c139b91cf83e7ef5ca7c3e7ac22ac0c0f30b943bccf9"
            )),
            hash
        )
    }

    #[test]
    fn recover_sender() {
        // ARRANGE
        let behavior = Behavior::new(ChainId(636363),
                                     U256::from(1_000_000_000_u128), 1688980216,
                                     bytes!("015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240"));
        // f8748463b0db4d822710b867f8650f84832156008287fb94cf7f9e66af820a19257a2108375b180b0ec491678204d2802ca035b7bfeb9ad9ece2cbafaaf8e202e706b4cfaeb233f46198f00b44d4a566a981a0612638fb29427ca33b9a3be2a0a561beecfe0269655be160d35e72d366a6a8603f8080
        let signature = BehaviorSignature {
            odd_y_parity: false,
            r: H256::from(hex!(
                "2bd4fc8fb69a1494a7a45945f25d57ceaa363c78eae7970d293b3d2143b51516"
            )),
            s: H256::from(hex!(
                "009272960a6cc168966715c04c51d97c4dca8c6ad681c0f0ad6f2d5ccb20140a"
            )),
        };

        let _signed_behavior2 = BehaviorWithSignature {
            behavior,
            signature,
        };
        let bytes = &mut &hex::decode("3ef9015b8309b5cb843b9aca008464abcaf8b90104015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f424083136bb9a070053136f2911dee91c6f74feb2bd0ace75afc980f9c47ac0d68a7f2d4c0ac9ea0203086b1e77d22a65a294ae1a988940ca826419c2e040b216884802cd1f1ff47").unwrap()[..];
        let signed_behavior = BehaviorWithSignature::decode(bytes).unwrap();

        // bx EcoMobiCoinBehaviorTransactionRequest { tx: TransactionRequest { from: None, to: None, gas: None, gas_price: None, value: Some(210000), data: Some(Bytes(0x015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240)), nonce: None, chain_id: Some(636363) }, timestamp: Some(1688737410) }
        // behavior (unsigned) hash: ab052685f7c4b81541a73283340b831cdea735ab87a07b8339332309f4d739a6
        // println!(
        //     "recover_sender: signed_behavior hash: {:?}",
        //     signed_behavior.behavior.hash()
        // );
        assert_eq!(
            H256::from(hex!(
                "c297eb4d6eeeb046120a90388324476f154a3533292bd9aa0618d2ba21f23217"
            )),
            signed_behavior.behavior.hash()
        );
        // println!("signed_behavior: {:?}", signed_behavior);
        // println!("signed behavior input {:02x}", signed_behavior.input);

        // ACT
        let sender = signed_behavior.recover_sender().unwrap();

        // ASSERT
        assert_eq!(
            Address::from(hex!("2c7536E3605D9C16a7a3D7b1898e529396a65c23")),
            sender
        )
    }

    #[test]
    fn recover_sender_postman() {
        // ARRANGE
        let input: [u8; 123] = [
            1, 2, 3, 4, 184, 117, 2, 248, 114, 4, 26, 132, 89, 104, 47, 0, 132, 89, 104, 47, 13,
            130, 82, 8, 148, 97, 129, 87, 116, 56, 48, 153, 226, 72, 16, 171, 131, 42, 91, 42, 84,
            37, 193, 84, 213, 136, 41, 162, 36, 26, 246, 44, 0, 0, 128, 192, 1, 160, 89, 230, 182,
            127, 72, 251, 50, 231, 229, 112, 223, 177, 30, 4, 43, 90, 210, 229, 94, 60, 227, 206,
            156, 217, 137, 199, 224, 110, 7, 254, 234, 253, 160, 1, 107, 131, 244, 249, 128, 105,
            78, 210, 238, 228, 209, 6, 103, 36, 43, 31, 64, 220, 64, 105, 1, 179, 65, 37, 176, 8,
            211, 52, 212, 116, 105,
        ];
        let behavior = Behavior::new(
            ChainId(636363),
            U256::from(2441406250_u128),
            1693552573,
            Bytes::copy_from_slice(&input),
        );
        let signature = BehaviorSignature {
            odd_y_parity: true,
            r: H256::from(hex!(
                "70bcb39ac6f540498c3adfdf3a23ecce5cf7b4f75b0674c157da02350edf8ed4"
            )),
            s: H256::from(hex!(
                "40e997c09def486888c34e77565cce82b348d0035e2ea36bf125252f7895ff3c"
            )),
        };

        let signed_behavior = BehaviorWithSignature {
            behavior,
            signature,
        };

        // ACT
        let sender = signed_behavior.recover_sender().unwrap();

        // ASSERT
        assert_eq!(
            Address::from(hex!("6e04da1f76268713322d5a6e8631a0cfa41c034f")),
            sender
        )
    }

    #[test]
    #[ignore = "RFU: Need real behaviors :)"]
    fn decode_behavior() {
        let _bytes = &mut &hex::decode("f8650f84832156008287fb94cf7f9e66af820a19257a2108375b180b0ec491678204d2802ca035b7bfeb9ad9ece2cbafaaf8e202e706b4cfaeb233f46198f00b44d4a566a981a0612638fb29427ca33b9a3be2a0a561beecfe0269655be160d35e72d366a6a860").unwrap()[..];
        let _expected = Behavior::new(ChainId(63), U256::from(1672534861_u128), 10_000,
                                     bytes!("f8650f84832156008287fb94cf7f9e66af820a19257a2108375b180b0ec491678204d2802ca035b7bfeb9ad9ece2cbafaaf8e202e706b4cfaeb233f46198f00b44d4a566a981a0612638fb29427ca33b9a3be2a0a561beecfe0269655be160d35e72d366a6a860"));
        // decode already done in encode_behavior
    }

    #[test]
    fn decode_behavior_consumes_buffer() {
        let bytes = &mut &hex::decode("f9015b8309b5cb843b9aca008464abcaf8b90104015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f424083136bb9a070053136f2911dee91c6f74feb2bd0ace75afc980f9c47ac0d68a7f2d4c0ac9ea0203086b1e77d22a65a294ae1a988940ca826419c2e040b216884802cd1f1ff47").unwrap()[..];
        let _transaction_res = BehaviorWithSignature::decode(bytes).unwrap();
        assert_eq!(
            bytes.len(),
            0,
            "did not consume all bytes in the buffer, {:?} remaining",
            bytes.len()
        );
    }

    #[test]
    #[should_panic]
    fn decode_behavior_invalid_tx_type() {
        let bytes = &mut &hex::decode("b87502f872041a8459682f008459682f0d8252089461815774383099e24810ab832a5b2a5425c154d58829a2241af62c000080c001a059e6b67f48fb32e7e570dfb11e042b5ad2e55e3ce3ce9cd989c7e06e07feeafda0016b83f4f980694ed2eee4d10667242b1f40dc406901b34125b008d334d47469").unwrap()[..];
        let _transaction_res = BehaviorWithSignature::decode(bytes).unwrap();
    }

    #[test]
    #[should_panic]
    fn compact_decode_behavior_too_short() {
        let bytes = &mut &hex::decode("00").unwrap()[..];
        let _transaction_res = BehaviorWithSignature::compact_decode(bytes).unwrap();
    }

    #[test]
    fn encode_and_decode_behavior() {
        let input = bytes!("01020304b87502f872041a8459682f008459682f0d8252089461815774383099e24810ab832a5b2a5425c154d58829a2241af62c000080c001a059e6b67f48fb32e7e570dfb11e042b5ad2e55e3ce3ce9cd989c7e06e07feeafda0016b83f4f980694ed2eee4d10667242b1f40dc406901b34125b008d334d47469");
        let behavior = Behavior::new(ChainId(63), U256::from(1672534861_u128), 10_000, input);
        let signature = BehaviorSignature {
            odd_y_parity: true,
            r: H256::from(hex!(
                "0000000000000000000000000000000000000000000000000000000000000001"
            )),
            s: H256::from(hex!(
                "0000000000000000000000000000000000000000000000000000000000000001"
            )),
        };

        let signed_behavior = BehaviorWithSignature {
            behavior,
            signature,
        };
        let mut encoded = BytesMut::new();
        signed_behavior.encode(&mut encoded);

        // println!("Raw behavior: {:?}", hex::encode(&encoded));

        let decoded = BehaviorWithSignature::decode(&mut &*encoded).unwrap();
        assert_eq!(signed_behavior, decoded);
    }

    #[test]
    fn decode_behavior_3e_tx_type() {
        // ARRANGE
        let bytes = &mut &hex::decode(
            "3ef9015a8309b5cb830334508464a2d04ab90104015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f424083136bb9a0e0ebebcdb0698e528462783fb9413f2daa3268ef866be71bdfcec50647077352a04caad25cc3fdcc52f879751763ca159feee4eaaf8668d18e7748ef966577e178",
        )
            .unwrap()[..];
        // ACT
        let result = BehaviorWithSignature::decode(bytes).unwrap();

        // ASSERT
        assert_eq!(result.chain_id, ChainId(636363));
        assert_eq!(result.quantity, 210000);
        assert_eq!(result.timestamp, 1688391754);
    }

    #[test]
    fn encode_behavior_3e_tx_type() {
        // ARRANGE
        let behavior = Behavior::new(ChainId(63), U256::from(1688050897_u128), 210_000,
                                     bytes!("3ef901543f8303345084649ea253b90104015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f424080a02e973bda5b307dc792eb05be51dfb7fe41eda9d645a873df39d1258a38a82870a02659bfc13fded61bc788a3f2f03f790923dfa3f738f575a9579727f6e6d74a3d"));
        let mut buf = BytesMut::new();
        // ACT
        let _result = behavior.encode(&mut buf);

        println!("Encoded behavior: {:?}", hex::encode(&buf));

        // ASSERT
        // assert_eq!(result.chain_id, ChainId(63));
    }

    #[test]
    fn quantify_should_work() {
        assert_eq!(Behavior::default().quantify(), 0)
    }

    #[test]
    fn y_parity_and_chain_id() {
        for range in [0..27, 29..35] {
            for v in range {
                assert_eq!(YParityAndChainId::from_v(v), None);
            }
        }
        assert_eq!(
            YParityAndChainId::from_v(27).unwrap(),
            YParityAndChainId {
                odd_y_parity: false,
                chain_id: None,
            }
        );
        assert_eq!(
            YParityAndChainId::from_v(28).unwrap(),
            YParityAndChainId {
                odd_y_parity: true,
                chain_id: None,
            }
        );

        assert_eq!(
            YParityAndChainId::from_v(35).unwrap(),
            YParityAndChainId {
                odd_y_parity: false,
                chain_id: Some(ChainId(0)),
            }
        );
        assert_eq!(
            YParityAndChainId::from_v(36).unwrap(),
            YParityAndChainId {
                odd_y_parity: true,
                chain_id: Some(ChainId(0)),
            }
        );
        assert_eq!(
            YParityAndChainId::from_v(37).unwrap(),
            YParityAndChainId {
                odd_y_parity: false,
                chain_id: Some(ChainId(1)),
            }
        );
        assert_eq!(
            YParityAndChainId::from_v(38).unwrap(),
            YParityAndChainId {
                odd_y_parity: true,
                chain_id: Some(ChainId(1)),
            }
        );

        assert_eq!(
            YParityAndChainId {
                odd_y_parity: false,
                chain_id: None,
            }
            .v(),
            27
        );
        assert_eq!(
            YParityAndChainId {
                odd_y_parity: true,
                chain_id: None,
            }
            .v(),
            28
        );
        assert_eq!(
            YParityAndChainId {
                odd_y_parity: false,
                chain_id: Some(ChainId(1))
            }
            .v(),
            37
        );
        assert_eq!(
            YParityAndChainId {
                odd_y_parity: true,
                chain_id: Some(ChainId(1))
            }
            .v(),
            38
        );
    }
}
