use crate::{models::ChainSpec, res::chainspec};
use anyhow::format_err;
use derive_more::*;
use directories::ProjectDirs;
use expanded_pathbuf::ExpandedPathBuf;
use std::{
    fmt::Display,
    fs::File,
    path::{Path, PathBuf},
};

#[derive(Clone, Debug, Deref, DerefMut, FromStr)]

pub struct TruiteDataDir(pub ExpandedPathBuf);

impl TruiteDataDir {
    pub fn chain_data_dir(&self) -> PathBuf {
        self.0.join("chaindata")
    }

    pub fn etl_temp_dir(&self) -> PathBuf {
        self.0.join("etl-temp")
    }

    pub fn sentry_db(&self) -> PathBuf {
        self.0.join("sentrydb")
    }

    pub fn nodekey(&self) -> PathBuf {
        self.0.join("nodekey")
    }

    pub fn nodeid(&self) -> PathBuf {
        self.0.join("nodeid")
    }
}

impl Default for TruiteDataDir {
    fn default() -> Self {
        Self(ExpandedPathBuf(
            ProjectDirs::from("", "", "Truite")
                .map(|pd| pd.data_dir().to_path_buf())
                .unwrap_or_else(|| "data".into()),
        ))
    }
}

impl Display for TruiteDataDir {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0.as_os_str().to_str().unwrap())
    }
}

impl ChainSpec {
    pub fn load_from_file(path: impl AsRef<Path>) -> anyhow::Result<Self> {
        Ok(ron::de::from_reader(File::open(path)?)?)
    }

    pub fn load_builtin(name: impl AsRef<str>) -> anyhow::Result<Self> {
        let name = name.as_ref();
        Ok(match name.to_lowercase().as_str() {
            "ecomobicoin" => chainspec::ECOMOBICOIN.clone(),
            "clermont" => chainspec::CLERMONT.clone(),
            "mainnet" | "ethereum" => chainspec::MAINNET.clone(),
            _ => return Err(format_err!("Network {name} is unknown")),
        })
    }
}
