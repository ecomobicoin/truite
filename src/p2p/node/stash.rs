use std::fmt::Debug;

use itertools::Itertools;
use mdbx::EnvironmentKind;

use crate::{
    accessors::chain,
    kv::{tables, MdbxWithDirHandle},
    models::{
        behavior::BehaviorWithSignature, BlockBody, BlockHeader, BlockNumber, MessageWithSignature,
        H256,
    },
    p2p::types::{BlockId, GetBlockHeadersParams},
};

pub trait Stash: Send + Sync + Debug {
    fn get_headers(&self, _: GetBlockHeadersParams) -> anyhow::Result<Vec<BlockHeader>>;
    fn get_bodies(&self, _: Vec<H256>) -> anyhow::Result<Vec<BlockBody>>;
    fn get_transactions(&self, max_amount: usize) -> anyhow::Result<Vec<MessageWithSignature>>;
    fn get_behaviors(&self, max_amount: usize) -> anyhow::Result<Vec<BehaviorWithSignature>>;
    fn add_behaviors(&self, behaviors: &Vec<BehaviorWithSignature>) -> anyhow::Result<()>;
    fn add_transactions(&self, transactions: &Vec<MessageWithSignature>) -> anyhow::Result<()>;
    fn get_pending_behavior_db_state(&self) -> anyhow::Result<Vec<H256>>;
    fn get_pending_transactions_db_state(&self) -> anyhow::Result<Vec<H256>>;
}

#[allow(unused_variables)]
impl Stash for () {
    fn get_headers(&self, _: GetBlockHeadersParams) -> anyhow::Result<Vec<BlockHeader>> {
        Ok(vec![])
    }
    fn get_bodies(&self, _: Vec<H256>) -> anyhow::Result<Vec<BlockBody>> {
        Ok(vec![])
    }

    fn get_transactions(&self, max_amount: usize) -> anyhow::Result<Vec<MessageWithSignature>> {
        Ok(vec![])
    }

    fn get_behaviors(&self, max_amount: usize) -> anyhow::Result<Vec<BehaviorWithSignature>> {
        Ok(vec![])
    }

    fn add_behaviors(&self, behaviors: &Vec<BehaviorWithSignature>) -> anyhow::Result<()> {
        Ok(())
    }

    fn add_transactions(&self, transactions: &Vec<MessageWithSignature>) -> anyhow::Result<()> {
        Ok(())
    }

    fn get_pending_behavior_db_state(&self) -> anyhow::Result<Vec<H256>> {
        Ok(vec![])
    }

    fn get_pending_transactions_db_state(&self) -> anyhow::Result<Vec<H256>> {
        Ok(vec![])
    }
}

impl<E> Stash for MdbxWithDirHandle<E>
where
    E: EnvironmentKind,
{
    fn get_headers(&self, params: GetBlockHeadersParams) -> anyhow::Result<Vec<BlockHeader>> {
        let txn = self.begin()?;

        let limit = std::cmp::min(params.limit, 1024);
        let reverse = params.reverse == 1;

        let mut add_op = if params.skip == 0 {
            1
        } else {
            params.skip as i64 + 1
        };
        if reverse {
            add_op = -add_op;
        }

        let mut headers = Vec::with_capacity(limit as usize);
        let mut number_cursor = txn.cursor(tables::HeaderNumber)?;
        let mut header_cursor = txn.cursor(tables::Header)?;

        let mut next_number = match params.start {
            BlockId::Hash(hash) => number_cursor.seek_exact(hash)?.map(|(_, k)| k),
            BlockId::Number(number) => Some(number),
        };

        next_number = Some(next_number.unwrap_or_default() + params.skip);

        for _ in 0..limit {
            match next_number {
                Some(block_number) => {
                    if let Some((_, header)) = header_cursor.seek_exact(block_number)? {
                        headers.push(header);
                    }
                    next_number = u64::try_from(block_number.0 as i64 + add_op)
                        .ok()
                        .map(BlockNumber);
                }
                None => break,
            };
        }

        Ok::<_, anyhow::Error>(headers)
    }

    fn get_bodies(&self, hashes: Vec<H256>) -> anyhow::Result<Vec<BlockBody>> {
        let txn = self.begin().expect("Failed to begin transaction");

        Ok(hashes
            .into_iter()
            .filter_map(|hash| txn.get(tables::HeaderNumber, hash).unwrap_or(None))
            .filter_map(|number| {
                chain::block_body::read_without_senders(&txn, number).unwrap_or(None)
            })
            .collect::<Vec<_>>())
    }

    fn get_transactions(&self, max_amount: usize) -> anyhow::Result<Vec<MessageWithSignature>> {
        let tx = self.inner.begin().unwrap();
        chain::tx_pending::read_many(&tx, max_amount)
    }

    fn get_behaviors(&self, max_amount: usize) -> anyhow::Result<Vec<BehaviorWithSignature>> {
        let tx = self.inner.begin().unwrap();
        chain::bx_pending::read_many(&tx, max_amount)
    }

    fn add_behaviors(&self, behaviors: &Vec<BehaviorWithSignature>) -> anyhow::Result<()> {
        let tx = self.inner.begin_mutable().unwrap();
        chain::bx_pending::write_from_vec(&tx, 1, behaviors)
    }

    fn add_transactions(&self, transactions: &Vec<MessageWithSignature>) -> anyhow::Result<()> {
        let tx = self.inner.begin_mutable().unwrap();
        chain::tx_pending::write_from_vec(&tx, 1, transactions)
    }

    fn get_pending_behavior_db_state(&self) -> anyhow::Result<Vec<H256>> {
        let txn = self.inner.begin()?;
        let db_state = txn
            .cursor(tables::PendingBehavior)?
            .walk(None)
            .collect_vec();
        let mut res: Vec<H256> = vec![];
        for value in db_state {
            res.push(value.unwrap().1.hash());
        }
        Ok(res)
    }

    fn get_pending_transactions_db_state(&self) -> anyhow::Result<Vec<H256>> {
        let txn = self.inner.begin()?;
        let db_state = txn
            .cursor(tables::PendingTransaction)?
            .walk(None)
            .collect_vec();
        let mut res: Vec<H256> = vec![];
        for value in db_state {
            res.push(value.unwrap().1.hash());
        }
        Ok(res)
    }
}
