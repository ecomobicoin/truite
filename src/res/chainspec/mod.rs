use crate::models::ChainSpec;
use once_cell::sync::Lazy;

pub static MAINNET: Lazy<ChainSpec> =
    Lazy::new(|| ron::from_str(include_str!("ethereum.ron")).unwrap());
pub static ECOMOBICOIN: Lazy<ChainSpec> =
    Lazy::new(|| ron::from_str(include_str!("ecomobicoin.ron")).unwrap());
pub static CLERMONT: Lazy<ChainSpec> =
    Lazy::new(|| ron::from_str(include_str!("clermont.ron")).unwrap());
pub static LOCAL: Lazy<ChainSpec> = Lazy::new(|| ron::from_str(include_str!("local.ron")).unwrap());
