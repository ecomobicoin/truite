#!/bin/sh

set -e

if [ -f "$GENESIS_PATH" ]; then
    echo "Secrets have already been generated."
else
    echo "Generating secrets..."
    # secrets=$("$POLYGON_EDGE_BIN" secrets init --insecure --num 4 --data-dir /data/data- --json)
    # echo "Secrets have been successfully generated"
    # mkdir -p /data/data-1 /data/data-2
    echo -n "6cc26b8e1321d771f5bf3af188aca546941b7c35941ba426d4bfc38ec54bbe46" > /data/data-1/nodekey
    echo -n"433d2cd42ec425403903ecffefd7cbcdc582e34575e2b5b8d2d7bb7958045162" > /data/data-1/jwt.hex

    echo -n "a0e7c71661af160e5b71be81672629d4960b9736e2d5568e153650253f2cfbd8" > /data/data-2/nodekey
    echo -n "fa927d7eb2051cf226df5c21d44cb27c035ea4227dea435a8dd8ca983e6335e1" > /data/data-2/jwt.hex

    chown truite:truite /data/data-{1,2}/nodekey
    chown truite:truite /data/data-{1,2}/jwt.hex
    
    echo "Added keys for node-1, node-2"

    # Need root user to create file
    cp ./local.ron /data/local.ron
    cp ./local.ron /data/local-1.ron
    cp ./local.ron /data/local-2.ron

    sed -i 's/\/\/"enode:\/\/NODE1/"enode:\/\/0bcddc979717e692c7e05e281db4febb96fa566fbe43ae6b3f7860bec62af336393c3443d9a9dc746d66b7712f5b6742cd90d22afdc8ad2c08d1cd9b958964cd/g' /data/local.ron
    sed -i 's/\/\/"enode:\/\/NODE2/"enode:\/\/e4ac9a8b6b779d482e74ee2f7b35b58269757dbd2d53364fe683a90935b7866d0262cf6cb947e06bf28b0563964edef15be15ff472c2c232ec7d1bb20764394a/g' /data/local.ron

    sed -i 's/\/\/"enode:\/\/NODE2/"enode:\/\/e4ac9a8b6b779d482e74ee2f7b35b58269757dbd2d53364fe683a90935b7866d0262cf6cb947e06bf28b0563964edef15be15ff472c2c232ec7d1bb20764394a/g' /data/local-1.ron

    sed -i 's/\/\/"enode:\/\/NODE1/"enode:\/\/0bcddc979717e692c7e05e281db4febb96fa566fbe43ae6b3f7860bec62af336393c3443d9a9dc746d66b7712f5b6742cd90d22afdc8ad2c08d1cd9b958964cd/g' /data/local-2.ron

    echo "Updated local.ron"

    # cd /data && /polygon-edge/polygon-edge genesis $CHAIN_CUSTOM_OPTIONS \
    # --bootnode "/dns4/node-1/tcp/1478/p2p/$(echo "$secrets" | jq -r '.[0] | .node_id')" \
    # --bootnode "/dns4/node-2/tcp/1478/p2p/$(echo "$secrets" | jq -r '.[1] | .node_id')"
fi